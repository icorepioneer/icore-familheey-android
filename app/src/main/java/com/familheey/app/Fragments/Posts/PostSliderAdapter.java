package com.familheey.app.Fragments.Posts;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.familheey.app.Activities.FamilyDashboardActivity;
import com.familheey.app.BuildConfig;
import com.familheey.app.Models.Request.HistoryImages;
import com.familheey.app.Post.PostDetailActivity;
import com.familheey.app.R;
import com.familheey.app.Topic.MainActivity;
import com.familheey.app.Topic.TopicsListingActivity;
import com.familheey.app.Utilities.Constants;
import com.familheey.app.Utilities.VideoEnabledWebChromeClient;
import com.familheey.app.Utilities.VideoEnabledWebView;
import com.google.gson.Gson;
import com.smarteist.autoimageslider.SliderViewAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.familheey.app.Utilities.Constants.ApiPaths.S3_DEV_IMAGE_URL_SQUARE_DETAILED;
import static com.familheey.app.Utilities.Constants.ApiPaths.S3_DEV_IMAGE_URL_THUMB;
import static com.familheey.app.Utilities.Constants.Bundle.DATA;

public class PostSliderAdapter extends SliderViewAdapter<PostSliderAdapter.SliderAdapterVH> {
    private Context context;
    private PostData postData;
    private String IMAGE_BASE_URL = BuildConfig.IMAGE_BASE_URL;

    public PostSliderAdapter(Context context, PostData postData) {
        this.context = context;
        this.postData = postData;
    }

    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_post_slider, null);
        return new SliderAdapterVH(inflate);
    }

    @Override
    public void onBindViewHolder(SliderAdapterVH viewHolder, int position) {
        HistoryImages document = postData.getPost_attachment().get(position);

        if (document.getType() == null || document.getType().contains("image")) {

            viewHolder.doc.setVisibility(View.GONE);
            viewHolder.autoSliderImage.setVisibility(View.VISIBLE);
            viewHolder.web.setVisibility(View.GONE);
            viewHolder.video_back_ground.setVisibility(View.GONE);
            if (position == 0 && document.getHeight() != null) {
                int w = ((int) Double.parseDouble(document.getWidth()));
                int h = ((int) Double.parseDouble(document.getHeight()));

                if (document.getHeight1() != null && document.getHeight1().length() > 0) {
                    h = ((int) Double.parseDouble(document.getHeight1()));
                }

                if (w < h || w > 1400 && h > 850) {
                    viewHolder.autoSliderImage.setScaleType(ImageView.ScaleType.FIT_XY);
                } else {
                    viewHolder.autoSliderImage.setScaleType(ImageView.ScaleType.FIT_CENTER);
                }
            } else {
                viewHolder.autoSliderImage.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            }

            String url = "";
            String turl = "";

            if (document.getFilename() != null) {
                if (postData.getPublish_type() != null && "albums".equals(postData.getPublish_type())) {
                    if (document.getFilename().contains(".gif")) {
                        url = BuildConfig.IMAGE_BASE_URL + "Documents/" + document.getFilename();
                        turl = url;
                    } else {
                        url = S3_DEV_IMAGE_URL_SQUARE_DETAILED + BuildConfig.IMAGE_BASE_URL + "Documents/" + document.getFilename();
                        turl = S3_DEV_IMAGE_URL_THUMB + BuildConfig.IMAGE_BASE_URL + "Documents/" + document.getFilename();
                    }
                } else {
                    if (document.getFilename().contains(".gif")) {
                        url = BuildConfig.IMAGE_BASE_URL + "post/" + document.getFilename();
                        turl = url;
                    } else {
                        url = S3_DEV_IMAGE_URL_SQUARE_DETAILED + BuildConfig.IMAGE_BASE_URL + "post/" + document.getFilename();
                        turl = S3_DEV_IMAGE_URL_THUMB + BuildConfig.IMAGE_BASE_URL + "post/" + document.getFilename();
                    }
                }
            }
            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.progress_animation)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH)
                    .dontAnimate()
                    .dontTransform();
            RequestBuilder thumbnailRequest = Glide
                    .with( context )
                    .load(turl);
            Glide.with(context)
                    .load(url)
                    .thumbnail(thumbnailRequest)
                    .transition(DrawableTransitionOptions.withCrossFade()).apply(options)
                    .into(viewHolder.autoSliderImage);
            viewHolder.autoSliderImage.setOnClickListener(v -> {
                String type = "";
                if ("albums".equals(postData.getPublish_type())) {
                    type = "albums";
                }
                Intent intent = new Intent(context, PostDetailActivity.class).putExtra(DATA, new Gson().toJson(postData.getPost_attachment())).putExtra("pos", position).putExtra(Constants.Bundle.DETAIL, type);
                ActivityOptionsCompat activityOptions;
                if (context instanceof FamilyDashboardActivity) {
                    activityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(((FamilyDashboardActivity) context),
                            new Pair<>(viewHolder.autoSliderImage, PostDetailActivity.VIEW_NAME_HEADER_IMAGE));
                    ActivityCompat.startActivity(context, intent, activityOptions.toBundle());
                } else if (context instanceof MainActivity) {
                    activityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(((MainActivity) context),
                            new Pair<>(viewHolder.autoSliderImage, PostDetailActivity.VIEW_NAME_HEADER_IMAGE));
                    ActivityCompat.startActivity(context, intent, activityOptions.toBundle());
                } else if (context instanceof TopicsListingActivity) {
                    activityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(((TopicsListingActivity) context),
                            new Pair<>(viewHolder.autoSliderImage, PostDetailActivity.VIEW_NAME_HEADER_IMAGE));
                    ActivityCompat.startActivity(context, intent, activityOptions.toBundle());
                } else {
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }

            });
        } else if (document.getType().contains("video")) {
            viewHolder.doc.setVisibility(View.GONE);
            viewHolder.video_back_ground.setVisibility(View.VISIBLE);
            viewHolder.autoSliderImage.setVisibility(View.GONE);
            viewHolder.web.setVisibility(View.VISIBLE);
            String url = IMAGE_BASE_URL + "post/" + document.getFilename();

            if (postData.getPublish_type() != null && "albums".equals(postData.getPublish_type())) {
                url = IMAGE_BASE_URL + "Documents/" + document.getFilename();
            }

            String previwurl = IMAGE_BASE_URL + "default_video.jpg";
            if (document.getVideo_thumb() != null && !document.getVideo_thumb().equals("")) {
                previwurl = IMAGE_BASE_URL + document.getVideo_thumb();
            }
            String sourc = "<source src=\"" + url + "\"  type=\"video/mp4\">";
            String html = "<html><body><video width=\"100%\" height=\"100%\" poster=\"" + previwurl + "\"controls=\"controls\" controlsList=\"nodownload\" >" + sourc + "</video></body></html>";
            viewHolder.web.setBackgroundColor(Color.TRANSPARENT);
            viewHolder.webChromeClient = new VideoEnabledWebChromeClient(viewHolder.video_back_ground, viewHolder.video_back_ground) // See all available constructors...
            {
                // Subscribe to standard events, such as onProgressChanged()...
                @Override
                public void onProgressChanged(WebView view, int progress) {
                    // Your code...
                }
            };
            String type = "";
            if ("albums".equals(postData.getPublish_type())) {
                type = "albums";
            }
            String finalType = type;
            viewHolder.webChromeClient.setOnToggledFullscreen(fullscreen -> context.startActivity(new Intent(context, PostDetailActivity.class).putExtra(DATA, new Gson().toJson(postData.getPost_attachment())).putExtra("pos", position).putExtra(Constants.Bundle.DETAIL, finalType)));
            //  Log.e("HTML", html);
            viewHolder.web.setWebChromeClient(viewHolder.webChromeClient);
            viewHolder.web.loadData(html, "text/html", "UTF-8");

        } else {
            viewHolder.doc.setVisibility(View.VISIBLE);
            if(document.getOriginal_name()!=null&&!document.getOriginal_name().isEmpty()){
                viewHolder.txt_doc_name.setText(document.getOriginal_name());
            }else{
                viewHolder.txt_doc_name.setText(document.getFilename());
            }
            viewHolder.autoSliderImage.setVisibility(View.GONE);
            //viewHolder.videoView.setVisibility(View.GONE);
            viewHolder.video_back_ground.setVisibility(View.GONE);
            viewHolder.doc.setOnClickListener(v -> {
                String url = IMAGE_BASE_URL + "post/" + document.getFilename();

                if (postData.getPublish_type() != null && "documents".equals(postData.getPublish_type())) {
                    url = IMAGE_BASE_URL + "Documents/" + document.getFilename();
                }

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                viewHolder.itemView.getContext().startActivity(browserIntent);
            });
        }

    }

    @Override
    public int getCount() {
        return postData.getPost_attachment().size();
    }

    static class SliderAdapterVH extends SliderViewAdapter.ViewHolder {

        private VideoEnabledWebChromeClient webChromeClient;

        View itemView;

        @BindView(R.id.web)
        VideoEnabledWebView web;

        @BindView(R.id.iv_auto_image_slider)
        ImageView autoSliderImage;

        @BindView(R.id.doc)
        RelativeLayout doc;

        @BindView(R.id.txt_doc_name)
        TextView txt_doc_name;

        @BindView(R.id.doc_view)
        TextView doc_view;

       /* @BindView(R.id.video_view)
        VideoView videoView;*/

        @BindView(R.id.video_back_ground)
        RelativeLayout video_back_ground;


        public SliderAdapterVH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.itemView = itemView;
        }
    }

}