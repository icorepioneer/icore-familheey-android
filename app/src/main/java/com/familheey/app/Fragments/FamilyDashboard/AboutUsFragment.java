package com.familheey.app.Fragments.FamilyDashboard;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.familheey.app.Activities.ImageSliderActivity;
import com.familheey.app.Activities.TextEditActivity;
import com.familheey.app.EditHistoryActivity;
import com.familheey.app.Interfaces.FamilyDashboardInteractor;
import com.familheey.app.Models.Request.HistoryImages;
import com.familheey.app.Models.Response.Family;
import com.familheey.app.R;
import com.familheey.app.Utilities.Utilities;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.familheey.app.Utilities.Constants.ApiPaths.IMAGE_BASE_URL;
import static com.familheey.app.Utilities.Constants.Bundle.DATA;
import static com.familheey.app.Utilities.Constants.Bundle.ID;
import static com.familheey.app.Utilities.Constants.Bundle.POSITION;
import static com.familheey.app.Utilities.Constants.FamilyDashboardIdentifiers.TypeAboutUsFragment;

public class AboutUsFragment extends Fragment {

    @BindView(R.id.intro)
    TextView intro;
    @BindView(R.id.btn_add_history)
    Button btn_add_history;
    @BindView(R.id.familyName)
    TextView familyName;
    @BindView(R.id.familyIntroduction)
    TextView familyIntroduction;
    @BindView(R.id.editFamily)
    ImageView editFamily;
    @BindView(R.id.historyName)
    TextView historyName;
    @BindView(R.id.historyNameUnderLine)
    ImageView historyNameUnderLine;
    //@BindView(R.id.labelHistoryIntroduction)
    //TextView labelHistoryIntroduction;
    @BindView(R.id.editHistory)
    ImageView editHistory;

    @BindView(R.id.webview)
    WebView webview;
    private Family family;

    private FamilyDashboardInteractor familyDashboardInteractor;

    public AboutUsFragment() {
        // Required empty public constructor
    }

    public static AboutUsFragment newInstance(Family family) {
        AboutUsFragment fragment = new AboutUsFragment();
        Bundle args = new Bundle();
        args.putParcelable(DATA, family);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            family = getArguments().getParcelable(DATA);
            //getArguments().clear();
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about_us, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initRestrictions();
    }

    private void initRestrictions() {
        if (family != null && family.isAdmin()) {
            editFamily.setVisibility(View.VISIBLE);
            editHistory.setVisibility(View.VISIBLE);
            if (family.getHistoryText() == null || family.getHistoryText().isEmpty() && family.getHistoryImages() == null || family.getHistoryImages().size() > 0) {
                webview.setVisibility(View.GONE);
            }

        } else {
            editFamily.setVisibility(View.INVISIBLE);
            editHistory.setVisibility(View.INVISIBLE);
        }

        loadHtml();
    }


    private void loadHtml() {
        String text = "";
        String imageHtml = "";
        String html = "";

        if (family.getHistoryText() != null) {
            text = family.getHistoryText();
        }

        if (family.getHistoryImages() != null && family.getHistoryImages().size() > 0) {
            String url = IMAGE_BASE_URL + "history_images/" + family.getHistoryImages().get(0).getFilename();
            imageHtml = "<a href=\"https://www.gallery.com/\"><img align=\"left\"\"style=\"margin:0 16px 5px 0\" src=\"" + url + "\" align=\"left\" width=\"150px\" /></a>";
        }

        if (text.isEmpty() && imageHtml.isEmpty()) {
            webview.setVisibility(View.GONE);
        } else if (!text.isEmpty() && imageHtml.isEmpty()) {

        } else if (text.isEmpty() && !imageHtml.isEmpty()) {

        }
        webview.setVisibility(View.VISIBLE);
        html = "<html><body><style>div {width: 100%; }div.b {white-space:pre-wrap;}</style><div class=\"b\">" + imageHtml +  text + "</div></body></html>";
        webview.setWebViewClient(new MyWebViewClient(getContext()));
        webview.loadData(html, "text/html", null);

    }
/*

 */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        familyDashboardInteractor = Utilities.getListener(this, FamilyDashboardInteractor.class);
        if (familyDashboardInteractor == null)
            throw new RuntimeException(context.toString() + " must implement FamilyDashboardInteractor");
    }

    public void fillDetails(Family family) {
        this.family = family;
        familyIntroduction.setText(family.getIntro());
        initRestrictions();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        familyDashboardInteractor = null;
    }

    @OnClick({R.id.intro, R.id.editHistory, R.id.editFamily})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.editHistory:
                startActivityForResult(new Intent(getActivity(), EditHistoryActivity.class).putExtra(DATA, new Gson().toJson(family)), 101);
                //mListener.updateFamily(FamilyEditDialogFragment.TYPE_HISTORY, family);
                break;
            case R.id.editFamily:
               // mListener.updateFamily(FamilyEditDialogFragment.TYPE_INTRODUCTION, family);
                startActivityForResult(new Intent(getActivity(), TextEditActivity.class).putExtra(ID, family.getId() + "").putExtra("type", "family").putExtra("tittle", "Edit Introduction").putExtra(DATA, familyIntroduction.getText() + ""), 102);
                break;
        }
    }

    public class MyWebViewClient extends WebViewClient {

        private Context context;

        public MyWebViewClient(Context context) {
            this.context = context;
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (family.getHistoryImages() != null && family.getHistoryImages().size() > 0) {
                Intent intent = new Intent(getContext(), ImageSliderActivity.class);
                intent.putExtra(DATA, family.getHistoryImagesUrls());
                intent.putExtra(POSITION, 0);
                startActivity(intent);
            }
            return super.shouldOverrideUrlLoading(view, url);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == 101) {
            family.setHistoryText(data.getExtras().getString("history"));
            family.setHistoryImages((ArrayList<HistoryImages>) fromJson(data.getExtras().getString("images"),
                    new TypeToken<ArrayList<HistoryImages>>() {
                    }.getType()));
            loadHtml();
        }
        else if (resultCode == Activity.RESULT_OK && requestCode == 102) {
            familyIntroduction.setText(data.getExtras().getString(DATA));
        }

    }

    public static Object fromJson(String jsonString, Type type) {
        return new Gson().fromJson(jsonString, type);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (familyDashboardInteractor != null)
            familyDashboardInteractor.onFamilyAddComponentHidden(TypeAboutUsFragment);
    }
}
