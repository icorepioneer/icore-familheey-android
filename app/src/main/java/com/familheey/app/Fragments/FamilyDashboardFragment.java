package com.familheey.app.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.amazonaws.mobile.auth.core.internal.util.ThreadUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.familheey.app.Activities.ImageChangerActivity;
import com.familheey.app.Activities.LinkedFamilyActivity;
import com.familheey.app.Activities.UserConnectionsActivity;
import com.familheey.app.Adapters.FamilyDashboardTabAdapter;
import com.familheey.app.Adapters.LinkFamilyAdapter;
import com.familheey.app.Announcement.AnnouncementListingFragment;
import com.familheey.app.Dialogs.FamilyEditDialogFragment;
import com.familheey.app.Fragments.Events.AlbumFragment;
import com.familheey.app.Fragments.FamilyDashboard.AboutUsFragment;
import com.familheey.app.Fragments.FamilyDashboard.FamilyEventFragment;
import com.familheey.app.Fragments.FamilyDashboard.FamilyNeedsListingFragment;
import com.familheey.app.Fragments.FamilyDashboard.LinkingFragment;
import com.familheey.app.Fragments.FamilyViewMembers.FamilySubscriptionFragment;
import com.familheey.app.Fragments.Posts.FamilyPostFeedFragment;
import com.familheey.app.Interfaces.FamilyDashboardInteractor;
import com.familheey.app.Interfaces.FamilyDashboardListener;
import com.familheey.app.Interfaces.ProgressListener;
import com.familheey.app.Interfaces.RetrofitListener;
import com.familheey.app.Models.ApiCallbackParams;
import com.familheey.app.Models.ErrorData;
import com.familheey.app.Models.Response.Family;
import com.familheey.app.Models.Response.LinkfamilyList;
import com.familheey.app.Networking.Retrofit.ApiServiceProvider;
import com.familheey.app.Parsers.FamilyParser;
import com.familheey.app.R;
import com.familheey.app.Stripe.StripeActivity;
import com.familheey.app.Topic.MainActivity;
import com.familheey.app.Utilities.Constants;
import com.familheey.app.Utilities.NonSwipeableViewPager;
import com.familheey.app.Utilities.SharedPref;
import com.familheey.app.Utilities.Utilities;
import com.familheey.app.membership.MembershipFragment;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.shape.CornerFamily;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;
import static com.familheey.app.Utilities.Constants.ApiFlags.GET_FAMILY_DETAILS;
import static com.familheey.app.Utilities.Constants.ApiPaths.IMAGE_BASE_URL;
import static com.familheey.app.Utilities.Constants.ApiPaths.S3_DEV_IMAGE_URL_COVER;
import static com.familheey.app.Utilities.Constants.ApiPaths.S3_DEV_IMAGE_URL_SQUARE;
import static com.familheey.app.Utilities.Constants.Bundle.DATA;
import static com.familheey.app.Utilities.Constants.Bundle.DETAIL;
import static com.familheey.app.Utilities.Constants.Bundle.GLOBAL_SEARCH;
import static com.familheey.app.Utilities.Constants.Bundle.ID;
import static com.familheey.app.Utilities.Constants.Bundle.IDENTIFIER;
import static com.familheey.app.Utilities.Constants.Bundle.IS_CREATED_NOW;
import static com.familheey.app.Utilities.Constants.Bundle.LINKED_FAMILIES;
import static com.familheey.app.Utilities.Constants.Bundle.LINK_FAMILY_REQUEST;
import static com.familheey.app.Utilities.Constants.Bundle.MEMBER;
import static com.familheey.app.Utilities.Constants.Bundle.REQUEST;
import static com.familheey.app.Utilities.Constants.Bundle.TYPE;
import static com.familheey.app.Utilities.Constants.FamilyDashboardIdentifiers.TypeAboutUsFragment;
import static com.familheey.app.Utilities.Constants.FamilyDashboardIdentifiers.TypeAlbumFragment;
import static com.familheey.app.Utilities.Constants.FamilyDashboardIdentifiers.TypeAnnouncementListingFragment;
import static com.familheey.app.Utilities.Constants.FamilyDashboardIdentifiers.TypeFamilyEventFragment;
import static com.familheey.app.Utilities.Constants.FamilyDashboardIdentifiers.TypeFamilyMembership;
import static com.familheey.app.Utilities.Constants.FamilyDashboardIdentifiers.TypeFamilyNeedsListingFragment;
import static com.familheey.app.Utilities.Constants.FamilyDashboardIdentifiers.TypeFamilyPostFeedFragment;
import static com.familheey.app.Utilities.Constants.FamilyDashboardIdentifiers.TypeFamilyRequestsFragment;
import static com.familheey.app.Utilities.Constants.FamilyDashboardIdentifiers.TypeFamilySubscriptionFragment;
import static com.familheey.app.Utilities.Constants.FamilyDashboardIdentifiers.TypeFoldersFragment;
import static com.familheey.app.Utilities.Constants.FamilyDashboardIdentifiers.TypeLinkedFamilyFragment;
import static com.familheey.app.Utilities.Constants.FamilyDashboardIdentifiers.TypeLinkingFragment;
import static com.familheey.app.Utilities.Constants.ImageUpdate.FAMILY_COVER;
import static com.familheey.app.Utilities.Constants.ImageUpdate.FAMILY_LOGO;
import static com.familheey.app.Utilities.Constants.Paths.COVER_PIC;
import static com.familheey.app.Utilities.Constants.Paths.LOGO;

public class FamilyDashboardFragment extends Fragment implements TabLayout.OnTabSelectedListener, RetrofitListener, FamilyDashboardInteractor, FamilyEditDialogFragment.OnFamilyEditCompleted, ProgressListener, FamilyCreationLevelAdvancedFragment.OnFamilyCreationListener {


    @BindView(R.id.btn_paynow)
    MaterialButton btn_paynow;
    @BindView(R.id.txt_mtype)
    TextView txt_mtype;
    @BindView(R.id.txt_mduration)
    TextView txt_mduration;
    @BindView(R.id.txt_sdate)
    TextView txt_sdate;
    @BindView(R.id.txt_tilldate)
    TextView txt_tilldate;
    @BindView(R.id.txt_fees)
    TextView txt_fees;

    @BindView(R.id.img_payment_status)
    ImageView img_payment_status;
    @BindView(R.id.member_view)
    LinearLayout member_view;
    @BindView(R.id.view_due)
    LinearLayout view_due;
    @BindView(R.id.familyLogo)
    ImageView familyLogo;
    @BindView(R.id.familyName)
    TextView familyName;
    @BindView(R.id.txt_member)
    TextView txt_member;
    @BindView(R.id.familyDescription)
    TextView familyDescription;
    @BindView(R.id.familyLocation)
    TextView familyLocation;
    @BindView(R.id.familySettings)
    ImageView familySettings;
    @BindView(R.id.collapsingToolbar)
    CollapsingToolbarLayout collapsingToolbar;
    @BindView(R.id.appBar)
    AppBarLayout appBar;
    @BindView(R.id.myFamiliesTab)
    TabLayout myFamiliesTab;
    @BindView(R.id.myFamiliesDetailsViewPager)
    NonSwipeableViewPager myFamiliesDetailsViewPager;
    @BindView(R.id.goToSubscription)
    FloatingActionButton goToSubscription;
    @BindView(R.id.txtFolders)
    TextView txtFolders;
    @BindView(R.id.cancel_bottom)
    TextView cancel;

    @BindView(R.id.etxt_due_paid)
    EditText etxt_due_paid;
    @BindView(R.id.txt_paid_fees)
    TextView txt_paid_fees;
    @BindView(R.id.unfollow)
    TextView unfollow;
    @BindView(R.id.info_view)
    LinearLayout info_view;
    @BindView(R.id.txt_note)
    TextView txt_note;
    @BindView(R.id.bottom_sheet)
    LinearLayout bottomSheet;
    @BindView(R.id.bg)
    View bg;
    @BindView(R.id.bottom_sheet_more)
    ConstraintLayout bottomSheetMore;

    @BindView(R.id.backgroundCover)
    ImageView backgroundCover;

    @BindView(R.id.link_families)
    TextView linkFamilies;
    @BindView(R.id.editFamily)
    ImageView editFamily;
    @BindView(R.id.coverEdit)
    ImageView coverEdit;
    @BindView(R.id.parentCard)
    MaterialCardView parentCard;

    @BindView(R.id.membersCount)
    TextView membersCount;
    @BindView(R.id.familiesContainer)
    LinearLayout familiesContainer;
    @BindView(R.id.knownMembersCount)
    TextView knownMembersCount;
    @BindView(R.id.knownMembersContainer)
    LinearLayout knownMembersContainer;
    @BindView(R.id.eventsCount)
    TextView eventsCount;
    @BindView(R.id.eventsContainer)
    LinearLayout eventsContainer;
    @BindView(R.id.joinFamily)
    MaterialButton joinFamily;
    @BindView(R.id.imgMore)
    ImageView imgMore;
    @BindView(R.id.toolBarTitle)
    TextView toolBarTitle;

    @BindView(R.id.progressFamily)
    ProgressBar progressFamily;

    @BindView(R.id.addFamilyComponent)
    CardView addFamilyComponent;

    private BottomSheetBehavior sheetBehaviorMore;

    public Family family;

    private FamilyDashboardListener mListener;

    private FamilyDashboardTabAdapter familyDetailsPagerAdapter;
    private String notificationType = "";
    private String pay = "";
    private Boolean isCreatedNow = false;
    private boolean editStatus = false;

    public FamilyDashboardFragment() {
        // Required empty public constructor
    }

    public static FamilyDashboardFragment newInstance(Family family, Boolean isCreatedNow) {
        FamilyDashboardFragment fragment = new FamilyDashboardFragment();
        Bundle args = new Bundle();
        args.putParcelable(DATA, family);
        args.putBoolean(IS_CREATED_NOW, isCreatedNow);
        fragment.setArguments(args);
        return fragment;
    }

    public static FamilyDashboardFragment newInstance(Family family, String type, String pay) {
        FamilyDashboardFragment fragment = new FamilyDashboardFragment();
        Bundle args = new Bundle();
        args.putParcelable(DATA, family);
        args.putString(TYPE, type);
        args.putString("PAY", pay);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            pay = getArguments().getString("PAY", "");
            family = getArguments().getParcelable(DATA);
            notificationType = getArguments().getString(TYPE, "0");
            isCreatedNow = getArguments().getBoolean(IS_CREATED_NOW, false);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_family_dashboard, container, false);
        ButterKnife.bind(this, view);
        setSmallData();
        return view;
    }


    private void setSmallData() {

        ThreadUtils.runOnUiThread(() -> {
            if (family.getLogo() != null) {
                Glide.with(FamilyDashboardFragment.this.requireContext())
                        .load(S3_DEV_IMAGE_URL_SQUARE + IMAGE_BASE_URL + LOGO + family.getLogo())
                        .apply(Utilities.getCurvedRequestOptions())
                        .placeholder(R.drawable.family_logo)
                        .transition(withCrossFade(Utilities.getDrawableCrossFadeFactory()))
                        .into(familyLogo);
            }
            if (family.getGroupName() != null)
                familyName.setText(family.getGroupName());
            if (family.getBaseRegion() != null)
                familyLocation.setText(family.getBaseRegion());

            if (family.getCreatedByName() != null && family.getGroupType() != null)
                familyDescription.setText(family.getGroupCategory() + ", By " + family.getCreatedByName());
            else if (family.getCreatedByName() == null && family.getGroupType() == null)
                familyDescription.setVisibility(View.GONE);
            else if (family.getCreatedByName() != null)
                familyDescription.setText("By " + family.getCreatedByName());
            else
                familyDescription.setText(family.getGroupCategory());

            if (family.getMembersCount() != null) {
                membersCount.setText(family.getMembersCount());
            }
            if (family.getPostCount() != null)
                eventsCount.setText(family.getPostCount());

            getFamilyDetails();
            initBottomSheet();
            applyStyle();

        });


    }

    private void initBottomSheet() {
        sheetBehaviorMore = BottomSheetBehavior.from(bottomSheetMore);
        sheetBehaviorMore.setHideable(true);
        sheetBehaviorMore.setPeekHeight(0);

        sheetBehaviorMore.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (BottomSheetBehavior.STATE_COLLAPSED == newState) {
                    bg.setVisibility(View.GONE);
                    addFamilyComponent.setVisibility(View.VISIBLE);
                } else bg.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
    }

    private void applyStyle() {
        float radius = getResources().getDimension(R.dimen.card_top_radius);
        parentCard.setShapeAppearanceModel(
                parentCard.getShapeAppearanceModel()
                        .toBuilder()
                        .setTopLeftCorner(CornerFamily.ROUNDED, radius)
                        .setTopRightCorner(CornerFamily.ROUNDED, radius)
                        .setBottomRightCorner(CornerFamily.ROUNDED, 0)
                        .setBottomLeftCornerSize(0)
                        .build());
    }

    private void initRestrictions() {
        int memtab = Objects.requireNonNull(myFamiliesDetailsViewPager.getAdapter()).getCount() - 1;

        if (family.getUserStatus().equalsIgnoreCase("admin")) {
            editFamily.setVisibility(View.VISIBLE);
            coverEdit.setVisibility(View.VISIBLE);
            familySettings.setVisibility(View.VISIBLE);
            joinFamily.setVisibility(View.GONE);
            knownMembersContainer.setVisibility(View.GONE);
        } else if (family.getUserStatus().equalsIgnoreCase("not-member")) {
            editFamily.setVisibility(View.INVISIBLE);
            coverEdit.setVisibility(View.INVISIBLE);
            familySettings.setVisibility(View.INVISIBLE);
            joinFamily.setVisibility(View.VISIBLE);
            knownMembersContainer.setVisibility(View.VISIBLE);
        } else {
            editFamily.setVisibility(View.INVISIBLE);
            coverEdit.setVisibility(View.INVISIBLE);
            familySettings.setVisibility(View.VISIBLE);
            joinFamily.setVisibility(View.GONE);
            knownMembersContainer.setVisibility(View.GONE);
        }
        if (isCreatedNow) {
            myFamiliesTab.removeOnTabSelectedListener(this);
            myFamiliesDetailsViewPager.setCurrentItem(memtab);
            myFamiliesTab.addOnTabSelectedListener(this);
        }
        if (!family.getUserStatus().equalsIgnoreCase("not-member")) {
            switch (notificationType) {
                case MEMBER:
                    myFamiliesTab.removeOnTabSelectedListener(this);
                    myFamiliesDetailsViewPager.setCurrentItem(memtab);
                    myFamiliesTab.addOnTabSelectedListener(this);
                    break;
                case REQUEST:
                    myFamiliesTab.removeOnTabSelectedListener(this);
                    myFamiliesDetailsViewPager.setCurrentItem(memtab);
                    myFamiliesTab.addOnTabSelectedListener(this);
                    FamilySubscriptionFragment familySubscriptionFragment = (FamilySubscriptionFragment) familyDetailsPagerAdapter.getItem(memtab);
                    if (familySubscriptionFragment != null)
                        familySubscriptionFragment.displayMemberRequest();
                    break;
                case LINK_FAMILY_REQUEST:
                    myFamiliesTab.removeOnTabSelectedListener(this);
                    myFamiliesDetailsViewPager.setCurrentItem(7);
                    myFamiliesTab.addOnTabSelectedListener(this);
                    LinkingFragment linkingFragment = (LinkingFragment) familyDetailsPagerAdapter.getItem(7);
                    if (linkingFragment != null)
                        linkingFragment.displayLinkRequest();
                    break;
                case LINKED_FAMILIES:
                    myFamiliesTab.removeOnTabSelectedListener(this);
                    myFamiliesDetailsViewPager.setCurrentItem(7);
                    myFamiliesTab.addOnTabSelectedListener(this);
                    break;
                case DETAIL:
                    myFamiliesTab.removeOnTabSelectedListener(this);
                    myFamiliesDetailsViewPager.setCurrentItem(5);
                    myFamiliesTab.addOnTabSelectedListener(this);
                    break;
            }
        }
        if (family.getMemberJoining() != null && family.getMemberJoining().equalsIgnoreCase("1"))
            joinFamily.setText("Private");
        else if (family.getInvitationStatus() != null && family.getInvitationStatus()) {
            joinFamily.setText("Pending");
        } else {
            joinFamily.setText("Join");
        }
    }

    LinkFamilyAdapter linkFamilyAdapter;
    ArrayList<LinkfamilyList> linkfamilyLists = new ArrayList<>();

    private void followtheFamily() {
        JsonObject jsonObject = new JsonObject();
        ApiServiceProvider apiServiceProvider = ApiServiceProvider.getInstance(getContext());
        jsonObject.addProperty("group_id", family.getId().toString());
        jsonObject.addProperty("user_id", SharedPref.getUserRegistration().getId());
        apiServiceProvider.followtheFamily(jsonObject, null, this);
    }


    private void unFollowtheFamily() {
        JsonObject jsonObject = new JsonObject();
        ApiServiceProvider apiServiceProvider = ApiServiceProvider.getInstance(getContext());
        jsonObject.addProperty("group_id", family.getId().toString());
        jsonObject.addProperty("user_id", SharedPref.getUserRegistration().getId());
        apiServiceProvider.unFollowtheFamily(jsonObject, null, this);


    }


    private void openBottomSheetMore() {
        if (sheetBehaviorMore.getState() != BottomSheetBehavior.STATE_EXPANDED) {
            dataSetInBottomSheet();
            sheetBehaviorMore.setState(BottomSheetBehavior.STATE_EXPANDED);
            addFamilyComponent.setVisibility(View.GONE);
        } else {
            sheetBehaviorMore.setState(BottomSheetBehavior.STATE_COLLAPSED);
            addFamilyComponent.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FamilyDashboardListener) {
            mListener = (FamilyDashboardListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement FamilyDashboardListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    private String dateFormat(Long value, String format) {
        if (value != null && value > 100) {
            value = TimeUnit.SECONDS.toMillis(value);
            DateTime dateTime = new DateTime(value);
            DateTimeFormatter dtfOut = DateTimeFormat.forPattern(format);
            return dtfOut.print(dateTime);
        }
        return "";
    }

    private void setMemberShipdata() {
        if (family.getMembership_type() != null && family.getMembership_to() != null) {
            if (!family.getMembership_type().isEmpty() && !family.getMembership_to().isEmpty()/*&&family.getIs_membership()*/) {
                member_view.setVisibility(View.VISIBLE);
                member_view.setVisibility(View.VISIBLE);
                if ("Pending".equals(family.getMembership_payment_status()) || "Partial".equals(family.getMembership_payment_status())) {
                    img_payment_status.setVisibility(View.VISIBLE);
                } else {
                    img_payment_status.setVisibility(View.GONE);
                }
            }
            if (family.getMembership_period_type_id() == 4) {
                txt_member.setText(family.getMembership_type() + ", " + family.getMembership_sub_type());
            } else
                txt_member.setText(family.getMembership_type() + ", till " + dateFormat(Long.parseLong(family.getMembership_to()), "MMM dd yyyy"));
        }
        if (pay.equals("PAY")) {
            pay = "";
            member_view.performClick();
        }
    }

    private void initializeTabs(Family family) {
        try {
            familyDetailsPagerAdapter = new FamilyDashboardTabAdapter(getChildFragmentManager());
            if (family.getUserStatus().equalsIgnoreCase("not-member")) {//Not Member
                if (family.isPublic()) {//Family is Public
                    familyDetailsPagerAdapter.addFragment("Feed", FamilyPostFeedFragment.newInstance(family.getId(), family.isAdmin(), family.canCreatePost(), family.getPostCreate(), family.isMember()));
                    familyDetailsPagerAdapter.addFragment("Announcements", AnnouncementListingFragment.newInstance(family.getId() + ""));
                    familyDetailsPagerAdapter.addFragment("About Us", AboutUsFragment.newInstance(family));
                } else {//Family is Private
                    familyDetailsPagerAdapter.addFragment("About Us", AboutUsFragment.newInstance(family));
                }
            } else {
                familyDetailsPagerAdapter.addFragment("Feed", FamilyPostFeedFragment.newInstance(family.getId(), family.isAdmin(), family.canCreatePost(), family.getPostCreate(), family.isMember()));
                familyDetailsPagerAdapter.addFragment("Announcements", AnnouncementListingFragment.newInstance(family.getId() + ""));
                familyDetailsPagerAdapter.addFragment("Requests", FamilyNeedsListingFragment.newInstance(family.getId() + "", family.getPostCreate(), family.getStripe_account_id(), family.isAdmin(), family.getGroupName()));
                familyDetailsPagerAdapter.addFragment("Events", FamilyEventFragment.newInstance(family));
                familyDetailsPagerAdapter.addFragment("Albums", AlbumFragment.newInstance(Constants.FileUpload.TYPE_FAMILY, family.getId().toString(), family.isAdmin()));
                familyDetailsPagerAdapter.addFragment("About Us", AboutUsFragment.newInstance(family));
                familyDetailsPagerAdapter.addFragment("Documents", FoldersFragment.newInstance(family.getId().toString(), Constants.FileUpload.TYPE_FAMILY));
                familyDetailsPagerAdapter.addFragment("Linked Families", LinkingFragment.newInstance(family));
                if (family.getUserStatus().equalsIgnoreCase("admin") && family.getIs_membership() != null && family.getIs_membership()) {
                    familyDetailsPagerAdapter.addFragment("Membership", MembershipFragment.newInstance(family.getId().toString()));
                }
                familyDetailsPagerAdapter.addFragment("Members", FamilySubscriptionFragment.newInstance(family));
            }
            myFamiliesDetailsViewPager.setAdapter(familyDetailsPagerAdapter);
            myFamiliesTab.setupWithViewPager(myFamiliesDetailsViewPager);
            myFamiliesDetailsViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(myFamiliesTab));
            myFamiliesDetailsViewPager.setOffscreenPageLimit(myFamiliesTab.getTabCount());
            myFamiliesTab.addOnTabSelectedListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick({R.id.btn_paynow, R.id.btn_cancel, R.id.member_view, R.id.txt_member, R.id.editFamily, R.id.coverEdit, R.id.familySettings, R.id.goBack, R.id.backgroundCover, R.id.familiesContainer, R.id.eventsContainer, R.id.knownMembersContainer, R.id.familyName, R.id.familyLogo, R.id.imgMore, R.id.joinFamily, R.id.addFamilyComponent})
    public void onViewClicked(View v) {
        switch (v.getId()) {
            case R.id.btn_paynow:
                if (!etxt_due_paid.getText().toString().equals("")) {
                    int amt = Integer.parseInt(etxt_due_paid.getText().toString());
                    if (amt > 0) {
                        openBottomSheetMore();
                        String acc = "";
                        if (family.getStripe_account_id() != null) {
                            acc = family.getStripe_account_id();
                        }
                        startActivityForResult(new Intent(getActivity(), StripeActivity.class)
                                .putExtra("ITEMID", "")
                                .putExtra("TYPE", "membership")
                                .putExtra("CID", family.getGroup_map_id())
                                .putExtra("RID", family.getGroup_map_id())
                                .putExtra("ID", family.getId().toString())
                                .putExtra("ACC", acc)
                                .putExtra("AMT", etxt_due_paid.getText().toString()), 1001);

                    }
                }

                break;
            case R.id.editFamily:
            case R.id.familyLogo:
                if (family.getUserStatus() == null)
                    return;
                Intent familyLogoEditIntent = new Intent(getContext(), ImageChangerActivity.class);
                familyLogoEditIntent.putExtra(DATA, family);
                familyLogoEditIntent.putExtra(IDENTIFIER, family.getUserStatus().equalsIgnoreCase("admin"));
                familyLogoEditIntent.putExtra(TYPE, FAMILY_LOGO);
                startActivityForResult(familyLogoEditIntent, ImageChangerActivity.REQUEST_CODE);
                break;
            case R.id.coverEdit:
            case R.id.backgroundCover:

                if (family.getUserStatus() == null)
                    return;
                Intent familyCoverEditIntent = new Intent(getContext(), ImageChangerActivity.class);
                familyCoverEditIntent.putExtra(DATA, family);
                familyCoverEditIntent.putExtra(IDENTIFIER, family.getUserStatus().equalsIgnoreCase("admin"));
                familyCoverEditIntent.putExtra(TYPE, FAMILY_COVER);
                startActivityForResult(familyCoverEditIntent, ImageChangerActivity.REQUEST_CODE);
                break;
            case R.id.familySettings:
                mListener.loadFamilySettings(family);
                break;
            case R.id.goBack:
                goBack();
                break;
            case R.id.eventsContainer:
                if (!family.isAnonymous()) {
                    myFamiliesDetailsViewPager.setCurrentItem(0);
                } else if (family.getPostVisibilty() != null && family.getPostVisibilty().equalsIgnoreCase(Constants.FamilySettings.PostVisibility.PUBLIC)) {
                    myFamiliesDetailsViewPager.setCurrentItem(0);
                } else
                    Toast.makeText(getContext(), "You don't have authorization to view posts", Toast.LENGTH_SHORT).show();
                break;
            case R.id.familiesContainer:
                if (!family.isAnonymous()) {
                    int memtab = Objects.requireNonNull(myFamiliesDetailsViewPager.getAdapter()).getCount() - 1;
                    myFamiliesDetailsViewPager.setCurrentItem(memtab);
                } else {
                    Toast.makeText(getContext(), "You need to be a member to view this family", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.knownMembersContainer:
                Intent mutualConnectionsIntent = new Intent(getActivity(), UserConnectionsActivity.class);
                mutualConnectionsIntent.putExtra(TYPE, UserConnectionsActivity.FAMILY_CONNECTIONS);
                mutualConnectionsIntent.putExtra(ID, SharedPref.getUserRegistration().getId());
                mutualConnectionsIntent.putExtra(Constants.Bundle.FAMILY_ID, family.getId().toString());
                startActivity(mutualConnectionsIntent);
                break;
            case R.id.familyName:
                //mListener.loadFileUploading(family);
                break;
            case R.id.btn_cancel:
            case R.id.txt_member:
            case R.id.member_view:
                openBottomSheetMore();
                break;
            case R.id.joinFamily:
                if (family.getJoinedStatus() != null && family.getJoinedStatus().equalsIgnoreCase("pending")) {
                    Toast.makeText(getContext(), "You have already requested to join this family", Toast.LENGTH_SHORT).show();
                    return;
                } else if (family.getMemberJoining() != null && family.getMemberJoining().equalsIgnoreCase("1")) {
                    Toast.makeText(getContext(), "You can not join a private family", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (family.isAnonymous()) {
                    requestJoinFamily();
                }
                break;
            case R.id.addFamilyComponent:
                try {
                    Fragment selectedFragment = familyDetailsPagerAdapter.getItem(myFamiliesDetailsViewPager.getCurrentItem());
                    if (selectedFragment instanceof FamilyPostFeedFragment) {
                        FamilyPostFeedFragment familyPostFeedFragment = ((FamilyPostFeedFragment) selectedFragment);

                        familyPostFeedFragment.createNewPost();
                    } else if (selectedFragment instanceof AnnouncementListingFragment) {
                        AnnouncementListingFragment announcementListingFragment = ((AnnouncementListingFragment) selectedFragment);
                        announcementListingFragment.createNewAnnouncement();
                    } else if (selectedFragment instanceof FamilyEventFragment) {
                        FamilyEventFragment familyEventFragment = ((FamilyEventFragment) selectedFragment);

                        familyEventFragment.createNewEvent();
                    } else if (selectedFragment instanceof AlbumFragment) {
                        AlbumFragment albumFragment = ((AlbumFragment) selectedFragment);
                        albumFragment.createNewAlbum();
                    } else if (selectedFragment instanceof FoldersFragment) {
                        FoldersFragment foldersFragment = ((FoldersFragment) selectedFragment);
                        foldersFragment.createNewFolder();
                    } else if (selectedFragment instanceof LinkingFragment) {
                        LinkingFragment linkingFragment = ((LinkingFragment) selectedFragment);
                        linkingFragment.createNewFamilyLink();
                    } else if (selectedFragment instanceof FamilySubscriptionFragment) {
                        FamilySubscriptionFragment familySubscriptionFragment = ((FamilySubscriptionFragment) selectedFragment);
                        familySubscriptionFragment.createNewMember();
                    } else if (selectedFragment instanceof FamilyNeedsListingFragment) {
                        FamilyNeedsListingFragment familyNeedsListingFragment = ((FamilyNeedsListingFragment) selectedFragment);
                        familyNeedsListingFragment.createNewNeed();
                    } else if (selectedFragment instanceof MembershipFragment) {
                        MembershipFragment membershipFragment = ((MembershipFragment) selectedFragment);
                        membershipFragment.createMemberShipType();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }
    }

    public void goBack() {
        if (notificationType.equalsIgnoreCase("0")) {
            requireActivity().supportFinishAfterTransition();
        } else if (notificationType.equalsIgnoreCase(GLOBAL_SEARCH)) {
            Intent returnIntent = new Intent();
            if (editStatus)
                getActivity().setResult(Activity.RESULT_OK, returnIntent);
            else
                getActivity().setResult(Activity.RESULT_CANCELED, returnIntent);
            requireActivity().finish();
            getActivity().supportFinishAfterTransition();
        } else {
            Intent intent = new Intent(getContext(), MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            requireActivity().supportFinishAfterTransition();
        }
    }

    private void initListeners() {
        goToSubscription.setOnClickListener(view -> {
            // openBottomSheet();
        });

        txtFolders.setOnClickListener(v -> {
            mListener.loadFileUploading(family);
        });
        //  cancel.setOnClickListener(view -> openBottomSheet());
        if (family.getFollowing() != null && family.getFollowing())
            unfollow.setText(R.string.un_follow);
        else
            unfollow.setText(R.string.follow);
        unfollow.setOnClickListener(view -> {
            //  openBottomSheet();
            if (family.getFollowing() != null && family.getFollowing()) {
                unFollowtheFamily();
            } else {
                followtheFamily();
            }
        });


        if (family.getLinkFamily().contains("14")) {

            linkFamilies.setVisibility(View.GONE);

        }

        linkFamilies.setOnClickListener(v -> {
            //  openBottomSheet();
            Intent intent = new Intent(getContext(), LinkedFamilyActivity.class);
            intent.putExtra(DATA, family);
            startActivity(intent);
        });

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        appBar.setExpanded(false);
        myFamiliesDetailsViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    public void refreshFamilyDetails(Family family) {
        this.family = family;
        getFamilyDetails();
    }

    public void getFamilyDetails() {
//        mListener.showProgressDialog();
        showProgressBar();
        JsonObject jsonObject = new JsonObject();
        ApiServiceProvider apiServiceProvider = ApiServiceProvider.getInstance(getContext());
        jsonObject.addProperty("group_id", family.getId().toString());
        jsonObject.addProperty("user_id", SharedPref.getUserRegistration().getId());
        apiServiceProvider.getFamilyDetailsByID(jsonObject, null, this);
    }

    public void getFamilyDetailsAsNew() {
        showProgressBar();
        JsonObject jsonObject = new JsonObject();
        ApiServiceProvider apiServiceProvider = ApiServiceProvider.getInstance(getContext());
        jsonObject.addProperty("group_id", family.getId().toString());
        jsonObject.addProperty("user_id", SharedPref.getUserRegistration().getId());
        apiServiceProvider.getFamilyDetailsByID(jsonObject, new ApiCallbackParams(), this);
    }

    private void showProgressBar() {
        if (progressFamily != null) {
            progressFamily.setVisibility(View.VISIBLE);
        }

    }

    private void hideProgressBar() {
        if (progressFamily != null) {
            progressFamily.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResponseSuccess(String responseBodyString, ApiCallbackParams apiCallbackParams, int apiFlag) {
        if (mListener != null)
            mListener.hideProgressDialog();
        switch (apiFlag) {
            case Constants.ApiFlags.FOLLOW:
                Toast.makeText(getActivity(), family.getGroupName() + " followed", Toast.LENGTH_SHORT).show();
                getFamilyDetails();
                break;
            case Constants.ApiFlags.UNFOLLOW:
                getFamilyDetails();
                Toast.makeText(getActivity(), family.getGroupName() + " unfollowed", Toast.LENGTH_SHORT).show();
                break;

            case Constants.ApiFlags.FETCH_FAMILY_FOR_LINKING:
                linkfamilyLists.clear();
                linkfamilyLists.addAll(FamilyParser.parseLinkFamilyList(responseBodyString));
                linkFamilyAdapter.notifyDataSetChanged();
                break;
            default:
                hideProgressBar();
                try {
                    if (apiCallbackParams != null) {
                        familyDetailsPagerAdapter = null;
                    }
                    if (FamilyParser.parseLinkedFamilies(responseBodyString) != null && FamilyParser.parseLinkedFamilies(responseBodyString).size() == 0) {
                        SweetAlertDialog contentNotFoundDialog = Utilities.getContentNotFoundDialog(getContext());
                        contentNotFoundDialog.setConfirmClickListener(sweetAlertDialog -> {
                            requireActivity().finish();
                        });
                        contentNotFoundDialog.setCanceledOnTouchOutside(false);
                        contentNotFoundDialog.setCancelable(false);
                        contentNotFoundDialog.show();
                        Utilities.addPositiveButtonMargin(contentNotFoundDialog);
                        return;
                    }
                    family = FamilyParser.parseLinkedFamilies(responseBodyString).get(0);
                    setMemberShipdata();
                    if (familyDetailsPagerAdapter == null) {
                        initializeTabs(family);
                        initRestrictions();
                    } else {
                        initRestrictions();
                    }
                    fillFamilyBasicDetails();
                    updateFragment(family);
                    membersCount.setText(FamilyParser.getFamilyMembers(responseBodyString));
                    eventsCount.setText(FamilyParser.getPostCount(responseBodyString));
                    knownMembersCount.setText(FamilyParser.getKnownConnections(responseBodyString));
                    initListeners();
                } catch (JsonParseException | NullPointerException | ArrayIndexOutOfBoundsException e) {
                    e.printStackTrace();
                    if (mListener != null)
                        mListener.showErrorDialog("Something went wrong please try again!!");
                }
        }
    }

    @Override
    public void onResponseError(ErrorData errorData, ApiCallbackParams apiCallbackParams, Throwable throwable, int apiFlag) {
        if (mListener != null)
            mListener.hideProgressDialog();

        if (apiFlag == GET_FAMILY_DETAILS) {
            hideProgressBar();

            try {
                if (errorData.getCode() == 500) {
                    SweetAlertDialog contentNotFoundDialog = Utilities.getContentNotFoundDialog(getContext());
                    contentNotFoundDialog.setConfirmClickListener(sweetAlertDialog -> {
                        getActivity().finish();
                    });
                    contentNotFoundDialog.setCanceledOnTouchOutside(false);
                    contentNotFoundDialog.setCancelable(false);
                    contentNotFoundDialog.show();
                    Utilities.addPositiveButtonMargin(contentNotFoundDialog);
                } else
                    networkError();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void updateFamily(int TYPE, Family family) {
        FamilyEditDialogFragment.newInstance(TYPE, family).show(getChildFragmentManager(), "FamilyEditDialogFragment");
    }

    @Override
    public void onFamilyAddComponentVisible(int type) {
        try {
            Fragment selectedFragment = familyDetailsPagerAdapter.getItem(myFamiliesDetailsViewPager.getCurrentItem());
            switch (type) {
                case TypeFamilySubscriptionFragment:
                /*if (selectedFragment instanceof FamilySubscriptionFragment) {
                    FamilySubscriptionFragment familySubscriptionFragment = ((FamilySubscriptionFragment) selectedFragment);
                    if (familySubscriptionFragment != null)
                        addFamilyComponent.setVisibility(View.VISIBLE);
                }*/
                    addFamilyComponent.setVisibility(View.VISIBLE);
                    break;
                case TypeAlbumFragment:
                    if (selectedFragment instanceof AlbumFragment) {
                        AlbumFragment albumFragment = ((AlbumFragment) selectedFragment);
                        addFamilyComponent.setVisibility(View.VISIBLE);
                    }
                    break;
                case TypeAboutUsFragment:
                    addFamilyComponent.setVisibility(View.VISIBLE);
                    break;
                case TypeFamilyEventFragment:
                    if (selectedFragment instanceof FamilyEventFragment) {
                        addFamilyComponent.setVisibility(View.VISIBLE);
                    }
                    break;
                case TypeFoldersFragment:
                    if (selectedFragment instanceof FoldersFragment) {
                        addFamilyComponent.setVisibility(View.VISIBLE);
                    }
                    break;
                case TypeFamilyNeedsListingFragment:
                    if (selectedFragment instanceof FamilyNeedsListingFragment) {
                        addFamilyComponent.setVisibility(View.VISIBLE);
                    }
                    break;
                case TypeAnnouncementListingFragment:
                    if (selectedFragment instanceof AnnouncementListingFragment) {
                        addFamilyComponent.setVisibility(View.VISIBLE);
                    }
                    break;
                case TypeFamilyPostFeedFragment:
                    if (selectedFragment instanceof FamilyPostFeedFragment) {
                        if (sheetBehaviorMore.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                            addFamilyComponent.setVisibility(View.VISIBLE);
                        }


                    }
                    break;
                case TypeLinkingFragment:
                case TypeLinkedFamilyFragment:
                case TypeFamilyRequestsFragment:
                    if (selectedFragment instanceof LinkingFragment) {
                        addFamilyComponent.setVisibility(View.VISIBLE);
                    }
                    break;
                case TypeFamilyMembership:
                    addFamilyComponent.setVisibility(View.VISIBLE);
                    break;
                default:
                    addFamilyComponent.setVisibility(View.VISIBLE);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFamilyAddComponentHidden(int type) {
        try {
            Fragment selectedFragment = familyDetailsPagerAdapter.getItem(myFamiliesDetailsViewPager.getCurrentItem());
            switch (type) {
                case TypeFamilySubscriptionFragment:
                    addFamilyComponent.setVisibility(View.INVISIBLE);
                    break;
                case TypeAlbumFragment:
                    if (selectedFragment instanceof AlbumFragment) {
                        addFamilyComponent.setVisibility(View.INVISIBLE);
                    }
                    break;
                case TypeAboutUsFragment:
                    addFamilyComponent.setVisibility(View.INVISIBLE);
                    break;
                case TypeFamilyEventFragment:
                    if (selectedFragment instanceof FamilyEventFragment) {
                        addFamilyComponent.setVisibility(View.INVISIBLE);
                    }
                    break;
                case TypeFoldersFragment:
                    if (selectedFragment instanceof FoldersFragment) {
                        addFamilyComponent.setVisibility(View.INVISIBLE);
                    }
                    break;
                case TypeFamilyNeedsListingFragment:
                    if (selectedFragment instanceof FamilyNeedsListingFragment) {
                        addFamilyComponent.setVisibility(View.INVISIBLE);
                    }
                    break;
                case TypeAnnouncementListingFragment:
                    if (selectedFragment instanceof AnnouncementListingFragment) {
                        addFamilyComponent.setVisibility(View.INVISIBLE);
                    }
                    break;
                case TypeFamilyPostFeedFragment:
                    if (selectedFragment instanceof FamilyPostFeedFragment) {
                        addFamilyComponent.setVisibility(View.INVISIBLE);
                    }
                    break;
                case TypeLinkingFragment:
                case TypeLinkedFamilyFragment:
                case TypeFamilyRequestsFragment:
                    if (selectedFragment instanceof LinkingFragment) {
                        addFamilyComponent.setVisibility(View.INVISIBLE);
                    }
                    break;
                default:
                    addFamilyComponent.setVisibility(View.INVISIBLE);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateFragment(Family family) {
        if (family == null) return;
        collapsingToolBarBehaviour();
        LinkingFragment linkedFamilyFragment = familyDetailsPagerAdapter.getInstance(LinkingFragment.class);
        if (linkedFamilyFragment != null)
            linkedFamilyFragment.updateFamily(family);
        FamilySubscriptionFragment familySubscriptionFragment = familyDetailsPagerAdapter.getInstance(FamilySubscriptionFragment.class);
        if (familySubscriptionFragment != null)
            familySubscriptionFragment.fillDetails(family);
        AlbumFragment albumFragment = familyDetailsPagerAdapter.getInstance(AlbumFragment.class);
        if (albumFragment != null)
            albumFragment.updateAdminStatus(family.isAdmin(), family.isAdmin() || family.canCreatePost(), family.isAdmin(), SharedPref.getUserRegistration().getId());
        AboutUsFragment aboutUsFragment = familyDetailsPagerAdapter.getInstance(AboutUsFragment.class);
        if (aboutUsFragment != null)
            aboutUsFragment.fillDetails(family);
        FamilyEventFragment familyEventFragment = familyDetailsPagerAdapter.getInstance(FamilyEventFragment.class);
        if (familyEventFragment != null)
            familyEventFragment.updateFamily(family);
        FoldersFragment foldersFragment = familyDetailsPagerAdapter.getInstance(FoldersFragment.class);
        if (foldersFragment != null)
            foldersFragment.updateAdminStatus(family.isAdmin(), family.isAdmin() || family.canCreatePost(), family.isAdmin(), SharedPref.getUserRegistration().getId());
        FamilyPostFeedFragment familyPostFeedFragment = familyDetailsPagerAdapter.getInstance(FamilyPostFeedFragment.class);
        if (familyPostFeedFragment != null)
            familyPostFeedFragment.updateFamily(family);
        AnnouncementListingFragment announcementListingFragment = familyDetailsPagerAdapter.getInstance(AnnouncementListingFragment.class);
        if (announcementListingFragment != null)
            announcementListingFragment.updateFamily(family);


        if (family.getFollowing() != null && family.getFollowing())
            unfollow.setText(R.string.un_follow);
        else
            unfollow.setText(R.string.follow);

        if (family.getLinkFamily().contains("14"))
            linkFamilies.setVisibility(View.GONE);
    }

    private void fillFamilyBasicDetails() {
        familyName.setText(family.getGroupName());
        familyLocation.setText(family.getBaseRegion());
        if (family.getCreatedByName() != null && family.getGroupType() != null)
            familyDescription.setText(family.getGroupCategory() + ", By " + family.getCreatedByName());
        else if (family.getCreatedByName() == null && family.getGroupType() == null)
            familyDescription.setVisibility(View.GONE);
        else if (family.getCreatedByName() != null)
            familyDescription.setText("By " + family.getCreatedByName());
        else
            familyDescription.setText(family.getGroupCategory());
        if (family.getLogo() != null) {
            Glide.with(FamilyDashboardFragment.this.requireContext())
                    .load(S3_DEV_IMAGE_URL_SQUARE + IMAGE_BASE_URL + LOGO + family.getLogo())
                    .apply(Utilities.getCurvedRequestOptions())
                    .placeholder(R.drawable.family_logo)
                    .transition(withCrossFade(Utilities.getDrawableCrossFadeFactory()))
                    .into(familyLogo);
        }
        if (family.getCoverPic() != null) {
            Glide.with(FamilyDashboardFragment.this.requireContext())
                    .load(S3_DEV_IMAGE_URL_COVER + IMAGE_BASE_URL + COVER_PIC + family.getCoverPic())
                    .placeholder(R.drawable.family_dashboard_background)
                    .transition(DrawableTransitionOptions.withCrossFade(1000))
                    .into(backgroundCover);
        }
    }

    @Override
    public void onFamilyEditCompleted(int TYPE, Family family) {
        this.family = family;
        mListener.hideProgressDialog();
        updateFragment(family);
        fillFamilyBasicDetails();
    }

    @Override
    public void hideProgressDialog() {
        mListener.hideProgressDialog();
    }

    @Override
    public void showProgressDialog() {
        mListener.showProgressDialog();
    }

    @Override
    public void showErrorDialog(String errorMessage) {
        mListener.showErrorDialog(errorMessage);
    }

    @Override
    public void onFamilyCreated(Family family) {
        getActivity().onBackPressed();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ImageChangerActivity.REQUEST_CODE && resultCode == Activity.RESULT_OK || requestCode == 1001) {
            refreshFamilyDetails(family);
        }
    }

    private void requestJoinFamily() {
        mListener.showProgressDialog();
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("user_id", SharedPref.getUserRegistration().getId());
        jsonObject.addProperty("group_id", "" + family.getId());
        ApiServiceProvider apiServiceProvider = ApiServiceProvider.getInstance(getActivity());
        apiServiceProvider.joinFamily(jsonObject, null, new RetrofitListener() {
            @Override
            public void onResponseSuccess(String responseBodyString, ApiCallbackParams apiCallbackParams, int apiFlag) {
                mListener.hideProgressDialog();
                try {
                    editStatus = true;
                    String status = new JSONObject(responseBodyString).getJSONObject("data").getString("status");
                    if (status.equalsIgnoreCase("Pending")) {
                        joinFamily.setText("Pending");
                    } else {
                        joinFamily.setText("Joined");
                        getFamilyDetailsAsNew();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onResponseError(ErrorData errorData, ApiCallbackParams apiCallbackParams, Throwable throwable, int apiFlag) {
                mListener.showErrorDialog(Constants.SOMETHING_WRONG);
            }
        });
    }

    private void collapsingToolBarBehaviour() {
        appBar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    if (family != null && family.getGroupName() != null)
                        toolBarTitle.setText(family.getGroupName());
                    isShow = true;
                    hide();
                } else if (isShow) {
                    toolBarTitle.setText("");
                    isShow = false;
                    visible();
                }
            }
        });
    }


    private void hide() {

        try {
            Fragment selectedFragment = familyDetailsPagerAdapter.getItem(myFamiliesDetailsViewPager.getCurrentItem());
            if (selectedFragment instanceof FamilyPostFeedFragment) {
                FamilyPostFeedFragment familyPostFeedFragment = ((FamilyPostFeedFragment) selectedFragment);
                //familyPostFeedFragment.stickyPostHide();
            }
        } catch (Exception e) {
        }
    }

    private void visible() {

        new Handler().postDelayed(() -> requireActivity().runOnUiThread(() -> {

            Fragment selectedFragment = familyDetailsPagerAdapter.getItem(myFamiliesDetailsViewPager.getCurrentItem());
            if (selectedFragment instanceof FamilyPostFeedFragment) {
                FamilyPostFeedFragment familyPostFeedFragment = ((FamilyPostFeedFragment) selectedFragment);
                // familyPostFeedFragment.stickyPostShow();
            }
        }), 200);
    }

    private void networkError() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Uh oh! Check your internet connection and retry.")
                .setCancelable(false)
                .setPositiveButton("Retry", (dialog, which) -> getFamilyDetails()).setNegativeButton("Cancel", (dialog, which) -> {
            requireActivity().finish();
            dialog.dismiss();
        });
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        AlertDialog alert = builder.create();
        alert.setTitle("Connection Unavailable");
        alert.show();
        params.setMargins(0, 0, 20, 0);
        alert.getButton(DialogInterface.BUTTON_NEGATIVE).setLayoutParams(params);
    }

    private void dataSetInBottomSheet() {
        if (family.getMembership_fees() != null && family.getMembership_total_payed_amount() != null) {
            int due = (Integer.parseInt(family.getMembership_fees()) -
                    Integer.parseInt(family.getMembership_total_payed_amount()));
            if (due > 0)
                etxt_due_paid.setText(due + "");
            else etxt_due_paid.setText("0");
        }

        txt_paid_fees.setText(": " + family.getMembership_total_payed_amount());
        etxt_due_paid.requestFocus();
        etxt_due_paid.setSelection(etxt_due_paid.getText().length());
        txt_mtype.setText(": " + family.getMembership_type());
        txt_mduration.setText(": " + family.getMembership_period_type());
        txt_sdate.setText(": " + getFormattedCreatedAt(Long.parseLong(family.getMembership_from())));
        txt_tilldate.setText(": " + getFormattedCreatedAt(Long.parseLong(family.getMembership_to())));
        txt_fees.setText(": " + family.getMembership_fees());
        if (family.getMembership_payment_notes() != null && !family.getMembership_payment_notes().equals("")) {
            info_view.setVisibility(View.VISIBLE);
            txt_note.setText(family.getMembership_payment_notes());
        }

        if ("Pending".equals(family.getMembership_payment_status()) || "Partial".equals(family.getMembership_payment_status())) {
            btn_paynow.setVisibility(View.VISIBLE);
            view_due.setVisibility(View.VISIBLE);
        } else {
            view_due.setVisibility(View.GONE);
            btn_paynow.setVisibility(View.GONE);
        }
    }

    private String getFormattedCreatedAt(Long values) {
        Long value = values;
        if (value != null && value > 100) {
            value = TimeUnit.SECONDS.toMillis(value);
            DateTime dateTime = new DateTime(value);
            DateTimeFormatter dtfOut = DateTimeFormat.forPattern("MMM dd yyyy");
            return dtfOut.print(dateTime);
        }
        return "";
    }
}
