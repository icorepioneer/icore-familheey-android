package com.familheey.app.Fragments.Posts;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.familheey.app.Decorators.BottomAdditionalMarginDecorator;
import com.familheey.app.FamilheeyApplication;
import com.familheey.app.Interfaces.FamilyDashboardInteractor;
import com.familheey.app.Models.Response.Family;
import com.familheey.app.Networking.Retrofit.ApiServices;
import com.familheey.app.Networking.Retrofit.RetrofitBase;
import com.familheey.app.Post.CreatePostActivity;
import com.familheey.app.R;
import com.familheey.app.Utilities.SharedPref;
import com.familheey.app.Utilities.Utilities;
import com.familheey.app.pagination.PaginationAdapterCallback;
import com.familheey.app.pagination.PaginationScrollListener;
import com.google.android.material.button.MaterialButton;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import okhttp3.MediaType;
import okhttp3.RequestBody;

import static com.familheey.app.Utilities.Constants.Bundle.POSITION;
import static com.familheey.app.Utilities.Constants.FamilyDashboardIdentifiers.TypeFamilyPostFeedFragment;

public class FamilyPostFeedFragment extends Fragment implements PaginationAdapterCallback, PostAdapterInFamilyFeed.postItemClick {

    private static final int POST_CREATE_REQUEST = 1006;
    public CompositeDisposable subscriptions;
    @BindView(R.id.searchPost)
    EditText searchPost;
    @BindView(R.id.postList)
    RecyclerView postList;
    @BindView(R.id.stickypostList)
    RecyclerView stickypostList;
    List<PostData> data = new ArrayList<>();
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.layoutNoPost)
    LinearLayout layoutNoPost;
    @BindView(R.id.clearSearch)
    ImageView clearSearch;
    @BindView(R.id.create_post)
    MaterialButton createPost;
    @BindView(R.id.create_new_post)
    CardView createPostFab;
    private Integer prev_id = 0;
    private boolean isLoading = true;
    private boolean isLastPage = false;

    private boolean isAdmin = false;
    private String PostCreate = "";
    private final int TOTAL_PAGES = 5;
    private int currentPage = 0;
    private String query = "";
    private PostAdapterInFamilyFeed adapter;
    private FamilyDashboardInteractor familyDashboardInteractor;
    private int id = 0;
    private Boolean canCreatePost;
    private Boolean isMember;
    private List<StickyPost> stickyPost = new ArrayList<>();

    public FamilyPostFeedFragment() {
    }

    public static FamilyPostFeedFragment newInstance(int id, Boolean isAdmin, Boolean canCreatePost, String PostCreate, Boolean isMember) {
        FamilyPostFeedFragment fragment = new FamilyPostFeedFragment();
        Bundle args = new Bundle();
        args.putInt("ID", id);
        args.putBoolean("ADMIN", isAdmin);
        args.putBoolean("ISMEMBER", isMember);
        args.putBoolean("CANCREATE", canCreatePost);
        args.putString("POSTCREATE", PostCreate);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        subscriptions = new CompositeDisposable();
        assert getArguments() != null;
        id = (getArguments().getInt("ID"));
        PostCreate = (getArguments().getString("POSTCREATE"));
        isAdmin = getArguments().getBoolean("ADMIN");
        canCreatePost = getArguments().getBoolean("CANCREATE");
        isMember = getArguments().getBoolean("ISMEMBER");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_family_post_feed, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeSearchClearCallback();
        fetchFamilyFeed();
    }
/*
    @Override
    public void onItemClick(int pid) {
        unStickyPost(pid + "");
    }*/

    private void fetchFamilyFeed() {
        isLastPage = false;
        data.clear();
        if (adapter != null)
            adapter.notifyDataSetChanged();
        else {
            adapter = new PostAdapterInFamilyFeed(this, getContext(), stickyPost, data, this, "PRIVATE", isAdmin);
            setupRecyclerView();
        }
        currentPage = 0;

        getStickyPost(false);
        getPost();
        if (familyDashboardInteractor != null)
            familyDashboardInteractor.onFamilyAddComponentHidden(TypeFamilyPostFeedFragment);
        else createPostFab.setVisibility(View.INVISIBLE);

    }

    private void initializeSearchClearCallback() {
        searchPost.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0)
                    clearSearch.setVisibility(View.INVISIBLE);
                else clearSearch.setVisibility(View.VISIBLE);
            }
        });
        clearSearch.setOnClickListener(v -> {
            searchPost.setText("");
            onSearchQueryListener(EditorInfo.IME_ACTION_SEARCH);
        });
    }

    private void setupRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        postList.setLayoutManager(layoutManager);
        postList.setItemAnimator(new DefaultItemAnimator());
        postList.setAdapter(adapter);
        postList.addOnScrollListener(new PaginationScrollListener(layoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;

                //currentPage = data.size();
                getPost();
            }

            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    int c = layoutManager.findFirstVisibleItemPosition();
                    if (c > 0 && data.size() > c) {
                        if (data.get(c) != null) {
                            if (data.get(c).getPost_id() != null) {
                                if (!(prev_id.equals(data.get(c).getPost_id()))) {
                                    addViewCount(data.get(c).getPost_id() + "");
                                    prev_id = data.get(c).getPost_id();
                                }
                            }
                        }
                    }
                }
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

    }


    private void addViewCount(String post_id) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("user_id", SharedPref.getUserRegistration().getId());
        jsonObject.addProperty("post_id", post_id);
        RequestBody requestBody = RequestBody.create(jsonObject.toString(), MediaType.parse("application/json; charset=utf-8"));
        FamilheeyApplication application = FamilheeyApplication.get(getActivity());
        ApiServices apiServices = RetrofitBase.createRxResource(getActivity(), ApiServices.class);
        assert application != null;
        subscriptions.add(apiServices.addViewCount(requestBody)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(application.defaultSubscribeScheduler())
                .subscribe(response -> {
                }, throwable -> {
                }));
    }


    private void getStickyPost(boolean value) {
        SweetAlertDialog progressDialog = Utilities.getProgressDialog(getActivity());
        if (value) {
            progressDialog.show();
        }
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("user_id", SharedPref.getUserRegistration().getId());
        if (id != 0) {
            jsonObject.addProperty("group_id", id + "");
        }
        RequestBody requestBody = RequestBody.create(jsonObject.toString(), MediaType.parse("application/json; charset=utf-8"));
        FamilheeyApplication application = FamilheeyApplication.get(getActivity());
        ApiServices apiServices = RetrofitBase.createRxResource(getActivity(), ApiServices.class);
        subscriptions.add(apiServices.getStickyPost(requestBody)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(application.defaultSubscribeScheduler())
                .subscribe(response -> {
                    if (response.body().size() > 0) {
                        stickyPost = response.body();
                        adapter.addStickyPost(stickyPost, id + "");

                    } else {

                        stickyPost.clear();
                        adapter.addStickyPost(stickyPost, id + "");
                        stickypostList.setVisibility(View.GONE);
                    }
                    if (value) {
                        progressDialog.hide();
                    }

                }, throwable -> {
                    if (value) {
                        progressDialog.hide();
                    }
                }));
    }

    private void getPost() {
        if (currentPage == 0) {
            progressBar.setVisibility(View.VISIBLE);
        }
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("user_id", SharedPref.getUserRegistration().getId());
        if (id != 0) {
            jsonObject.addProperty("group_id", id);
        }
        jsonObject.addProperty("type", "post");
        jsonObject.addProperty("query", query);
        jsonObject.addProperty("offset", currentPage + "");
        jsonObject.addProperty("limit", "30");
        RequestBody requestBody = RequestBody.create(jsonObject.toString(), MediaType.parse("application/json; charset=utf-8"));
        FamilheeyApplication application = FamilheeyApplication.get(getActivity());
        ApiServices apiServices = RetrofitBase.createRxResource(getActivity(), ApiServices.class);
        subscriptions.add(apiServices.getPost(requestBody)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(application.defaultSubscribeScheduler())
                .subscribe(response -> {
                    if (currentPage > 0) {
                        adapter.removeLoadingFooter();
                    }


                    if (response.body() != null && response.body().getData() != null && response.body().getData().size() == 30) {
                        data.addAll(response.body().getData());
                        isLoading = false;
                        if (adapter != null) {
                            adapter.notifyDataSetChanged();
                            adapter.addLoadingFooter();
                        }

                    } else {
                        assert response.body() != null;
                        assert response.body().getData() != null;
                        data.addAll(response.body().getData());
                        isLastPage = true;
                    }

                    currentPage = data.size();
                    progressBar.setVisibility(View.GONE);
                    if (postList.getItemDecorationCount() > 0) {
                        postList.removeItemDecorationAt(0);
                    }
                    postList.addItemDecoration(new BottomAdditionalMarginDecorator());
                    adapter.notifyDataSetChanged();

                    /*if (response.body() != null && response.body().getData() != null && response.body().getData().size() > 0) {
                        data.addAll(response.body().getData());
                        currentPage = data.size();
                        if (adapter != null) {
                            adapter.notifyDataSetChanged();
                            adapter.addLoadingFooter();
                        }
                    } else {
                        isLastPage = true;
                        if (data.size() > 0) {
                            adapter.removeLoadingFooter();
                        }
                        if (postList.getItemDecorationCount() > 0) {
                            postList.removeItemDecorationAt(0);
                        }
                        adapter.removeLoadingFooter();
                        if (postList.getItemDecorationCount() > 0) {
                            postList.removeItemDecorationAt(0);
                        }
                        postList.addItemDecoration(new BottomAdditionalMarginDecorator());
                        adapter.notifyDataSetChanged();
                    }*/

                    if (data.size() > 0) {
                        layoutNoPost.setVisibility(View.GONE);
                        postList.setVisibility(View.VISIBLE);
                        //createPostFab.setVisibility(View.VISIBLE);
                        if (familyDashboardInteractor != null)
                            familyDashboardInteractor.onFamilyAddComponentVisible(TypeFamilyPostFeedFragment);
                        else createPostFab.setVisibility(View.VISIBLE);
                    } else {
                        layoutNoPost.setVisibility(View.VISIBLE);
                        postList.setVisibility(View.GONE);
                        if (familyDashboardInteractor != null)
                            familyDashboardInteractor.onFamilyAddComponentHidden(TypeFamilyPostFeedFragment);
                        else createPostFab.setVisibility(View.INVISIBLE);
                    }

                }, throwable -> {
                    progressBar.setVisibility(View.GONE);
                    if (adapter != null)
                        adapter.showRetry(true, "");
                }));
    }

    private void makeStickyPost(String pid) {
        SweetAlertDialog progressDialog = Utilities.getProgressDialog(getActivity());
        progressDialog.show();
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("post_id", pid);
        if (id != 0) {
            jsonObject.addProperty("group_id", id + "");
        }
        jsonObject.addProperty("type", "stick");
        RequestBody requestBody = RequestBody.create(jsonObject.toString(), MediaType.parse("application/json; charset=utf-8"));
        FamilheeyApplication application = FamilheeyApplication.get(getActivity());
        ApiServices apiServices = RetrofitBase.createRxResource(getActivity(), ApiServices.class);
        subscriptions.add(apiServices.makeStickyPost(requestBody)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(application.defaultSubscribeScheduler())
                .subscribe(response -> {
                    getStickyPost(true);
                    progressDialog.hide();
                }, throwable -> {
                    progressDialog.hide();
                }));
    }

    private void unStickyPost(String pid) {
        SweetAlertDialog progressDialog = Utilities.getProgressDialog(getActivity());
        progressDialog.show();
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("post_id", pid);
        if (id != 0) {
            jsonObject.addProperty("group_id", id + "");
        }
        jsonObject.addProperty("type", "unstick");
        RequestBody requestBody = RequestBody.create(jsonObject.toString(), MediaType.parse("application/json; charset=utf-8"));
        FamilheeyApplication application = FamilheeyApplication.get(getActivity());
        ApiServices apiServices = RetrofitBase.createRxResource(getActivity(), ApiServices.class);
        subscriptions.add(apiServices.makeStickyPost(requestBody)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(application.defaultSubscribeScheduler())
                .subscribe(response -> {
                    getStickyPost(true);
                    progressDialog.hide();
                }, throwable -> {
                    progressDialog.hide();
                }));
    }

    public void searchPost(String query) {
        this.query = query;
        isLastPage = false;
        data.clear();
        currentPage = 0;
        isLoading = true;
        getPost();
    }

    @OnEditorAction(R.id.searchPost)
    protected boolean onSearchQueryListener(int actionId) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            searchPost(searchPost.getText().toString());
            try {
                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(searchPost.getWindowToken(), 0);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return true;
        }
        return false;
    }

    @Override
    public void makeAsSticky(int poston) {
        makeStickyPost(data.get(poston).getPost_id() + "");
    }

    @Override
    public void retryPageLoad() {
        getPost();
    }

    @Override
    public void onEditActivity(Intent intent) {
        startActivityForResult(intent, 1002);
    }

    @Override
    public void onChatActivity(Intent intent) {
        startActivityForResult(intent, 1003);
    }

    @Override
    public void makeAsUnSticky(String id) {
        unStickyPost(id);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1002 && resultCode == Activity.RESULT_OK) {
            int pos = data.getExtras().getInt("pos");
            PostData postData = new Gson().fromJson(data.getExtras().getString("data"), PostData.class);
            this.data.get(pos).setPost_attachment(postData.getPost_attachment());
            this.data.get(pos).setIs_shareable(postData.getIs_shareable());
            this.data.get(pos).setSnap_description(postData.getSnap_description());
            this.data.get(pos).setConversation_enabled(postData.getConversation_enabled());
            adapter.notifyDataSetChanged();
            getStickyPost(false);
        } else if (requestCode == 1003 && resultCode == Activity.RESULT_OK) {

            if (data.getExtras().getBoolean("isChat")) {
                this.data.clear();
                currentPage = 0;
                isLoading = true;
                isLastPage = false;
                setupRecyclerView();
                progressBar.setVisibility(View.VISIBLE);
                getPost();
            } else {
                this.data.get(data.getExtras().getInt(POSITION)).setConversation_count_new("0");
                adapter.notifyDataSetChanged();
            }
        } else if (requestCode == POST_CREATE_REQUEST && resultCode == Activity.RESULT_OK) {
            fetchFamilyFeed();
        }
    }

    @Override
    public void onSearchTag(String hashtag) {
        searchPost(hashtag);
        //PostFragment poostFrag = ((PostFragment)this.getParentFragment());
        //poostFrag.search_post.setText(hashtag);
    }


    @OnClick({R.id.create_post, R.id.create_new_post})
    public void onViewClicked() {
        createNewPost();
    }

    public void createNewPost() {
        if (canCreatePost) {
            Intent intent = new Intent(getContext(), CreatePostActivity.class).putExtra("post_create", PostCreate).putExtra("FROM", "FAMILY").putExtra("id", id + "");
            startActivityForResult(intent, POST_CREATE_REQUEST);
        } else {
            if (isMember)
                Toast.makeText(getContext(), "You don't have sufficient privileges to create post", Toast.LENGTH_SHORT).show();
            else {
                Toast.makeText(getContext(), "Please join the family to create a post", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        familyDashboardInteractor = Utilities.getListener(this, FamilyDashboardInteractor.class);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        familyDashboardInteractor = null;
    }

    public void updateFamily(Family family) {
        id = family.getId();
        isAdmin = family.isAdmin();
        canCreatePost = family.canCreatePost();
        PostCreate = family.getPostCreate();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (data != null && data.size() > 0) {
            if (familyDashboardInteractor != null)
                familyDashboardInteractor.onFamilyAddComponentVisible(TypeFamilyPostFeedFragment);
            else createPostFab.setVisibility(View.VISIBLE);
        } else {
            if (familyDashboardInteractor != null)
                familyDashboardInteractor.onFamilyAddComponentHidden(TypeFamilyPostFeedFragment);
            else createPostFab.setVisibility(View.INVISIBLE);
        }
    }
}
