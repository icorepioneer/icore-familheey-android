package com.familheey.app.Fragments.Posts;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.familheey.app.Activities.ChatActivity;
import com.familheey.app.Activities.CreateAlbumDetailedActivity;
import com.familheey.app.Activities.FamilyDashboardActivity;
import com.familheey.app.Activities.ProfileActivity;
import com.familheey.app.Activities.ShareEventActivity;
import com.familheey.app.Activities.SharelistActivity;
import com.familheey.app.Activities.SubFolderActivity;
import com.familheey.app.BrowserActivity;
import com.familheey.app.BuildConfig;
import com.familheey.app.FamilheeyApplication;
import com.familheey.app.Models.Response.Family;
import com.familheey.app.Models.Response.FamilyMember;
import com.familheey.app.Need.NeedRequestDetailedActivity;
import com.familheey.app.Networking.Retrofit.ApiServices;
import com.familheey.app.Networking.Retrofit.RetrofitBase;
import com.familheey.app.Post.EditPostActivity;
import com.familheey.app.R;
import com.familheey.app.Utilities.Constants;
import com.familheey.app.Utilities.SharedPref;
import com.familheey.app.Utilities.Utilities;
import com.familheey.app.pagination.PaginationAdapterCallback;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.luseen.autolinklibrary.AutoLinkMode;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import okhttp3.MediaType;
import okhttp3.RequestBody;

import static com.familheey.app.Utilities.Constants.ApiPaths.IMAGE_BASE_URL;
import static com.familheey.app.Utilities.Constants.ApiPaths.S3_DEV_IMAGE_URL_SQUARE;
import static com.familheey.app.Utilities.Constants.Bundle.CAN_CREATE;
import static com.familheey.app.Utilities.Constants.Bundle.CAN_UPDATE;
import static com.familheey.app.Utilities.Constants.Bundle.DATA;
import static com.familheey.app.Utilities.Constants.Bundle.ID;
import static com.familheey.app.Utilities.Constants.Bundle.IDENTIFIER;
import static com.familheey.app.Utilities.Constants.Bundle.IS_ADMIN;
import static com.familheey.app.Utilities.Constants.Bundle.IS_UPDATE_MODE;
import static com.familheey.app.Utilities.Constants.Bundle.SUB_TYPE;
import static com.familheey.app.Utilities.Constants.Bundle.TYPE;

public class PostAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private boolean isLoadingAdded = false;
    private boolean retryPageLoad = false;
    private String errorMsg;
    private PaginationAdapterCallback mCallback;

    private Context context;
    private List<PostData> repositories;
    private static final int ITEM = 0;
    private static final int SHARE = 1;
    private static final int LOADING = 2;

    private RequestOptions requestOptions;

    public CompositeDisposable subscriptions;
    private String type;
    private boolean isAdmin;
    private postItemClick mListener;

    PostAdapter(postItemClick mListener, Context context, List<PostData> data, PaginationAdapterCallback mCallback, String type, Boolean isAdmin) {
        this.repositories = data;
        this.context = context;
        this.isAdmin = isAdmin;
        this.mCallback = mCallback;
        requestOptions = new RequestOptions();
        this.type = type;
        this.mListener = mListener;
        requestOptions.transforms(new RoundedCorners(16));
        subscriptions = new CompositeDisposable();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if (viewType == ITEM) {

            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.item_myfamily_post, parent, false);
            return new ViewHolderOne(listItem);
        } else if (viewType == LOADING) {

            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.item_progress, parent, false);
            return new LoadingVH(listItem);
        } else {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.item_myfamily_post_share, parent, false);
            return new ViewHolderTwo(listItem);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        PostData postData = repositories.get(position);
        switch (getItemViewType(position)) {
            case ITEM:
                ViewHolderOne holderOne = (ViewHolderOne) holder;

                String id = postData.getCreated_by() + "";
                if (postData.getSnap_description().equals("")) {
                    holderOne.txt_less_or_more.setVisibility(View.GONE);
                    holderOne.txt_des.setVisibility(View.GONE);
                } else {
                    holderOne.txt_des.setVisibility(View.VISIBLE);
                }
                if (postData.getPropic() != null && !postData.getPropic().isEmpty()) {
                    String pro_url = S3_DEV_IMAGE_URL_SQUARE + IMAGE_BASE_URL + Constants.Paths.PROFILE_PIC + postData.getPropic();
                    Glide.with(holder.itemView.getContext())
                            .load(pro_url)
                            .placeholder(R.drawable.avatar_male)
                            .apply(Utilities.getCurvedRequestOptions())
                            .apply(requestOptions).into(holderOne.profileImage);
                } else {
                    holderOne.profileImage.setImageResource(R.drawable.avatar_male);
                }
                holderOne.postusername.setText(postData.getCreated_user_name());
                if (type.equals("PUBLIC")) {
                    holderOne.postedgroup.setText("Posted in Public");
                } else {
                    holderOne.postedgroup.setText("Posted in " + postData.getGroup_name());
                }
                holderOne.postdate.setText(dateFormat(postData.getCreatedAt()));
                holderOne.txt_des.addAutoLinkMode(
                        AutoLinkMode.MODE_HASHTAG,
                        AutoLinkMode.MODE_URL);
                holderOne.txt_des.setHashtagModeColor(ContextCompat.getColor(context, R.color.buttoncolor));
                holderOne.txt_des.setUrlModeColor(ContextCompat.getColor(context, R.color.buttoncolor));
                holderOne.txt_less_or_more.setText("Read More");

                if (postData.getPost_attachment() != null)
                    if (postData.getValid_urls() != null && postData.getValid_urls().size() == 1 && postData.getPost_attachment() == null || postData.getPost_attachment().size() == 0) {
                        if (postData.getUrl_metadata() != null) {
                            holderOne.album_view.setVisibility(View.GONE);
                            holderOne.sliderView.setVisibility(View.GONE);
                            holderOne.thanks_post_view.setVisibility(View.GONE);
                            holderOne.link_preview.setVisibility(View.GONE);
                            holderOne.txt_less_or_more.setVisibility(View.VISIBLE);
                            holderOne.middle_view.setVisibility(View.VISIBLE);
                            holderOne.middle_first_view.setVisibility(View.VISIBLE);
                            holderOne.txt_temp.setText(postData.getSnap_description().trim());
                            if (postData.getUrl_metadata().getUrlMetadataResult() != null) {
                                holderOne.link_preview.setVisibility(View.VISIBLE);
                                String t = postData.getUrl_metadata().getUrlMetadataResult().getTitle();
                                String d = postData.getUrl_metadata().getUrlMetadataResult().getDescription();
                                if (t != null && !t.isEmpty()) {
                                    holderOne.txt_url_des.setText(t);
                                } else if (d != null && !d.isEmpty()) {
                                    holderOne.txt_url_des.setText(t);
                                }
                                holderOne.txt_url.setText(postData.getUrl_metadata().getUrlMetadataResult().getUrl());

                                String url = postData.getUrl_metadata().getUrlMetadataResult().getImage() + "";
                                if (!url.contains("http")) {
                                    url = postData.getUrl_metadata().getUrlMetadataResult().getUrl() + url;
                                }
                                if (!url.isEmpty() && url.length() > 5) {
                                    holderOne.url_img.setScaleType(ImageView.ScaleType.CENTER);
                                    Glide.with(holder.itemView.getContext())
                                            .load(url)
                                            .placeholder(R.drawable.d_image)
                                            .apply(Utilities.getCurvedRequestOptions()).into(holderOne.url_img);
                                } else {
                                    holderOne.url_img.setScaleType(ImageView.ScaleType.FIT_XY);
                                }
                                holderOne.link_preview.setOnClickListener(view -> {
                                    try {
                                        String urls = postData.getValid_urls().get(0).trim();
                                        if (!urls.contains("http")) {
                                            urls = urls.replaceAll("www.", "http://www.");
                                        }
                                        if (urls.contains("familheey")) {
                                            openAppGetParams(urls);
                                        } else {
                                            context.startActivity(new Intent(context, BrowserActivity.class).putExtra("URL", urls));
                                        }
                                    } catch (Exception e) {
                                    }
                                });
                            }
                        }
                    } else if (postData.getPublish_type() != null && "request".equals(postData.getPublish_type())) {
                        holderOne.link_preview.setVisibility(View.GONE);
                        holderOne.sliderView.setVisibility(View.GONE);
                        holderOne.txt_less_or_more.setVisibility(View.GONE);
                        holderOne.txt_des.setVisibility(View.GONE);
                        holderOne.middle_first_view.setVisibility(View.VISIBLE);
                        holderOne.thanks_post_view.setVisibility(View.VISIBLE);
                        holderOne.text_description.setText(postData.getSnap_description());
                        holderOne.middle_view.setVisibility(View.VISIBLE);
                        holderOne.album_view.setVisibility(View.GONE);

                        holderOne.txt_less_or_more.setVisibility(View.GONE);
                        if (postData.getPublish_mention_items() != null && postData.getPublish_mention_items().size() > 0)
                            holderOne.txt_request_item.setText(postData.getPublish_mention_items().get(0).getItem_name());
                        Glide.with(holder.itemView.getContext())
                                .load(BuildConfig.IMAGE_BASE_URL + "post/" + postData.getPost_attachment().get(0).getFilename())
                                .into(holderOne.thanks_post_img);
                        if (postData.getPublish_mention_users() != null) {
                            holderOne.rv_name_list.setLayoutManager(new GridLayoutManager(context, 5));
                            holderOne.rv_name_list.setAdapter(new MensionNameAdapter(postData.getPublish_mention_users(), context));
                        }

                    } else {
                        holderOne.middle_first_view.setVisibility(View.GONE);
                        holderOne.thanks_post_view.setVisibility(View.GONE);
                        holderOne.link_preview.setVisibility(View.GONE);
                        holderOne.sliderView.setVisibility(View.GONE);
                        holderOne.txt_less_or_more.setVisibility(View.VISIBLE);
                        if (postData.getPublish_type() != null && "albums".equals(postData.getPublish_type()) || "documents".equals(postData.getPublish_type())) {
                            holderOne.middle_view.setVisibility(View.GONE);
                            holderOne.album_view.setVisibility(View.VISIBLE);
                            holderOne.txt_album_des.setText(postData.getSnap_description().trim());

                            if ("documents".equals(postData.getPublish_type())) {
                                holderOne.btn_open_album.setText("Open Folder");
                            } else {
                                holderOne.btn_open_album.setText("Open Album");
                            }

                            holderOne.btn_open_album.setOnClickListener(view -> {
                                boolean s = false;
                                if (id.equals(SharedPref.getUserRegistration().getId())) {
                                    s = true;
                                }
                                if ("albums".equals(postData.getPublish_type())) {
                                    Bundle args = new Bundle();
                                    //args.putParcelable(Constants.Bundle.DATA, folder);
                                    args.putBoolean(IS_UPDATE_MODE, true);

                                    args.putBoolean(IS_ADMIN, s);
                                    args.putBoolean(CAN_CREATE, s);
                                    args.putBoolean(CAN_UPDATE, s);
                                    args.putInt(TYPE, 1);
                                    args.putString(ID, postData.getPublish_id());
                                    args.putString(IDENTIFIER, postData.getTo_group_id());
                                    args.putString("FROM", "POST");
                                    Intent intent = new Intent(context, CreateAlbumDetailedActivity.class);
                                    intent.putExtras(args);
                                    context.startActivity(intent);
                                } else {
                                    Intent intent = new Intent(context, SubFolderActivity.class);
                                    intent.putExtra(Constants.Bundle.ID, postData.getTo_group_id());
                                    intent.putExtra(Constants.Bundle.FOLDER_ID, postData.getPublish_id());
                                    intent.putExtra(Constants.Bundle.TYPE, 1);
                                    intent.putExtra(Constants.Bundle.TITLE, "");
                                    intent.putExtra(Constants.Bundle.DESCRIPTION, "");
                                    intent.putExtra(IS_ADMIN, s);
                                    intent.putExtra(CAN_CREATE, s);
                                    intent.putExtra(CAN_UPDATE, s);
                                    context.startActivity(intent);
                                }
                            });
                        } else {
                            holderOne.middle_view.setVisibility(View.VISIBLE);
                            holderOne.album_view.setVisibility(View.GONE);
                            holderOne.txt_temp.setText(postData.getSnap_description().trim());
                        }

                        if (postData.getPost_attachment() != null && postData.getPost_attachment().size() > 0) {
                            holderOne.sliderView.setVisibility(View.VISIBLE);
                            if (postData.getPost_attachment().get(0).getType().contains("image")) {
                                if (postData.getPost_attachment().get(0).getWidth() != null && postData.getPost_attachment().get(0).getHeight() != null) {
                                    ViewGroup.LayoutParams params = holderOne.sliderView.getLayoutParams();
                                    params.height = getwidgetsize(postData.getPost_attachment().get(0).getWidth(), postData.getPost_attachment().get(0).getHeight());
                                    postData.getPost_attachment().get(0).setHeight1(params.height + "");
                                    holderOne.sliderView.setLayoutParams(params);
                                } else {
                                    ViewGroup.LayoutParams params = holderOne.sliderView.getLayoutParams();
                                    params.height = getwidgetsize();
                                    holderOne.sliderView.setLayoutParams(params);
                                }
                            } else if (postData.getPost_attachment().get(0).getType().contains("video")) {
                                ViewGroup.LayoutParams params = holderOne.sliderView.getLayoutParams();
                                params.height = 850;
                                holderOne.sliderView.setLayoutParams(params);
                            } else {
                                ViewGroup.LayoutParams params = holderOne.sliderView.getLayoutParams();
                                params.height = 600;
                                holderOne.sliderView.setLayoutParams(params);
                            }

                            PostSliderAdapter adapter = new PostSliderAdapter(context, postData);
                            holderOne.sliderView.setSliderAdapter(adapter);

                            holderOne.sliderView.setIndicatorAnimation(IndicatorAnimations.THIN_WORM);
                            holderOne.sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
                        }
                    }

                holderOne.share_count.setText(postData.getShared_user_count());

                if (id.equals(SharedPref.getUserRegistration().getId()) && !"request".equals(postData.getPublish_type()) && !"albums".equals(postData.getPublish_type()) && !"documents".equals(postData.getPublish_type())) {
                    holderOne.view_count.setText(postData.getViews_count());
                    holderOne.imgviw.setVisibility(View.VISIBLE);
                    holderOne.view_count.setVisibility(View.VISIBLE);
                    if (postData.is_shareable) {
                        holderOne.imgsha.setVisibility(View.VISIBLE);
                        holderOne.share_count.setVisibility(View.VISIBLE);
                    }

                } else {
                    holderOne.imgviw.setVisibility(View.GONE);
                    holderOne.view_count.setVisibility(View.GONE);
                    holderOne.imgsha.setVisibility(View.GONE);
                    holderOne.share_count.setVisibility(View.GONE);
                }

                if (postData.getConversation_enabled()) {
                    holderOne.txt_count.setText(postData.getConversation_count());
                    holderOne.img_con.setVisibility(View.VISIBLE);
                    holderOne.txt_count.setVisibility(View.VISIBLE);
                } else {
                    holderOne.img_con.setVisibility(View.GONE);
                    holderOne.txt_count.setVisibility(View.GONE);
                }

                if (postData.getConversation_enabled() && Integer.parseInt(postData.getConversation_count_new()) > 0) {
                    holderOne.unread_status.setVisibility(View.VISIBLE);
                } else {
                    holderOne.unread_status.setVisibility(View.GONE);
                }
                holderOne.txt_less_or_more.setOnClickListener(v -> {

                    String description = postData.getSnap_description().trim();
                    description = description.replaceAll("HTTP:", "http:").replaceAll("Http:", "http:")
                            .replaceAll("Https:", "https:")
                            .replaceAll("HTTPS:", "https:");
                    description = description.replaceAll("WWW.", "www.").replaceAll("Www.", "www.");
                    if (holderOne.txt_less_or_more.getText().equals("Read More")) {
                        holderOne.txt_less_or_more.setText("Read Less");
                        holderOne.txt_des.setAutoLinkText(description);
                        holderOne.txt_des.setMaxLines(Integer.MAX_VALUE);
                        holderOne.txt_des.setEllipsize(null);
                    } else {
                        holderOne.txt_less_or_more.setText("Read More");
                        holderOne.txt_des.setMaxLines(2);
                        holderOne.txt_des.setEllipsize(TextUtils.TruncateAt.END);
                    }
                });

                holderOne.btn_more.setOnClickListener(v ->
                        {
                            addViewCount(postData.getPost_id() + "");
                            if (id.equals(SharedPref.getUserRegistration().getId())) {
                                showMenusPostOwner(v, position, postData);
                            } else {
                                showMenusNormalUser(v, position, postData);
                            }
                        }
                );

                if (postData.getIs_shareable()) {
                    holderOne.btn_share.setVisibility(View.VISIBLE);
                } else {
                    holderOne.btn_share.setVisibility(View.GONE);
                }
                holderOne.txt_des.setAutoLinkOnClickListener((autoLinkMode, matchedText) -> {

                    if (autoLinkMode == AutoLinkMode.MODE_URL) {
                        try {
                            String url = matchedText.trim();
                            if (!url.contains("http")) {
                                url = url.replaceAll("www.", "http://www.");
                            }

                            if (url.contains("familheey")) {
                                openAppGetParams(url);
                            } else {
                                context.startActivity(new Intent(context, BrowserActivity.class).putExtra("URL", url));
                            }

                        } catch (Exception e) {
                        }
                    } else {
                        mListener.onSearchTag(matchedText.trim());
                    }

                });

                /*holderOne.usersname.setAutoLinkOnClickListener((autoLinkMode, matchedText) -> {
                });
                */
                holderOne.btn_open_request.setOnClickListener(view -> {
                    Intent requestDetailedIntent = new Intent(context, NeedRequestDetailedActivity.class);
                    requestDetailedIntent.putExtra(DATA, postData.getPublish_id());
                    context.startActivity(requestDetailedIntent);
                });
                holderOne.postedgroup.setOnClickListener(v -> {
                    if (!type.equals("PUBLIC")) {
                        Family family = new Family();
                        family.setId(Integer.parseInt(postData.getTo_group_id()));
                        Intent intent = new Intent(context, FamilyDashboardActivity.class);
                        intent.putExtra(DATA, family);
                        holder.itemView.getContext().startActivity(intent);
                    }
                });

                holderOne.txt_temp.post(() -> {

                    String description = postData.getSnap_description().trim();
                    description = description.replaceAll("HTTP:", "http:").replaceAll("Http:", "http:")
                            .replaceAll("Https:", "https:")
                            .replaceAll("HTTPS:", "https:");
                    description = description.replaceAll("WWW.", "www.").replaceAll("Www.", "www.");
                    if (holderOne.txt_temp.getLineCount() > 2) {
                        holderOne.txt_less_or_more.setVisibility(View.VISIBLE);
                        holderOne.txt_des.setMaxLines(2);
                        holderOne.txt_des.setEllipsize(TextUtils.TruncateAt.END);
                        holderOne.txt_des.setAutoLinkText(description);
                    } else {
                        holderOne.txt_less_or_more.setVisibility(View.GONE);
                        holderOne.txt_des.setAutoLinkText(description);
                    }
                });

                holderOne.btn_share.setOnClickListener(v -> showMenusShare(v, position));

                holderOne.profileImage.setOnClickListener(v -> {
                    Intent intent = new Intent(context, ProfileActivity.class);
                    FamilyMember familyMember = new FamilyMember();
                    familyMember.setId(Integer.parseInt(SharedPref.getUserRegistration().getId()));
                    familyMember.setUserId(postData.getCreated_by());
                    familyMember.setProPic(postData.getPropic());
                    intent.putExtra(DATA, familyMember);
                    intent.putExtra(Constants.Bundle.FOR_EDITING, true);
                    ActivityOptionsCompat options = ActivityOptionsCompat.
                            makeSceneTransitionAnimation((Activity) context, holderOne.profileImage, "profile");
                    context.startActivity(intent, options.toBundle());
                });

                holderOne.img_con.setOnClickListener(v -> {
                    addViewCount(postData.getPost_id() + "");
                    mListener.onChatActivity(new Intent(context, ChatActivity.class)
                            .putExtra(SUB_TYPE, "POST")
                            .putExtra(TYPE, "")
                            .putExtra("POS", position)
                            .putExtra(DATA, postData));

                });
                holderOne.imgsha.setOnClickListener(v -> {


                    addViewCount(postData.getPost_id() + "");
                    if (postData.getShared_user_count() != null && Integer.parseInt(postData.getShared_user_count()) > 0) {
                        String pid;
                        if (postData.getOrgin_id() != null) {
                            pid = postData.getOrgin_id();
                        } else {
                            pid = postData.getPost_id() + "";
                        }


                        context.startActivity(new Intent(context, SharelistActivity.class)
                                .putExtra(Constants.Bundle.TYPE, "POST")
                                .putExtra("event_id", pid)
                                .putExtra("user_id", postData.getCreated_by() + ""));
                    }
                });


                holderOne.imgviw.setOnClickListener(v -> {
                    addViewCount(postData.getPost_id() + "");
                    if (postData.getViews_count() != null && Integer.parseInt(postData.getViews_count()) > 0) {
                        context.startActivity(new Intent(context, SharelistActivity.class)
                                .putExtra(Constants.Bundle.TYPE, "POSTVIEW")
                                .putExtra("event_id", postData.getPost_id() + "")
                                .putExtra("user_id", postData.getCreated_by() + ""));
                    }
                });
                holderOne.itemView.setOnClickListener(v -> addViewCount(postData.getPost_id() + ""));


                break;
            case SHARE:

                String ids = postData.getCreated_by() + "";
                ViewHolderTwo holderTwo = (ViewHolderTwo) holder;
                holderTwo.txt_des.addAutoLinkMode(
                        AutoLinkMode.MODE_HASHTAG,
                        AutoLinkMode.MODE_URL);
                holderTwo.txt_des.setHashtagModeColor(ContextCompat.getColor(context, R.color.buttoncolor));
                holderTwo.txt_des.setUrlModeColor(ContextCompat.getColor(context, R.color.buttoncolor));


                holderTwo.txt_less_or_more.setText("Read More");

                if (postData.getValid_urls() != null && postData.getValid_urls().size() > 0 && postData.getValid_urls().size() == 1 && postData.getPost_attachment() == null || postData.getPost_attachment().size() == 0) {
                    if (postData.getUrl_metadata() != null) {
                        if (postData.getUrl_metadata().getUrlMetadataResult() != null) {
                            holderTwo.link_preview.setVisibility(View.VISIBLE);
                            String t = postData.getUrl_metadata().getUrlMetadataResult().getTitle();
                            String d = postData.getUrl_metadata().getUrlMetadataResult().getDescription();
                            if (t != null && !t.isEmpty()) {
                                holderTwo.txt_url_des.setText(t);
                            } else if (d != null && !d.isEmpty()) {
                                holderTwo.txt_url_des.setText(t);
                            }
                            holderTwo.txt_url.setText(postData.getUrl_metadata().getUrlMetadataResult().getUrl());

                            String url = postData.getUrl_metadata().getUrlMetadataResult().getImage() + "";
                            if (!url.contains("http")) {
                                url = postData.getUrl_metadata().getUrlMetadataResult().getUrl() + url;
                            }

                            holderTwo.url_img.setScaleType(ImageView.ScaleType.CENTER);
                            Glide.with(holder.itemView.getContext())
                                    .load(url)
                                    .placeholder(R.drawable.d_image)
                                    .apply(Utilities.getCurvedRequestOptions())
                                    .into(holderTwo.url_img);

                            holderTwo.link_preview.setOnClickListener(view -> {
                                try {
                                    String urls = postData.getValid_urls().get(0).trim();
                                    if (!urls.contains("http")) {
                                        urls = urls.replaceAll("www.", "http://www.");
                                    }

                                    if (urls.contains("familheey")) {
                                        openAppGetParams(urls);
                                    } else {
                                        context.startActivity(new Intent(context, BrowserActivity.class).putExtra("URL", urls));
                                    }
                                } catch (Exception e) {
                                }
                            });

                        }
                    }
                } else {
                    holderTwo.link_preview.setVisibility(View.GONE);
                }

                holderTwo.txt_des.setAutoLinkOnClickListener((autoLinkMode, matchedText) -> {

                    if (autoLinkMode == AutoLinkMode.MODE_URL) {
                        try {
                            String url = matchedText.trim();
                            if (!url.contains("http")) {
                                url = url.replaceAll("www.", "http://www.");
                            }

                            if (url.contains("familheey")) {
                                openAppGetParams(url);
                            } else {
                                context.startActivity(new Intent(context, BrowserActivity.class).putExtra("URL", url));
                            }

                        } catch (Exception e) {
                        }
                    } else {
                        mListener.onSearchTag(matchedText.trim());
                    }

                });

                if (postData.getCommon_share_count() != null && Integer.parseInt(postData.getCommon_share_count()) > 1) {
                    int c = Integer.parseInt(postData.getCommon_share_count());
                    holderTwo.postusername.setText(postData.getShared_user_name() + " and other " + (c - 1) + " shared a post");
                } else {
                    holderTwo.postusername.setText(postData.getShared_user_name() + " shared a post");
                }
                holderTwo.innerpostusername.setText(postData.getParent_post_created_user_name());
                holderTwo.postedgroup.setText("Shared in " + postData.getGroup_name() + "");
                holderTwo.postdate.setText(dateFormat(postData.getCreatedAt()) + "");

                holderTwo.txt_count.setText(postData.getConversation_count());
                holderTwo.share_count.setText(postData.getShared_user_count());
                holderTwo.view_count.setText(postData.getViews_count());

                if (postData.getParent_post_grp_name() != null) {
                    holderTwo.innerpostedgroup.setText("Posted in " + postData.getParent_post_grp_name());
                } else {
                    holderTwo.innerpostedgroup.setText("Posted in " + postData.getPrivacy_type());
                }
                if (postData.getConversation_enabled() && Integer.parseInt(postData.getConversation_count_new()) > 0) {
                    holderTwo.unread_status.setVisibility(View.VISIBLE);
                } else {
                    holderTwo.unread_status.setVisibility(View.GONE);
                }

                holderTwo.txt_temp.setText(postData.getSnap_description());
                holderTwo.txt_temp.post(() -> {
                    String description = postData.getSnap_description().trim();
                    description = description.replaceAll("HTTP:", "http:").replaceAll("Http:", "http:")
                            .replaceAll("Https:", "https:")
                            .replaceAll("HTTPS:", "https:");
                    description = description.replaceAll("WWW.", "www.").replaceAll("Www.", "www.");
                    if (holderTwo.txt_temp.getLineCount() > 2) {
                        holderTwo.txt_less_or_more.setVisibility(View.VISIBLE);
                        holderTwo.txt_des.setMaxLines(2);
                        holderTwo.txt_des.setEllipsize(TextUtils.TruncateAt.END);
                        holderTwo.txt_des.setAutoLinkText(description);
                    } else {
                        holderTwo.txt_less_or_more.setVisibility(View.GONE);
                        holderTwo.txt_des.setAutoLinkText(description);
                    }
                });
                holderTwo.txt_temp.setVisibility(View.INVISIBLE);
                String url = S3_DEV_IMAGE_URL_SQUARE + IMAGE_BASE_URL + Constants.Paths.PROFILE_PIC + repositories.get(position).getParent_post_created_user_propic();
                Glide.with(holder.itemView.getContext())
                        .load(url)
                        .placeholder(R.drawable.avatar_male)
                        .into(holderTwo.innerprofileImage);

                if (ids.equals(SharedPref.getUserRegistration().getId())) {
                    holderTwo.view_count.setText(postData.getViews_count());
                    holderTwo.imgviw.setVisibility(View.VISIBLE);
                    holderTwo.view_count.setVisibility(View.VISIBLE);

                    if (postData.is_shareable) {
                        holderTwo.imgsha.setVisibility(View.VISIBLE);
                        holderTwo.share_count.setVisibility(View.VISIBLE);
                    }
                } else {
                    holderTwo.imgviw.setVisibility(View.GONE);
                    holderTwo.view_count.setVisibility(View.GONE);
                    holderTwo.imgsha.setVisibility(View.GONE);
                    holderTwo.share_count.setVisibility(View.GONE);
                }

                if (postData.getPost_attachment() != null && postData.getPost_attachment().size() > 0) {
                    holderTwo.sliderView.setVisibility(View.VISIBLE);

                    if (postData.getPost_attachment().get(0).getType().contains("image")) {
                        if (postData.getPost_attachment().get(0).getWidth() != null && postData.getPost_attachment().get(0).getHeight() != null) {

                            ViewGroup.LayoutParams params = holderTwo.sliderView.getLayoutParams();
                            params.height = getinnerwidgetsize(postData.getPost_attachment().get(0).getWidth(), postData.getPost_attachment().get(0).getHeight());
                            holderTwo.sliderView.setLayoutParams(params);

                        } else {
                            ViewGroup.LayoutParams params = holderTwo.sliderView.getLayoutParams();
                            params.height = getinnerwidgetsize();
                            holderTwo.sliderView.setLayoutParams(params);
                        }
                    } else if (postData.getPost_attachment().get(0).getType().contains("video")) {
                        ViewGroup.LayoutParams params = holderTwo.sliderView.getLayoutParams();
                        params.height = 850;
                        holderTwo.sliderView.setLayoutParams(params);
                    } else {
                        ViewGroup.LayoutParams params = holderTwo.sliderView.getLayoutParams();
                        params.height = 550;
                        holderTwo.sliderView.setLayoutParams(params);
                    }


                    PostSliderAdapter adapter = new PostSliderAdapter(context, postData);
                    holderTwo.sliderView.setSliderAdapter(adapter);
                    holderTwo.sliderView.setIndicatorAnimation(IndicatorAnimations.THIN_WORM);
                    holderTwo.sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
                } else {
                    holderTwo.sliderView.setVisibility(View.GONE);
                }

                holderTwo.btn_share.setOnClickListener(v -> showMenusShare(v, position));


                holderTwo.img_con.setOnClickListener(v -> {
                    addViewCount(postData.getPost_id() + "");

                    mListener.onChatActivity(new Intent(context, ChatActivity.class)
                            .putExtra(DATA, postData)
                            .putExtra(TYPE, "")
                            .putExtra(SUB_TYPE, "POST")
                            .putExtra("POS", position));
                });
                holderTwo.imgsha.setOnClickListener(v -> {
                    addViewCount(postData.getPost_id() + "");
                    if (Integer.parseInt(postData.getShared_user_count()) > 0) {

                        context.startActivity(new Intent(context, SharelistActivity.class)
                                .putExtra(Constants.Bundle.TYPE, "POST")
                                .putExtra("event_id", postData.getOrgin_id() + "")
                                .putExtra("user_id", postData.getCreated_by() + ""));
                    }
                });


                holderTwo.imgviw.setOnClickListener(v -> {
                    if (Integer.parseInt(postData.getViews_count()) > 0) {
                        context.startActivity(new Intent(context, SharelistActivity.class)
                                .putExtra(Constants.Bundle.TYPE, "POSTVIEW")
                                .putExtra("event_id", postData.getPost_id() + "")
                                .putExtra("user_id", postData.getCreated_by() + ""));
                    }
                });
                holderTwo.itemView.setOnClickListener(v -> addViewCount(postData.getPost_id() + ""));
                holderTwo.txt_less_or_more.setOnClickListener(v -> {
                    if (holderTwo.txt_less_or_more.getText().equals("Read More")) {
                        holderTwo.txt_less_or_more.setText("Read Less");
                        holderTwo.txt_des.setAutoLinkText(postData.getSnap_description());
                        holderTwo.txt_des.setMaxLines(Integer.MAX_VALUE);
                        holderTwo.txt_des.setEllipsize(null);
                    } else {
                        holderTwo.txt_less_or_more.setText("Read More");
                        holderTwo.txt_des.setMaxLines(2);
                        holderTwo.txt_des.setEllipsize(TextUtils.TruncateAt.END);
                    }
                });
                holderTwo.btn_more.setOnClickListener(v ->
                        {
                            addViewCount(postData.getPost_id() + "");
                            if (ids.equals(SharedPref.getUserRegistration().getId())) {
                                showMenusPostOwnerShare(v, position, postData);
                            } else {
                                showMenusNormalUser(v, position, postData);
                            }
                        }
                );

                holderTwo.postedgroup.setOnClickListener(v -> {
                    Family family = new Family();
                    family.setId(Integer.parseInt(postData.getTo_group_id()));
                    Intent intent = new Intent(context, FamilyDashboardActivity.class);
                    intent.putExtra(DATA, family);
                    holder.itemView.getContext().startActivity(intent);
                });
                break;
            case LOADING:
                LoadingVH loadingVH = (LoadingVH) holder;
                if (retryPageLoad) {
                    loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                    loadingVH.mProgressBar.setVisibility(View.GONE);
                    loadingVH.mErrorTxt.setText(
                            errorMsg != null ?
                                    errorMsg :
                                    context.getString(R.string.error_msg_unknown));
                } else {
                    loadingVH.mErrorLayout.setVisibility(View.GONE);
                    loadingVH.mProgressBar.setVisibility(View.VISIBLE);
                }
                break;
        }


    }

    @Override
    public int getItemCount() {
        return repositories.size();
    }


    @Override
    public int getItemViewType(int position) {

        PostData postData = repositories.get(position);
       /* if(position==0&&){}
        else*/
        if (postData == null && isLoadingAdded)
            return LOADING;

        else if (postData != null && postData.getShared_user_names() == null)
            return ITEM;


        else return SHARE;
    }


    public void addLoadingFooter() {
        isLoadingAdded = true;
        repositories.add(null);
        notifyItemInserted(repositories.size() - 1);
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        if (repositories.size() > 0) {
            int position = repositories.size() - 1;
            PostData result = getItem(position);

            if (result == null) {
                repositories.remove(position);
                notifyItemRemoved(position);
            }
        }
    }

    public PostData getItem(int position) {
        return repositories.get(position);
    }


    private String dateFormat(String time) {
        DateTime dateTime = ISODateTimeFormat.dateTimeParser().parseDateTime(time);
        DateTimeFormatter dtfOut = DateTimeFormat.forPattern("MMM dd yyyy hh:mm aa");
        return dtfOut.print(dateTime);
    }

    private void showMenusNormalUser(View v, int position, PostData data) {
        PopupMenu popup = new PopupMenu(v.getContext(), v);
        popup.getMenuInflater().inflate(R.menu.popup_menu_post_user, popup.getMenu());
        if (data.getMuted() != null && data.getMuted()) {
            popup.getMenu().getItem(0).setTitle("UnMute");
        } else {
            popup.getMenu().getItem(0).setTitle("Mute");
        }

        if (isAdmin) {
            popup.getMenu().getItem(3).setVisible(true);

            popup.getMenu().getItem(4).setVisible(true);
        } else {
            popup.getMenu().getItem(3).setVisible(false);
            popup.getMenu().getItem(4).setVisible(false);
        }
        popup.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.mutePost:
                    muteConversation(position);
                    break;
                case R.id.removepost:
                    removePost(repositories.get(position).getPost_id() + "", position);
                    break;
                case R.id.report:
                    Dialoguereport(position);
                    break;

                case R.id.delete:
                    confirmation(position);
                    break;

                case R.id.sticky:
                    confirmationForStiky(position);
                    break;

                case R.id.share:
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_SEND);

                    intent.setType("text/plain");
                    intent.putExtra(Intent.EXTRA_TEXT, Constants.ApiPaths.BASE_URL);
                    context.startActivity(Intent.createChooser(intent, "Share"));
                    break;
            }
            return true;
        });

        popup.show();
    }


    private void showMenusPostOwnerShare(View v, int position, PostData postData) {
        PopupMenu popup = new PopupMenu(v.getContext(), v);
        popup.getMenuInflater().inflate(R.menu.popup_menu_post_owner, popup.getMenu());
        if (postData.getMuted() != null && postData.getMuted()) {
            popup.getMenu().getItem(0).setTitle("UnMute");
        } else {
            popup.getMenu().getItem(0).setTitle("Mute");
        }
        popup.getMenu().getItem(1).setVisible(false);

        popup.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.mutePost:
                    muteConversation(position);
                    break;
                case R.id.editpost:

                    mListener.onEditActivity(new Intent(context, EditPostActivity.class).putExtra("pos", position).putExtra("POST", new Gson().toJson(repositories.get(position))));

                    break;
                case R.id.delete:
                    confirmation(position);
                    break;

                case R.id.share:
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_SEND);

                    intent.setType("text/plain");
                    intent.putExtra(Intent.EXTRA_TEXT, Constants.ApiPaths.BASE_URL);
                    context.startActivity(Intent.createChooser(intent, "Share"));
                    break;
            }
            return true;
        });

        popup.show();
    }

    private void showMenusPostOwner(View v, int position, PostData postData) {
        PopupMenu popup = new PopupMenu(v.getContext(), v);
        popup.getMenuInflater().inflate(R.menu.popup_menu_post_owner, popup.getMenu());
        if (postData.getMuted() != null && postData.getMuted()) {
            popup.getMenu().getItem(0).setTitle("UnMute");
        } else {
            popup.getMenu().getItem(0).setTitle("Mute");
        }
        if (postData.getPublish_type() != null && "request".equals(postData.getPublish_type()) || "albums".equals(postData.getPublish_type()) || "documents".equals(postData.getPublish_type())) {
            popup.getMenu().getItem(1).setVisible(false);
            popup.getMenu().getItem(2).setVisible(false);
        }
        if (isAdmin) {
            popup.getMenu().getItem(3).setVisible(true);
        } else {
            popup.getMenu().getItem(3).setVisible(false);
        }
        popup.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.mutePost:
                    muteConversation(position);
                    break;
                case R.id.editpost:

                    mListener.onEditActivity(new Intent(context, EditPostActivity.class).putExtra("pos", position).putExtra("POST", new Gson().toJson(repositories.get(position))));

                    break;
                case R.id.delete:
                    confirmation(position);
                    break;

                case R.id.sticky:
                    confirmationForStiky(position);
                    break;
            }
            return true;
        });

        popup.show();
    }


    private void showMenusShare(View v, int position) {

        PopupMenu popup = new PopupMenu(v.getContext(), v);
        popup.getMenuInflater().inflate(R.menu.popup_menu_post_share, popup.getMenu());

        if (!SharedPref.userHasFamily()) {
            popup.getMenu().getItem(0).setVisible(false);
        }

        popup.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {

                case R.id.sharefamily:


                    addViewCount(repositories.get(position).getPost_id() + "");
                    context.startActivity(new Intent(context, ShareEventActivity.class).putExtra(Constants.Bundle.TYPE, "POST").putExtra("Post_id", repositories.get(position).getPost_id() + ""));
                    break;


                case R.id.sharesocial:
                    String url = Constants.ApiPaths.SHARE_URL + "page/posts/" + repositories.get(position).getPost_id();

                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_SEND);
                    intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "post");
                    intent.setType("text/plain");
                    intent.putExtra(Intent.EXTRA_TEXT, url);
                    context.startActivity(Intent.createChooser(intent, "Share"));
                    break;
            }
            return true;
        });

        popup.show();
    }

    private void confirmationForStiky(int position) {

        SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                .setContentText("Do you want to stick this post?")
                .setConfirmText("Yes")
                .setCancelText("No");

        pDialog.show();
        pDialog.setConfirmClickListener(sDialog -> {
            mListener.makeAsSticky(position);
            pDialog.cancel();
        });
        pDialog.setCancelClickListener(SweetAlertDialog::cancel);
    }

    static class ViewHolderTwo extends RecyclerView.ViewHolder {
        @BindView(R.id.unread_status)
        View unread_status;
        @BindView(R.id.txt_less_or_more)
        TextView txt_less_or_more;
        @BindView(R.id.innerprofileImage)
        ImageView innerprofileImage;
        @BindView(R.id.postusername)
        TextView postusername;

        @BindView(R.id.innerpostedgroup)
        TextView innerpostedgroup;
        @BindView(R.id.postedgroup)
        TextView postedgroup;
        @BindView(R.id.innerpostusername)
        TextView innerpostusername;
        @BindView(R.id.postdate)
        TextView postdate;
        @BindView(R.id.txt_count)
        TextView txt_count;

        @BindView(R.id.txt_des)
        com.luseen.autolinklibrary.AutoLinkTextView txt_des;

        @BindView(R.id.txt_temp)
        TextView txt_temp;

        @BindView(R.id.share_count)
        TextView share_count;

        @BindView(R.id.view_count)
        TextView view_count;


        @BindView(R.id.imageSlider)
        com.smarteist.autoimageslider.SliderView sliderView;


        @BindView(R.id.btn_share)
        ImageView btn_share;

        @BindView(R.id.img_con)
        ImageView img_con;

        @BindView(R.id.imgsha)
        ImageView imgsha;


        @BindView(R.id.imgviw)
        ImageView imgviw;
        @BindView(R.id.btn_more)
        ImageView btn_more;


        @BindView(R.id.link_preview)
        LinearLayout link_preview;

        @BindView(R.id.url_img)
        ImageView url_img;

        @BindView(R.id.txt_url_des)
        TextView txt_url_des;

        @BindView(R.id.txt_url)
        TextView txt_url;


        ViewHolderTwo(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(repositories.size() - 1);

        if (errorMsg != null) this.errorMsg = errorMsg;
    }


    protected class LoadingVH extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ProgressBar mProgressBar;
        private TextView mErrorTxt;
        private LinearLayout mErrorLayout;

        LoadingVH(View itemView) {
            super(itemView);

            mProgressBar = itemView.findViewById(R.id.loadmore_progress);
            ImageButton mRetryBtn = itemView.findViewById(R.id.loadmore_retry);
            mErrorTxt = itemView.findViewById(R.id.loadmore_errortxt);
            mErrorLayout = itemView.findViewById(R.id.loadmore_errorlayout);

            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.loadmore_retry:
                case R.id.loadmore_errorlayout:

                    showRetry(false, null);
                    mCallback.retryPageLoad();

                    break;
            }
        }
    }


    private int getinnerwidgetsize() {

        float screenWidth = Resources.getSystem().getDisplayMetrics().widthPixels - 100;
        float s = screenWidth / 4;
        return Math.round(s * 3);
    }

    private int getinnerwidgetsize(String width, String hight) {
        float screenWidth = Resources.getSystem().getDisplayMetrics().widthPixels - 100;
        float wf = Float.parseFloat(width);
        float hf = Float.parseFloat(hight);
        return Math.round((screenWidth / wf) * hf);

    }


    private int getwidgetsize() {
        float screenWidth = Resources.getSystem().getDisplayMetrics().widthPixels;
        float s = screenWidth / 4;
        return Math.round(s * 3);
    }

    private int getwidgetsize(String width, String hight) {


        float screenWidth;
        float wf = Float.parseFloat(width);
        float hf = Float.parseFloat(hight);
        if (wf > 450) {
            screenWidth = Resources.getSystem().getDisplayMetrics().widthPixels + 40;
        } else {
            screenWidth = Resources.getSystem().getDisplayMetrics().widthPixels;
        }

        return Math.round((screenWidth / wf) * hf);
    }

    private void deletePost(String id, int position) {
        SweetAlertDialog progressDialog = Utilities.getProgressDialog(context);
        progressDialog.show();
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id", id);
        jsonObject.addProperty("user_id", SharedPref.getUserRegistration().getId());
        RequestBody requestBody = RequestBody.create(jsonObject.toString(), MediaType.parse("application/json; charset=utf-8"));
        FamilheeyApplication application = FamilheeyApplication.get(context);
        ApiServices apiServices = RetrofitBase.createRxResource(context, ApiServices.class);
        subscriptions.add(apiServices.deletePost(requestBody)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(application.defaultSubscribeScheduler())
                .subscribe(response -> {
                    repositories.remove(position);
                    notifyDataSetChanged();
                    progressDialog.dismiss();
                }, throwable -> progressDialog.dismiss()));
    }

    private void muteConversation(int position) {

        SweetAlertDialog progressDialog = Utilities.getProgressDialog(context);
        progressDialog.show();
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("user_id", SharedPref.getUserRegistration().getId());
        jsonObject.addProperty("post_id", repositories.get(position).getPost_id() + "");
        RequestBody requestBody = RequestBody.create(jsonObject.toString(), MediaType.parse("application/json; charset=utf-8"));
        FamilheeyApplication application = FamilheeyApplication.get(context);
        ApiServices apiServices = RetrofitBase.createRxResource(context, ApiServices.class);
        subscriptions.add(apiServices.muteConversation(requestBody)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(application.defaultSubscribeScheduler())
                .subscribe(response -> {
                    assert response.body() != null;
                    repositories.get(position).setMuted(response.body().getData().getIs_active());
                    notifyDataSetChanged();
                    progressDialog.dismiss();
                }, throwable -> progressDialog.dismiss()));
    }


    private void openAppGetParams(String url) {
        // UserNotification
        SweetAlertDialog progressDialog = Utilities.getProgressDialog(context);
        progressDialog.show();
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("url", url);
        RequestBody requestBody = RequestBody.create(jsonObject.toString(), MediaType.parse("application/json; charset=utf-8"));
        FamilheeyApplication application = FamilheeyApplication.get(context);
        ApiServices apiServices = RetrofitBase.createRxResource(context, ApiServices.class);
        subscriptions.add(apiServices.openAppGetParams(requestBody)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(application.defaultSubscribeScheduler())
                .subscribe(response -> {
                    progressDialog.dismiss();
                    response.body().getData().goToCorrespondingDashboard(context);
                }, throwable -> progressDialog.dismiss()));
    }


    private void removePost(String post_id, int position) {

        SweetAlertDialog progressDialog = Utilities.getProgressDialog(context);
        progressDialog.show();
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("user_id", SharedPref.getUserRegistration().getId());
        jsonObject.addProperty("post_id", post_id);
        RequestBody requestBody = RequestBody.create(jsonObject.toString(), MediaType.parse("application/json; charset=utf-8"));
        FamilheeyApplication application = FamilheeyApplication.get(context);
        ApiServices apiServices = RetrofitBase.createRxResource(context, ApiServices.class);
        subscriptions.add(apiServices.removePost(requestBody)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(application.defaultSubscribeScheduler())
                .subscribe(response -> {
                    repositories.remove(position);
                    notifyDataSetChanged();
                    progressDialog.dismiss();
                }, throwable -> progressDialog.dismiss()));
    }

    private void addViewCount(String post_id) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("user_id", SharedPref.getUserRegistration().getId());
        jsonObject.addProperty("post_id", post_id);
        RequestBody requestBody = RequestBody.create(jsonObject.toString(), MediaType.parse("application/json; charset=utf-8"));
        FamilheeyApplication application = FamilheeyApplication.get(context);
        ApiServices apiServices = RetrofitBase.createRxResource(context, ApiServices.class);
        subscriptions.add(apiServices.addViewCount(requestBody)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(application.defaultSubscribeScheduler())
                .subscribe(response -> {
                }, throwable -> {
                }));
    }

    private void confirmation(int position) {

        SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                .setContentText("Are you sure you want to delete this post?")
                .setConfirmText("Yes")
                .setCancelText("No");

        pDialog.show();
        pDialog.setConfirmClickListener(sDialog -> {
            deletePost(repositories.get(position).getPost_id() + "", position);
            pDialog.cancel();
        });
        pDialog.setCancelClickListener(SweetAlertDialog::cancel);
    }

    public interface postItemClick {
        void onEditActivity(Intent intent);

        void onChatActivity(Intent intent);

        void onSearchTag(String hashtag);

        void makeAsSticky(int poston);

    }

    static class ViewHolderOne extends RecyclerView.ViewHolder {

        @BindView(R.id.unread_status)
        View unread_status;
        @BindView(R.id.txt_less_or_more)
        TextView txt_less_or_more;
        @BindView(R.id.imgsha)
        ImageView imgsha;
        @BindView(R.id.profileImage)
        ImageView profileImage;
        @BindView(R.id.txt_temp)
        TextView txt_temp;
        @BindView(R.id.postusername)
        TextView postusername;
        @BindView(R.id.postedgroup)
        TextView postedgroup;
        @BindView(R.id.postdate)
        TextView postdate;
        @BindView(R.id.txt_count)
        TextView txt_count;
        @BindView(R.id.img_con)
        ImageView img_con;
        @BindView(R.id.txt_des)
        com.luseen.autolinklibrary.AutoLinkTextView txt_des;
        @BindView(R.id.imageSlider)
        com.smarteist.autoimageslider.SliderView sliderView;
        @BindView(R.id.view_count)
        TextView view_count;
        @BindView(R.id.imgviw)
        ImageView imgviw;

        @BindView(R.id.btn_more)
        ImageView btn_more;
        @BindView(R.id.btn_share)
        ImageView btn_share;


        @BindView(R.id.share_count)
        TextView share_count;
        @BindView(R.id.bottom_view)
        RelativeLayout bottom_view;

        @BindView(R.id.link_preview)
        LinearLayout link_preview;

        @BindView(R.id.url_img)
        ImageView url_img;

        @BindView(R.id.txt_url_des)
        TextView txt_url_des;

        @BindView(R.id.txt_url)
        TextView txt_url;


        @BindView(R.id.thanks_post_view)
        LinearLayout thanks_post_view;


        @BindView(R.id.middle_first_view)
        LinearLayout middle_first_view;
        @BindView(R.id.thanks_post_img)
        ImageView thanks_post_img;
        @BindView(R.id.text_description)
        TextView text_description;
        @BindView(R.id.txt_request_item)
        TextView txt_request_item;
        @BindView(R.id.btn_open_request)
        TextView btn_open_request;
        @BindView(R.id.rv_name_list)
        RecyclerView rv_name_list;

        @BindView(R.id.album_view)
        RelativeLayout album_view;

        @BindView(R.id.middle_view)
        LinearLayout middle_view;

        @BindView(R.id.txt_album_des)
        TextView txt_album_des;

        @BindView(R.id.btn_open_album)
        TextView btn_open_album;

        ViewHolderOne(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    private void Dialoguereport(int position) {

        final Dialog dialog = new Dialog(context);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialogue_report);
        dialog.setCanceledOnTouchOutside(false);
        TextInputEditText editText = dialog.findViewById(R.id.description);

        dialog.findViewById(R.id.btn_close).setOnClickListener(view -> dialog.dismiss());
        dialog.findViewById(R.id.btndone).setOnClickListener(v -> {
            if (Objects.requireNonNull(editText.getText()).toString().trim().equals("")) {
                Toast.makeText(context, "Reason is required", Toast.LENGTH_LONG).show();
            } else {
                reportPost(repositories.get(position).getPost_id() + "", editText.getText().toString().trim(), dialog);
            }
        });

        dialog.show();
    }

    private void reportPost(String post_id, String des, Dialog dialog) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("user_id", SharedPref.getUserRegistration().getId());
        jsonObject.addProperty("type_id", post_id);
        jsonObject.addProperty("description", des);
        jsonObject.addProperty("spam_page_type", "post");
        RequestBody requestBody = RequestBody.create(jsonObject.toString(), MediaType.parse("application/json; charset=utf-8"));
        FamilheeyApplication application = FamilheeyApplication.get(context);
        ApiServices apiServices = RetrofitBase.createRxResource(context, ApiServices.class);
        subscriptions.add(apiServices.reportPost(requestBody)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(application.defaultSubscribeScheduler())
                .subscribe(response -> {
                    dialog.dismiss();
                    Toast.makeText(context, "Reported successfully.We will review the post and do the needful,thank you", Toast.LENGTH_LONG).show();
                }, throwable -> {
                }));
    }
}