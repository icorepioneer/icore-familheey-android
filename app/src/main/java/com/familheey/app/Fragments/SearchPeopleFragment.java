package com.familheey.app.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.familheey.app.Activities.MyFamiliesActivity;
import com.familheey.app.Adapters.PeopleSearchAdapter;
import com.familheey.app.CustomViews.TextViews.SemiBoldTextView;
import com.familheey.app.Interfaces.GlobalSearchListener;
import com.familheey.app.Interfaces.RetrofitListener;
import com.familheey.app.Models.ApiCallbackParams;
import com.familheey.app.Models.ErrorData;
import com.familheey.app.Models.Response.PeopleSearchModal;
import com.familheey.app.Networking.Retrofit.ApiServiceProvider;
import com.familheey.app.Parsers.FamilyParser;
import com.familheey.app.R;
import com.familheey.app.Utilities.Constants;
import com.familheey.app.Utilities.SharedPref;
import com.familheey.app.Utilities.Utilities;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.familheey.app.Utilities.Constants.Bundle.DATA;
import static com.familheey.app.Utilities.Constants.Bundle.IDENTIFIER;

public class SearchPeopleFragment extends Fragment implements PeopleSearchAdapter.OnPeopleJoinInteraction, RetrofitListener {


    private final ArrayList<PeopleSearchModal> peopleSearchArrayList = new ArrayList<>();
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    @BindView(R.id.searchLabelIndicator)
    TextView searchLabelIndicator;
    @BindView(R.id.emptyResultText)
    SemiBoldTextView emptyResultText;
//    @BindView(R.id.progressBar)
//    ProgressBar progressBar;
    private PeopleSearchAdapter peopleSearchAdapter;
    private GlobalSearchListener globalSearchListener;
    private boolean defaultSearchEnabled = false;
    private boolean isSuggestionTextNeeded = true;
    @BindView(R.id.shimmer_view_container)
    com.facebook.shimmer.ShimmerFrameLayout shimmer_view_container;

    public static SearchPeopleFragment newInstance(boolean defaultSearchEnabled, boolean isSuggestionTextNeeded) {
        SearchPeopleFragment searchPeopleFragment = new SearchPeopleFragment();
        Bundle args = new Bundle();
        args.putBoolean(DATA, defaultSearchEnabled);
        args.putBoolean(IDENTIFIER, isSuggestionTextNeeded);
        searchPeopleFragment.setArguments(args);
        return searchPeopleFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            defaultSearchEnabled = getArguments().getBoolean(DATA, false);
            isSuggestionTextNeeded = getArguments().getBoolean(IDENTIFIER, true);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.search_result_people_fragment, container, false);
        ButterKnife.bind(this, view);
        peopleSearchAdapter = new PeopleSearchAdapter(this, peopleSearchArrayList, this);
        recyclerview.setHasFixedSize(true);
        recyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerview.setAdapter(peopleSearchAdapter);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (defaultSearchEnabled) {
            if (shimmer_view_container != null) {
                shimmer_view_container.setVisibility(View.VISIBLE);
                shimmer_view_container.startShimmer();
            }
        }
        if (isSuggestionTextNeeded)
            searchLabelIndicator.setVisibility(View.VISIBLE);
        else searchLabelIndicator.setVisibility(View.INVISIBLE);

    }

    public void updatePeople(ArrayList<PeopleSearchModal> peopleSearchModalArrayList) {
        this.peopleSearchArrayList.clear();
        this.peopleSearchArrayList.addAll(peopleSearchModalArrayList);
        if (peopleSearchArrayList.size() == 0)
            emptyResultText.setVisibility(View.VISIBLE);
        else
            emptyResultText.setVisibility(View.INVISIBLE);
        if (shimmer_view_container != null) {
            shimmer_view_container.stopShimmer();
            shimmer_view_container.setVisibility(View.GONE);
        }
        peopleSearchAdapter.hideAddToTopics(true);
        peopleSearchAdapter.notifyDataSetChanged();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        globalSearchListener = Utilities.getListener(this, GlobalSearchListener.class);
        if (globalSearchListener == null)
            throw new RuntimeException(context.getClass().getSimpleName() + " must implement GlobalSearchListener");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        globalSearchListener = null;
    }

    void fetchPeoples(String query) {
        if (shimmer_view_container != null) {
            shimmer_view_container.setVisibility(View.VISIBLE);
            shimmer_view_container.startShimmer();
        }
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("userid", SharedPref.getUserRegistration().getId());//
        jsonObject.addProperty("type", "users");
        jsonObject.addProperty("searchtxt", query);
        jsonObject.addProperty("offset", "0");
        jsonObject.addProperty("limit", "10000");
        ApiServiceProvider apiServiceProvider = ApiServiceProvider.getInstance(getContext());
        apiServiceProvider.searchData(jsonObject, null, this);
    }

    @Override
    public void onUserSelected(PeopleSearchModal user) {
        startActivity(new Intent(getActivity(), MyFamiliesActivity.class).putExtra(Constants.Bundle.DATA, user));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        globalSearchListener.requsetGlobalSearch();
    }

    void updateSearchIndication(String searchText) {
        if (searchLabelIndicator == null) return;
        peopleSearchArrayList.clear();
        peopleSearchAdapter.notifyDataSetChanged();
        if (searchText.length() == 0) {
            searchLabelIndicator.setText("Suggested");
        } else {
            searchLabelIndicator.setText("Showing results for \"" + searchText + "\"");
        }
    }

    @Override
    public void onResponseSuccess(String responseBodyString, ApiCallbackParams apiCallbackParams, int apiFlag) {
        if (shimmer_view_container != null) {
            shimmer_view_container.stopShimmer();
            shimmer_view_container.setVisibility(View.GONE);
        }
        try {
            JSONObject mainObject = new JSONObject(responseBodyString);
            JSONArray peopleArray = mainObject.getJSONArray("users");
            peopleSearchArrayList.clear();
            peopleSearchArrayList.addAll(FamilyParser.parseSearchedPeoples(peopleArray));
            peopleSearchAdapter.notifyDataSetChanged();
            if (peopleSearchArrayList.size() == 0)
                emptyResultText.setVisibility(View.VISIBLE);
            else
                emptyResultText.setVisibility(View.INVISIBLE);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResponseError(ErrorData errorData, ApiCallbackParams apiCallbackParams, Throwable throwable, int apiFlag) {
        if (shimmer_view_container != null) {
            shimmer_view_container.stopShimmer();
            shimmer_view_container.setVisibility(View.GONE);
        }
    }
}
