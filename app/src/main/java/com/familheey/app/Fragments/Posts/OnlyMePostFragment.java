package com.familheey.app.Fragments.Posts;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.familheey.app.FamilheeyApplication;
import com.familheey.app.Models.Response.FamilyMember;
import com.familheey.app.Networking.Retrofit.ApiServices;
import com.familheey.app.Networking.Retrofit.RetrofitBase;
import com.familheey.app.R;
import com.familheey.app.Utilities.SharedPref;
import com.familheey.app.pagination.PaginationAdapterCallback;
import com.familheey.app.pagination.PaginationScrollListener;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnEditorAction;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class OnlyMePostFragment extends Fragment implements PaginationAdapterCallback, UserPostAdapter.OnSearchQueryChangedListener {

    public CompositeDisposable subscriptions;
    @BindView(R.id.searchPost)
    EditText searchPost;
    @BindView(R.id.clearSearch)
    ImageView clearSearch;

    @BindView(R.id.layoutNoPost)
    LinearLayout layoutNoPost;

    @BindView(R.id.postList)
    RecyclerView postList;
    private final List<PostData> data = new ArrayList<>();
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int TOTAL_PAGES = 5;
    private int currentPage = 0;
    private String query = "";
    private UserPostAdapter adapter;
    private Integer prev_id=0;
    public OnlyMePostFragment() {
        // Required empty public constructor
    }

    public static OnlyMePostFragment newInstance(FamilyMember familyMember) {
        OnlyMePostFragment fragment = new OnlyMePostFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        subscriptions = new CompositeDisposable();
        assert getArguments() != null;
        getArguments().clear();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_only_me_post, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new UserPostAdapter(getContext(), data, this, this);
        setupRecyclerView();
        initializeSearchClearCallback();
    }

    private void initializeSearchClearCallback() {
        searchPost.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0)
                    clearSearch.setVisibility(View.INVISIBLE);
                else clearSearch.setVisibility(View.VISIBLE);
            }
        });
        clearSearch.setOnClickListener(v -> {
            searchPost.setText("");
            onSearchQueryListener(EditorInfo.IME_ACTION_SEARCH);
        });
    }

    private void setupRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        postList.setLayoutManager(layoutManager);
        postList.setItemAnimator(new DefaultItemAnimator());
        postList.setAdapter(adapter);
        postList.addOnScrollListener(new PaginationScrollListener(layoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                //currentPage = data.size();
                getPost();
            }

            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                if(newState==RecyclerView.SCROLL_STATE_IDLE){
                    int c = layoutManager.findFirstVisibleItemPosition();
                    if (c > 0) {
                        if (data.get(c) != null) {
                            if (data.get(c).getPost_id() != null) {
                                if(!(prev_id.equals(data.get(c).getPost_id()))) {
                                    addViewCount(data.get(c).getPost_id() + "");
                                    prev_id = data.get(c).getPost_id();
                                }
                            }
                        }
                    }
                }
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

    }

    private void addViewCount(String post_id) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("user_id", SharedPref.getUserRegistration().getId());
        jsonObject.addProperty("post_id", post_id);
        RequestBody requestBody = RequestBody.create(jsonObject.toString(), MediaType.parse("application/json; charset=utf-8"));
        FamilheeyApplication application = FamilheeyApplication.get(getActivity());
        ApiServices apiServices = RetrofitBase.createRxResource(getActivity(), ApiServices.class);
        subscriptions.add(apiServices.addViewCount(requestBody)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(application.defaultSubscribeScheduler())
                .subscribe(response -> {
                }, throwable -> {
                }));
    }

    private void getPost() {
        if (currentPage == 0) {
            progressBar.setVisibility(View.VISIBLE);
        }
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("user_id", SharedPref.getUserRegistration().getId());
        jsonObject.addProperty("type", "post");
        jsonObject.addProperty("query", query);
        jsonObject.addProperty("offset", currentPage + "");
        jsonObject.addProperty("limit", "30");
        RequestBody requestBody = RequestBody.create(jsonObject.toString(), MediaType.parse("application/json; charset=utf-8"));
        FamilheeyApplication application = FamilheeyApplication.get(getActivity());
        ApiServices apiServices = RetrofitBase.createRxResource(getActivity(), ApiServices.class);
        subscriptions.add(apiServices.getMyPost(requestBody)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(application.defaultSubscribeScheduler())
                .subscribe(response -> {
                    if (currentPage > 0) {
                        adapter.removeLoadingFooter();
                        isLoading = false;
                    }


                    progressBar.setVisibility(View.GONE);
                    if (response.body() != null) {
                        data.addAll(response.body().getData());
                        currentPage = data.size();
                        if (data.size() == 0) {
                            layoutNoPost.setVisibility(View.VISIBLE);
                        } else {
                            layoutNoPost.setVisibility(View.GONE);

                        }
                    }


                    adapter.notifyDataSetChanged();

                    if (response.body().getData().size() == 150) adapter.addLoadingFooter();
                    else isLastPage = true;

                }, throwable -> {
                    progressBar.setVisibility(View.GONE);
                    adapter.showRetry(true, null);
                }));
    }

    public void searchPost(String query) {
        this.query = query;
        isLastPage = false;
        data.clear();
        currentPage = 0;
        getPost();
    }

    @OnEditorAction(R.id.searchPost)
    protected boolean onSearchQueryListener(int actionId) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            searchPost(searchPost.getText().toString());
            try {
                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(searchPost.getWindowToken(), 0);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return true;
        }
        return false;
    }

    @Override
    public void retryPageLoad() {
        getPost();
    }

    @Override
    public void onSearchQueryChanged(String searchQuery) {
        searchPost.setText(searchQuery);
        onSearchQueryListener(EditorInfo.IME_ACTION_SEARCH);
    }

    @Override
    public void onResume() {
        super.onResume();
        isLoading = false;
        isLastPage = false;
        TOTAL_PAGES = 5;
        currentPage = 0;
        query = "";
        if (data != null)
            data.clear();
        if (adapter != null)
            adapter.notifyDataSetChanged();
        getPost();
    }
}
