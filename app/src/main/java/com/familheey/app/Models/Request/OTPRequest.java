package com.familheey.app.Models.Request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OTPRequest {

    @SerializedName("otp")
    @Expose
    private String receivedOTP;
    @SerializedName("phone")
    @Expose
    private String mobileNumber;

    public String getReceivedOTP() {
        return receivedOTP;
    }

    public void setReceivedOTP(String receivedOTP) {
        this.receivedOTP = receivedOTP;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
}
