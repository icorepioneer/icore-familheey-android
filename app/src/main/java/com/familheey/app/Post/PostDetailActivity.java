package com.familheey.app.Post;

import android.Manifest;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.ViewCompat;
import androidx.viewpager.widget.ViewPager;

import com.familheey.app.Models.Request.HistoryImages;
import com.familheey.app.R;
import com.familheey.app.Utilities.HackyViewPager;
import com.familheey.app.Utilities.Utilities;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.util.Util;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.tbuonomo.viewpagerdotsindicator.WormDotsIndicator;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.familheey.app.Utilities.Constants.ApiPaths.IMAGE_BASE_URL;
import static com.familheey.app.Utilities.Constants.Bundle.DATA;
import static com.familheey.app.Utilities.Constants.Bundle.DETAIL;

public class PostDetailActivity extends AppCompatActivity {

    public static final String VIEW_NAME_HEADER_IMAGE = "detail:header:image";

    @BindView(R.id.imagedownload)
    ImageView imagedownload;
    @BindView(R.id.pager)
    HackyViewPager pager;
    private int pos = 0;
    @BindView(R.id.onback)
    ImageView onback;
    private String type = "";
    private SimpleExoPlayer player;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_detail);
        ButterKnife.bind(this);

    }

    private void init() {
        player = ExoPlayerFactory.newSimpleInstance(this);
        WormDotsIndicator dotsIndicator = findViewById(R.id.worm_dots_indicator);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            ArrayList<HistoryImages> historyImages = new Gson().fromJson(getIntent().getExtras().getString(DATA), new TypeToken<ArrayList<HistoryImages>>() {
            }.getType());
            if (getIntent().getExtras().getString(DETAIL) != null) {
                type = getIntent().getExtras().getString(DETAIL);
            }

            if (historyImages != null) {
                pos = getIntent().getExtras().getInt("pos");
                ViewCompat.setTransitionName(pager, VIEW_NAME_HEADER_IMAGE);
                pager.setAdapter(new PostPagerAdapter(this, historyImages, player, type));
                pager.setOffscreenPageLimit(0);
                dotsIndicator.setViewPager(pager);
                pager.setCurrentItem(pos);
                onback.setOnClickListener(v -> onBackPressed());
                if (historyImages.size() == 1) {
                    dotsIndicator.setVisibility(View.GONE);
                } else {
                    dotsIndicator.setVisibility(View.VISIBLE);
                }

                imagedownload.setVisibility(View.VISIBLE);

                pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                    }

                    @Override
                    public void onPageSelected(int position) {
                        if (historyImages.get(position).getType().contains("image")) {
                            imagedownload.setVisibility(View.VISIBLE);

                        } else {
                            imagedownload.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {

                    }
                });
            }

            imagedownload.setOnClickListener(view -> {
                        if (isReadStoragePermissionGranted()) {
                            assert historyImages != null;
                            String url = IMAGE_BASE_URL + "post/" + historyImages.get(pager.getCurrentItem()).getFilename();
                            if ("albums".equals(type)) {
                                url = IMAGE_BASE_URL + "Documents/" + historyImages.get(pager.getCurrentItem()).getFilename();
                            }

                            Toast.makeText(this, "Downloading media...", Toast.LENGTH_SHORT).show();
                            Utilities.downloadDocuments(this, url, historyImages.get(pager.getCurrentItem()).getFilename());
                        } else
                            showPermission();

                    }
            );
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (Util.SDK_INT > 23) player.release();
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }

    private boolean isReadStoragePermissionGranted() {
        return TedPermission.isGranted(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }

    private void requestPermission() {
        TedPermission.with(this)
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        imagedownload.performClick();
                    }

                    @Override
                    public void onPermissionDenied(List<String> deniedPermissions) {

                    }
                })
                .setDeniedMessage("If you reject permission,you can not download images\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
                .check();
    }

    private void showPermission() {
        Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialogue_permission);
        Window window = dialog.getWindow();
        assert window != null;
        window.setBackgroundDrawableResource(android.R.color.transparent);

        TextView txt_decs = dialog.findViewById(R.id.txt_decs);
        txt_decs.setText("To view images, allow Familheey access to your device's photos,media, and files.");
        dialog.findViewById(R.id.btn_cancel).setOnClickListener(view -> dialog.dismiss());
        dialog.findViewById(R.id.btn_continue).setOnClickListener(view -> {
            dialog.dismiss();
            requestPermission();
        });
        dialog.show();
    }
}
