package com.familheey.app.Post;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.familheey.app.FamilheeyApplication;
import com.familheey.app.Fragments.Posts.PostData;
import com.familheey.app.Fragments.Posts.UserPostAggregateAdapter;
import com.familheey.app.Networking.Retrofit.ApiServices;
import com.familheey.app.Networking.Retrofit.RetrofitBase;
import com.familheey.app.R;
import com.familheey.app.Utilities.SharedPref;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class PostAggregateListingActivity extends AppCompatActivity {


    @BindView(R.id.shimmer_view_container)
    com.facebook.shimmer.ShimmerFrameLayout shimmerFrameLayout;
    @BindView(R.id.refresh)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.list_recyclerView)
    RecyclerView listRecyclerView;
    @BindView(R.id.toolBarTitle)
    TextView toolBarTitle;
    @BindView(R.id.goBack)
    ImageView goBack;
    @BindView(R.id.layoutEmpty)
    LinearLayout layoutEmpty;
    @BindView(R.id.linearLayout3)
    RelativeLayout linearLayout3;


    private CompositeDisposable subscriptions;
    private List<PostData> data = new ArrayList<>();
    private UserPostAggregateAdapter adapter;
    private String PostRefId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_aggregate_listing);
        init();
    }


    private void init() {
        ButterKnife.bind(this);
        subscriptions = new CompositeDisposable();
        if (getIntent().getExtras() != null && getIntent().hasExtra("id")) {
            PostRefId = getIntent().getExtras().getString("id");
        }
        getPost();
        toolBarTitle.setText("");
        goBack.setOnClickListener(v -> onBackPressed());
        listRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new UserPostAggregateAdapter(this, data);
        listRecyclerView.setAdapter(adapter);

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            data.clear();
            adapter.notifyDataSetChanged();
            mSwipeRefreshLayout.setRefreshing(false);
            getPost();
        });
    }

    private void getPost() {
        shimmerFrameLayout.setVisibility(View.VISIBLE);
        shimmerFrameLayout.startShimmer();
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("user_id", SharedPref.getUserRegistration().getId());
        jsonObject.addProperty("type", "post");
        jsonObject.addProperty("offset", "0");
        jsonObject.addProperty("limit", "100");
        jsonObject.addProperty("post_ref_id", PostRefId);
        RequestBody requestBody = RequestBody.create(jsonObject.toString(), MediaType.parse("application/json; charset=utf-8"));
        FamilheeyApplication application = FamilheeyApplication.get(this);
        ApiServices apiServices = RetrofitBase.createRxResource(this, ApiServices.class);

        subscriptions.add(apiServices.getPostAggregate(requestBody)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(application.defaultSubscribeScheduler())
                .subscribe(response -> {
                    shimmerFrameLayout.setVisibility(View.GONE);

                    linearLayout3.setVisibility(View.VISIBLE);
                    layoutEmpty.setVisibility(View.GONE);
                    assert response.body() != null;
                    data.addAll(response.body().getData());
                    adapter.notifyDataSetChanged();

                }, throwable -> {
                    shimmerFrameLayout.setVisibility(View.GONE);
                    if (throwable instanceof IOException) {
                        networkError();
                    }
                }));


    }


    private void networkError() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Uh oh! Check your internet connection and retry.")
                .setCancelable(false)
                .setPositiveButton("Retry", (dialog, which) -> getPost()).setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        AlertDialog alert = builder.create();
        alert.setTitle("Connection Unavailable");
        alert.show();
        params.setMargins(0, 0, 20, 0);
        alert.getButton(DialogInterface.BUTTON_NEGATIVE).setLayoutParams(params);


    }

}


