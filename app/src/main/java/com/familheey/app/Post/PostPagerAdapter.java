package com.familheey.app.Post;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.familheey.app.Models.Request.HistoryImages;
import com.familheey.app.R;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import java.util.ArrayList;

import static com.familheey.app.Utilities.Constants.ApiPaths.IMAGE_BASE_URL;
import static com.familheey.app.Utilities.Constants.ApiPaths.S3_DEV_IMAGE_URL_SQUARE_DETAILED;

public class PostPagerAdapter extends PagerAdapter {
    private Context context;
    private ArrayList<HistoryImages> historyImages;

    private SimpleExoPlayer player;
    private String type;

    PostPagerAdapter(Context context, ArrayList<HistoryImages> historyImages, SimpleExoPlayer player, String type) {

        this.historyImages = historyImages;
        this.context = context;
        this.player = player;
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.transforms(new RoundedCorners(16));
        this.type = type;
        // player = ExoPlayerFactory.newSimpleInstance(context);
    }


    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        HistoryImages images = historyImages.get(position);
        LayoutInflater inflater = LayoutInflater.from(context);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.item_post_pager, container, false);


        PhotoView photoView = layout.findViewById(R.id.image);
        ProgressBar progressBar = layout.findViewById(R.id.progressBar);
        ImageView volume_on = layout.findViewById(R.id.volume_on);
        ImageView volume_off = layout.findViewById(R.id.volume_off);
        ImageView doc = layout.findViewById(R.id.doc);
        RelativeLayout videoview = layout.findViewById(R.id.videoview);
        RelativeLayout photoContainer = layout.findViewById(R.id.photoContainer);
        com.google.android.exoplayer2.ui.PlayerView video_view = layout.findViewById(R.id.video_view);


        if (images.getType().contains("image")) {
            photoContainer.setVisibility(View.VISIBLE);
            doc.setVisibility(View.GONE);
            videoview.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            String url = S3_DEV_IMAGE_URL_SQUARE_DETAILED + IMAGE_BASE_URL + "post/" + images.getFilename();
            if ("albums".equals(type)) {
                url = S3_DEV_IMAGE_URL_SQUARE_DETAILED + IMAGE_BASE_URL + "Documents/" + images.getFilename();
            }
            Glide.with(context)
                    .load(url)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, com.bumptech.glide.load.DataSource dataSource, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            return false;
                        }
                    }).into(photoView);

        } else if (images.getType().contains("video")) {
            photoContainer.setVisibility(View.GONE);
            doc.setVisibility(View.GONE);
            videoview.setVisibility(View.VISIBLE);
            DataSource.Factory mediaDataSourceFactory = new DefaultDataSourceFactory(context, Util.getUserAgent(context, "Familheey"));

            String url = IMAGE_BASE_URL + "post/" + images.getFilename();
            if ("albums".equals(type)) {
                url = IMAGE_BASE_URL + "Documents/" + images.getFilename();
            }

            ProgressiveMediaSource mediaSource = new ProgressiveMediaSource.Factory(mediaDataSourceFactory)
                    .createMediaSource(Uri.parse(url));
            player.prepare(mediaSource, false, false);
            player.setPlayWhenReady(false);
            video_view.setShutterBackgroundColor(Color.TRANSPARENT);
            video_view.setPlayer(player);
            video_view.requestFocus();
            float currentvolume = player.getVolume();
            volume_on.setOnClickListener(v -> {
                volume_on.setVisibility(View.GONE);
                volume_off.setVisibility(View.VISIBLE);
                player.setVolume(0f);
            });

            volume_off.setOnClickListener(v -> {
                volume_on.setVisibility(View.VISIBLE);
                volume_off.setVisibility(View.GONE);
                player.setVolume(currentvolume);
            });

        } else {
            //player.release();

            photoContainer.setVisibility(View.GONE);
            doc.setVisibility(View.VISIBLE);
            videoview.setVisibility(View.GONE);
            doc.setOnClickListener(v -> {

                String url = IMAGE_BASE_URL + "post/" + images.getFilename();
                if ("albums".equals(type)) {
                    url = IMAGE_BASE_URL + "Documents/" + images.getFilename();
                }

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                context.startActivity(browserIntent);
            });
        }


        container.addView(layout);
        return layout;
    }

    @Override
    public int getCount() {
        return historyImages.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

}