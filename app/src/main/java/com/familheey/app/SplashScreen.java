package com.familheey.app;

import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.ProgressBar;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;

import com.familheey.app.Activities.CreatedEventDetailActivity;
import com.familheey.app.Activities.FamilyDashboardActivity;
import com.familheey.app.Activities.LoginActivity;
import com.familheey.app.Activities.UserRegistrationActivity;
import com.familheey.app.Announcement.AnnouncementDetailActivity;
import com.familheey.app.Models.Response.UserRegistrationResponse;
import com.familheey.app.Models.Response.UserSettings;
import com.familheey.app.Networking.Retrofit.ApiServices;
import com.familheey.app.Networking.Retrofit.RetrofitBase;
import com.familheey.app.Networking.utils.GsonUtils;
import com.familheey.app.Post.PostDetailForPushActivity;
import com.familheey.app.Topic.MainActivity;
import com.familheey.app.Utilities.BackGroundDataFetching;
import com.familheey.app.Utilities.Constants;
import com.familheey.app.Utilities.SharedPref;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.luseen.autolinklibrary.AutoLinkMode;

import java.io.IOException;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import okhttp3.MediaType;
import okhttp3.RequestBody;

import static com.familheey.app.Utilities.Constants.Bundle.DATA;
import static com.familheey.app.Utilities.Constants.Bundle.DETAIL;
import static com.familheey.app.Utilities.Constants.Bundle.ID;
import static com.familheey.app.Utilities.Constants.Bundle.PUSH;
import static com.familheey.app.Utilities.Constants.Bundle.TYPE;

public class SplashScreen extends AppCompatActivity {
    @BindView(R.id.retry)
    MaterialButton retry;
    @BindView(R.id.exit)
    MaterialButton exit;
    @BindView(R.id.onBoardCheckProgress)
    ProgressBar progressBar;


    private UserSettings userSettings = null;
    private CompositeDisposable subscriptions;
    private AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        ButterKnife.bind(this);
        subscriptions = new CompositeDisposable();
        callIsRegistered();

    }

    private void callIsRegistered() {
        if (SharedPref.read(SharedPref.IS_REGISTERED, false)) {
            refreshToken();
        } else {
            new Handler().postDelayed(() -> {
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                finish();
            }, 1000);
        }
    }
/*
    //For testing 138
    private void setTestData() {
        if ((BuildConfig.DEBUG)) {
            UserRegistrationResponse response = new UserRegistrationResponse();
            response.setId("6943");
            response.setActive(true);
            response.setVerified(true);
            response.setFullName("liju");
            response.setOrigin("Kollam, Kerala, India");
            response.setLocation("Kollam, Kerala, India");
            SharedPref.setWalkThroughStatus(false);
            SharedPref.write(SharedPref.IS_REGISTERED, true);
            SharedPref.write(SharedPref.USER, GsonUtils.getInstance().getGson().toJson(response));
            SharedPref.setUserRegistration(response);
        }
    }*/


    private void refreshToken() {
        JsonObject jsonObject1 = new JsonObject();
        jsonObject1.addProperty("refresh_token", SharedPref.getUserRegistration().getRefreshToken());
        jsonObject1.addProperty("phone", SharedPref.getUserRegistration().getMobileNumber());
        RequestBody requestBody = RequestBody.create(jsonObject1.toString(), MediaType.parse("application/json; charset=utf-8"));
        FamilheeyApplication application = FamilheeyApplication.get(this);
        ApiServices apiServices = RetrofitBase.createRxResource(this, ApiServices.class);
        subscriptions.add(apiServices.delegation(requestBody)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(application.defaultSubscribeScheduler())
                .subscribe(response -> {
                    final String accessToken = response.getAsJsonPrimitive("accessToken").getAsString();
                    UserRegistrationResponse userRegistrationResponse = SharedPref.getUserRegistration();
                    userRegistrationResponse.setAccessToken(accessToken);
                    SharedPref.write(SharedPref.USER, GsonUtils.getInstance().getGson().toJson(userRegistrationResponse));
                    SharedPref.setUserRegistration(userRegistrationResponse);
                    getKeys();
                    checkOnBoard();
                    new BackGroundDataFetching(this).loadDataFromApi();
                }, throwable ->
                {
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                    finish();
                }));
    }


    private void getKeys() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("user_id", SharedPref.getUserRegistration().getId());
        RequestBody requestBody = RequestBody.create(jsonObject.toString(), MediaType.parse("application/json; charset=utf-8"));
        FamilheeyApplication application = FamilheeyApplication.get(getApplicationContext());
        ApiServices apiServices = RetrofitBase.createRxResource(getApplicationContext(), ApiServices.class);
        subscriptions.add(apiServices.getKeys(requestBody)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(application.defaultSubscribeScheduler())
                .subscribe(response -> {
                    assert response.body() != null;
                    SharedPref.write(SharedPref.GOOGLE_API, response.body().getGoogle_api_key());
                    SharedPref.write(SharedPref.STRIPE_KEY, response.body().getStripe());
                }, throwable ->
                        checkOnBoard()));
    }

    private void checkOnBoard() {
        retry.setVisibility(View.INVISIBLE);
        exit.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("user_id", SharedPref.getUserRegistration().getId());
        RequestBody requestBody = RequestBody.create(jsonObject.toString(), MediaType.parse("application/json; charset=utf-8"));
        FamilheeyApplication application = FamilheeyApplication.get(getApplicationContext());
        ApiServices apiServices = RetrofitBase.createRxResource(getApplicationContext(), ApiServices.class);
        subscriptions.add(apiServices.onBoardCheck(requestBody)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(application.defaultSubscribeScheduler())
                .subscribe(response -> {
                    assert response.body() != null;
                    userSettings = GsonUtils.getInstance().getGson().fromJson(response.body().string(), UserSettings.class);

                    if (userSettings.getUser_is_blocked()) {
                        DialogBlockUsers(userSettings.getId() + "");
                    } else if (!userSettings.getIsActive()) {

                        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                        finish();
                    } else {
                        saveToPreference(new Gson().toJson(userSettings));
                        if (userSettings.isForceUpdateRequired())
                            forceUpdateApp();
                        else goToNormalWorkFlow1(userSettings);
                    }
                }, throwable ->
                {
                    if (!(throwable instanceof IOException)) {
                        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                        finish();
                    } else {
                        retry.setVisibility(View.VISIBLE);
                        exit.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                    }
                }));
    }

    private void forceUpdateApp() {
        AlertDialog.Builder builder = new AlertDialog.Builder(SplashScreen.this);
        builder.setTitle("A New Update is Available");
        builder.setPositiveButton("Update", (dialog, which) -> {
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + "com.familheey.app")));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + "com.familheey.app")));
            }
            dialog.dismiss();
        });

        builder.setNegativeButton("Exit", (dialog, which) -> {
            dialog.dismiss();
            finish();
        });
        alertDialog = builder.show();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
    }

    private void saveToPreference(String response) {
        SharedPref.write(SharedPref.ON_BOARD, response);
    }

    public void goToNormalWorkFlow1(UserSettings userSettings) {

        try {
            if (!userSettings.isValidUser()) {
                UserRegistrationResponse userRegistrationResponse = new UserRegistrationResponse();
                userRegistrationResponse.setId(userSettings.getId().toString());
                Intent registrationIntent = new Intent(getApplicationContext(), UserRegistrationActivity.class);
                registrationIntent.putExtra(Constants.Bundle.DATA, userRegistrationResponse);
                startActivity(registrationIntent);
                finish();
                return;
            }
            boolean isGlobalSearchEnabled;
            isGlobalSearchEnabled = userSettings.getFamilyCount() <= 0;
            SharedPref.setUserHasFamily(!isGlobalSearchEnabled);

            FirebaseDynamicLinks.getInstance()
                    .getDynamicLink(getIntent())
                    .addOnSuccessListener(this, pendingDynamicLinkData -> {
                        if (pendingDynamicLinkData != null) {
                            Uri uri = pendingDynamicLinkData.getLink();
                            assert uri != null;
                            String type = (uri.getQueryParameter("type"));
                            if (type != null) {
                                if (type.contains("family")) {
                                    startActivity(new Intent(getApplicationContext(), FamilyDashboardActivity.class).putExtra(TYPE, DETAIL).putExtra(DATA, uri.getQueryParameter("type_id")));
                                    finish();
                                } else if (type.contains("event")) {
                                    startActivity(new Intent(getApplicationContext(), CreatedEventDetailActivity.class).putExtra(TYPE, PUSH).putExtra(ID, uri.getQueryParameter("type_id")));
                                    finish();
                                } else if (type.contains("post")) {
                                    startActivity(new Intent(getApplicationContext(), PostDetailForPushActivity.class).putExtra("ids", uri.getQueryParameter("type_id")).putExtra(TYPE, "PUSH"));
                                    finish();
                                } else if (type.contains("announcement")) {
                                    startActivity(new Intent(getApplicationContext(), AnnouncementDetailActivity.class).putExtra("id", uri.getQueryParameter("type_id")).putExtra(TYPE, "PUSH"));
                                    finish();
                                }

                            }
                        }
                    });

            navigateMainActivity(isGlobalSearchEnabled);
        } catch (JsonParseException e) {
            e.printStackTrace();
            retry.setVisibility(View.VISIBLE);
            exit.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
        }
    }


    private void navigateMainActivity(boolean isGlobalSearchEnabled) {

        Intent homeIntent = new Intent(SplashScreen.this, MainActivity.class);
        homeIntent.putExtra(Constants.Bundle.IS_GLOBAL_SEARCH_ENABLED, isGlobalSearchEnabled);
        homeIntent.putExtra(Constants.Bundle.IS_LOGGED_IN_NOW, true);
        startActivity(homeIntent);
        finish();
    }

    @OnClick({R.id.retry, R.id.exit})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.retry:
                checkOnBoard();
                break;
            case R.id.exit:
                finish();
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        NotificationManagerCompat.from(this).cancelAll();
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            notificationManager.cancelAll();
        }
        if (userSettings != null) {
            if (alertDialog != null && alertDialog.isShowing())
                return;
            checkOnBoard();
        }
    }

    private void DialogBlockUsers(String id) {
        progressBar.setVisibility(View.INVISIBLE);
        final Dialog dialog = new Dialog(this);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialogue_user_block);
        com.luseen.autolinklibrary.AutoLinkTextView textView = dialog.findViewById(R.id.txt_msg);
        textView.addAutoLinkMode(AutoLinkMode.MODE_EMAIL);
        textView.setEmailModeColor(ContextCompat.getColor(this, R.color.buttoncolor));
        dialog.setCanceledOnTouchOutside(false);
        textView.setAutoLinkText("Sorry, you account have been suspended. Please email us on contact@familheey.com to reactivate your account.");

        textView.setAutoLinkOnClickListener((autoLinkMode, matchedText) -> {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("*/*");
            intent.putExtra(Intent.EXTRA_EMAIL, new String[]{matchedText});
            intent.putExtra(Intent.EXTRA_SUBJECT, "Reactivate Account - " + id);
            intent.setType("text/html");
            intent.setPackage("com.google.android.gm");
            startActivity(Intent.createChooser(intent, "Send mail"));
            finish();
        });

        dialog.findViewById(R.id.btn_close).setOnClickListener(view -> dialog.dismiss());

        dialog.findViewById(R.id.btn_login).setOnClickListener(view -> {
            dialog.dismiss();

            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            finish();
        });
        dialog.show();
    }

    private static String getSystemProperty(String name) throws Exception {
        Class systemPropertyClazz = Class.forName("android.os.SystemProperties");
        return (String) systemPropertyClazz.getMethod("get", new Class[]{String.class
        }).invoke(systemPropertyClazz,
                new Object[]{name});
    }

    public static boolean checkEmulator() {
        try {
            boolean goldfish = getSystemProperty("ro.hardware").contains("goldfish");
            boolean qemu = getSystemProperty("ro.kernel.qemu").length() > 0;
            boolean sdk = getSystemProperty("ro.product.model").equals("sdk");
            return qemu || goldfish || sdk;
        } catch (Exception e) {
            return false;
        }
    }

}
