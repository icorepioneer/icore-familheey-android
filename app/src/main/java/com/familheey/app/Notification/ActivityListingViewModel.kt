package com.familheey.app.Notification

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.familheey.app.FamilheeyApplication
import com.familheey.app.Models.Response.UserSettings
import com.familheey.app.Networking.Retrofit.ApiServices
import com.familheey.app.Networking.Retrofit.RetrofitBase
import com.familheey.app.Networking.utils.GsonUtils
import com.familheey.app.Topic.RestApiService
import com.familheey.app.Topic.RetrofitUtil
import com.familheey.app.Utilities.Constants
import com.familheey.app.Utilities.SharedPref
import com.google.firebase.database.*
import com.google.gson.Gson
import com.google.gson.JsonObject
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody

class ActivityListingViewModel(application: Application) : AndroidViewModel(application) {
    private val context = getApplication<Application>().applicationContext
    private val compositeDisposable = CompositeDisposable()
    private val _authors = MutableLiveData<List<Activity>>()
    private var databaseReference: DatabaseReference? = null
    private val application = "application/json; charset=utf-8"

    var db = FirebaseDatabase.getInstance("https://familheey-255407.firebaseio.com/").getReference(Constants.ApiPaths.FIREBASE_USER_TYPE + SharedPref.getUserRegistration().id + "_notification")
    var count = MutableLiveData<Long>()

    val authors: LiveData<List<Activity>>
        get() = _authors

    private val _result = MutableLiveData<String?>()
    val result: LiveData<String?>
        get() = _result


    private val _clear = MutableLiveData<String?>()
    val clear: LiveData<String?>
        get() = _clear


    private val _read = MutableLiveData<String?>()
    val read: LiveData<String?>
        get() = _read


    private val _response = MutableLiveData<AdminAcceptResponse?>()
    val response: LiveData<AdminAcceptResponse?>
        get() = _response

    fun deleteAuthor(author: Activity?) {
        db.child(author?.key!!).removeValue()
                .addOnCompleteListener {

                }
    }


    fun clearNotification() {
        updateSettings()
        val jsonObject = JsonObject()
        jsonObject.addProperty("user_id", SharedPref.getUserRegistration().id)
        val requestBody: RequestBody = jsonObject.toString().toRequestBody(application.toMediaTypeOrNull())
        val application = FamilheeyApplication.get(context)
        val apiServices = RetrofitBase.createRxResource(context, ApiServices::class.java)
        compositeDisposable.add(apiServices.clearNotification(requestBody)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(application.defaultSubscribeScheduler())
                .subscribe({
                    _clear.value = "200"
                }) {
                    _clear.value = "200"
                })
    }

    fun markAsReadAllNotification() {
        updateSettings()
        val jsonObject = JsonObject()
        jsonObject.addProperty("user_id", SharedPref.getUserRegistration().id)
        val requestBody: RequestBody = jsonObject.toString().toRequestBody(application.toMediaTypeOrNull())
        val application = FamilheeyApplication.get(context)
        val apiServices = RetrofitBase.createRxResource(context, ApiServices::class.java)
        compositeDisposable.add(apiServices.readNotifications(requestBody)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(application.defaultSubscribeScheduler())
                .subscribe({
                    _read.value = "200"
                }) {
                    _read.value = "200"
                })
    }

    fun acceptOrReject(jsonObject: JsonObject) {
        val requestBody: RequestBody = jsonObject.toString().toRequestBody(application.toMediaTypeOrNull())
        val application = FamilheeyApplication.get(context)
        val apiServices = RetrofitBase.createRxResource(context, ApiServices::class.java)
        compositeDisposable.add(apiServices.respondToFamilyInvitationRx(requestBody)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(application.defaultSubscribeScheduler())
                .subscribe({

                    _result.value = "200"

                }) {
                    _result.value = it?.message!!

                })
    }


    fun acceptOrRejectAdmin(jsonObject: JsonObject) {
        val requestBody: RequestBody = jsonObject.toString().toRequestBody(application.toMediaTypeOrNull())
        val application = FamilheeyApplication.get(context)
        val apiServices = RetrofitBase.createRxResource(context, ApiServices::class.java)
        compositeDisposable.add(apiServices.memberRequestActionNew(requestBody)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(application.defaultSubscribeScheduler())
                .subscribe({
                    _response.value = it.body()
                }) {
                    _response.value = null

                })
    }

    fun goingOrInterstedOrNotgoing(eventId: String?, resp: String?) {
        val jsonObject = JsonObject()
        jsonObject.addProperty("user_id", SharedPref.getUserRegistration().id)
        jsonObject.addProperty("event_id", eventId)
        jsonObject.addProperty("resp", resp)
        val requestBody: RequestBody = jsonObject.toString().toRequestBody(application.toMediaTypeOrNull())
        val application = FamilheeyApplication.get(context)
        val apiServices = RetrofitBase.createRxResource(context, ApiServices::class.java)
        compositeDisposable.add(apiServices.respondToRSVP1(requestBody)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(application.defaultSubscribeScheduler())
                .subscribe({

                    _result.value = "200"

                }) {
                    _result.value = it?.message!!

                })
    }

    fun fetchDataFromPref(): Boolean {
        val response = SharedPref.read(SharedPref.ON_BOARD, "")

        if (response != "" && GsonUtils.getInstance().gson.fromJson(response, UserSettings::class.java).notification_auto_delete != null)
            return GsonUtils.getInstance().gson.fromJson(response, UserSettings::class.java).notification_auto_delete

        return false
    }

    private fun updateSettings() {
        val response = SharedPref.read(SharedPref.ON_BOARD, "")
        if (response != "") {
            val setting = GsonUtils.getInstance().gson.fromJson(response, UserSettings::class.java)
            setting.notification_auto_delete = false
            SharedPref.write(SharedPref.ON_BOARD, Gson().toJson(setting))
        }

    }


    fun notificationAutoDeleteStatusChange() {

        val jsonObject = JsonObject()
        jsonObject.addProperty("id", SharedPref.getUserRegistration().id)
        jsonObject.addProperty("notification_auto_delete", false)
        val requestBody: RequestBody = jsonObject.toString().toRequestBody(application.toMediaTypeOrNull())
        val requestInterface = RetrofitUtil.createRxResource(context, RestApiService::class.java)
        compositeDisposable.add(
                requestInterface.updateSettings(requestBody).observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({
                        }, {
                        })
        )
    }


    fun initializeFireBase() {
        databaseReference = FirebaseDatabase.getInstance("https://familheey-255407.firebaseio.com/").getReference(Constants.ApiPaths.FIREBASE_USER_TYPE + SharedPref.getUserRegistration().id + "_notification")
        databaseReference?.removeEventListener(notificationListener)
        databaseReference?.addValueEventListener(notificationListener)
    }

    //CHECKSTYLE:OFF

    private var notificationListener: ValueEventListener = object : ValueEventListener {
        override fun onDataChange(dataSnapshot: DataSnapshot) {
            val authors = mutableListOf<Activity>()
            for (authorSnapshot in dataSnapshot.children) {

                val obj = Activity()
                if (authorSnapshot.child("from_id").value != null) {
                    obj.fromId = authorSnapshot.child("from_id").value.toString()
                }
                if (authorSnapshot.child("group_id").value != null) {
                    obj.groupId = authorSnapshot.child("group_id").value.toString()
                }
                if (authorSnapshot.child("category").value != null) {
                    obj.category = authorSnapshot.child("category").value.toString()
                }
                if (authorSnapshot.child("create_time").value != null) {
                    obj.createTime = authorSnapshot.child("create_time").value.toString()
                }
                if (authorSnapshot.child("rsvp").value != null) {
                    obj.rsvp = authorSnapshot.child("rsvp").getValue(Boolean::class.java)
                }
                if (authorSnapshot.child("from_date").value != null) {
                    obj.fromDate = authorSnapshot.child("from_date").getValue(Long::class.java)
                }
                if (authorSnapshot.child("type_id").value != null) {
                    obj.typeId = authorSnapshot.child("type_id").value.toString()
                }
                if (authorSnapshot.child("event_id").value != null) {
                    obj.eventId = authorSnapshot.child("event_id").value.toString()
                }

                if (authorSnapshot.child("visible_status").value != null) {
                    obj.visibleStatus = authorSnapshot.child("visible_status").value.toString()
                }
                if (authorSnapshot.child("type").value != null) {
                    obj.type = authorSnapshot.child("type").value.toString()
                }
                if (authorSnapshot.child("message").value != null) {
                    obj.message = authorSnapshot.child("message").value.toString()
                }
                if (authorSnapshot.child("privacy_type").value != null) {
                    obj.privacyType = authorSnapshot.child("privacy_type").value.toString()
                }
                if (authorSnapshot.child("message_title").value != null) {
                    obj.messageTitle = authorSnapshot.child("message_title").value.toString()
                }
                if (authorSnapshot.child("propic").value != null) {
                    obj.propic = authorSnapshot.child("propic").value.toString()
                }
                if (authorSnapshot.child("sub_type").value != null) {
                    obj.subType = authorSnapshot.child("sub_type").value.toString()
                }
                if (authorSnapshot.child("link_to").value != null) {
                    obj.linkTo = authorSnapshot.child("link_to").value.toString()
                }
                if (authorSnapshot.child("cover_image").value != null) {
                    obj.coverImage = authorSnapshot.child("cover_image").value.toString()
                }
                if (authorSnapshot.child("to_group_name").value != null) {
                    obj.toGroupName = authorSnapshot.child("to_group_name").value.toString()
                }
                if (authorSnapshot.child("description").value != null) {
                    obj.description = authorSnapshot.child("description").value.toString()
                }
                if (authorSnapshot.child("created_by_user").value != null) {
                    obj.createdByUser = authorSnapshot.child("created_by_user").value.toString()
                }
                if (authorSnapshot.child("created_by_propic").value != null) {
                    obj.createdByPropic = authorSnapshot.child("created_by_propic").value.toString()
                }
                if (authorSnapshot.child("location").value != null) {
                    obj.location = authorSnapshot.child("location").value.toString()
                }
                if (authorSnapshot.child("membercount").value != null) {
                    obj.membercount = authorSnapshot.child("membercount").value.toString()
                }
                obj.key = authorSnapshot.key
                obj.let {
                    if (!obj.type.equals(""))
                        authors.add(obj)
                }
            }

            _authors.value = authors
        }

        override fun onCancelled(databaseError: DatabaseError) {
            _authors.value = mutableListOf<Activity>()
        }
    }

    //CHECKSTYLE:ON

}