package com.familheey.app.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.familheey.app.Adapters.LinkFamilyAdapter;
import com.familheey.app.Decorators.VerticalSpaceItemDecoration;
import com.familheey.app.Interfaces.ProgressListener;
import com.familheey.app.Interfaces.RetrofitListener;
import com.familheey.app.Models.ApiCallbackParams;
import com.familheey.app.Models.ErrorData;
import com.familheey.app.Models.Response.Family;
import com.familheey.app.Networking.Retrofit.ApiServiceProvider;
import com.familheey.app.Parsers.FamilyParser;
import com.familheey.app.R;
import com.familheey.app.Utilities.SharedPref;
import com.familheey.app.Utilities.Utilities;
import com.google.android.material.button.MaterialButton;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.familheey.app.Utilities.Constants.ApiFlags.FETCH_FAMILY_FOR_LINKING;
import static com.familheey.app.Utilities.Constants.ApiFlags.REQUEST_LINK_FAMILY;
import static com.familheey.app.Utilities.Constants.Bundle.DATA;
import static com.familheey.app.Utilities.Constants.Bundle.IDENTIFIER;

public class LinkFamilyActivity extends AppCompatActivity implements RetrofitListener, ProgressListener {

    @BindView(R.id.goBack)
    ImageView goBack;
    @BindView(R.id.toolBarTitle)
    TextView toolBarTitle;
    @BindView(R.id.emptyIndicatorContainer)
    View emptyIndicatorContainer;
    @BindView(R.id.linkedFamilyList)
    RecyclerView linkedFamilyList;
    @BindView(R.id.linkFamilies)
    MaterialButton linkFamilies;
    @BindView(R.id.createFamilyWithLinking)
    TextView createFamilyWithLinking;

    private Family family;
    private LinkFamilyAdapter linkFamilyAdapter;
    private SweetAlertDialog progressDialog;
    private ArrayList<Family> familiesToBeLinked = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_link_family);
        ButterKnife.bind(this);
        family = getIntent().getParcelableExtra(DATA);
        if (getIntent().hasExtra(IDENTIFIER))
            createFamilyWithLinking.setVisibility(View.INVISIBLE);
        initializeToolbar();
        initializeAdapter();
        fetchFamiliesForlinking();
    }

    private void initializeAdapter() {
        linkFamilyAdapter = new LinkFamilyAdapter(familiesToBeLinked);
        linkedFamilyList.setAdapter(linkFamilyAdapter);
        linkedFamilyList.setLayoutManager(new LinearLayoutManager(this));
        linkedFamilyList.addItemDecoration(new VerticalSpaceItemDecoration(16));
    }

    private void initializeToolbar() {
        toolBarTitle.setText("Link Families");
        goBack.setOnClickListener(v -> finish());
    }

    private void fetchFamiliesForlinking() {
        showProgressDialog();
        JsonObject fetchFamiliesRequestJson = new JsonObject();
        fetchFamiliesRequestJson.addProperty("user_id", SharedPref.getUserRegistration().getId());
        fetchFamiliesRequestJson.addProperty("group_id", family.getId());
        ApiServiceProvider apiServiceProvider = ApiServiceProvider.getInstance(this);
        apiServiceProvider.fetchFamiliesForLinking(fetchFamiliesRequestJson, null, this);
    }

    private void requestFamilyLinking() {
        showProgressDialog();
        ApiServiceProvider apiServiceProvider = ApiServiceProvider.getInstance(getApplicationContext());
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("from_group", family.getId());
        jsonObject.add("to_group", linkFamilyAdapter.getSelectedFamiliesForLinking());
        jsonObject.addProperty("requested_by", SharedPref.getUserRegistration().getId());
        apiServiceProvider.requestLinkFamily(jsonObject, null, this);
    }

    @OnClick({R.id.goBack, R.id.linkFamilies, R.id.exit, R.id.createFamilyWithLinking})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.exit:
            case R.id.goBack:
                finish();
                break;
            case R.id.linkFamilies:
                if (linkFamilyAdapter.getSelectedFamiliesForLinking().size() > 0)
                    requestFamilyLinking();
                else
                    Toast.makeText(this, "Please select families for linking!", Toast.LENGTH_SHORT).show();
                break;
            case R.id.createFamilyWithLinking:
                Intent createFamilyIntent = new Intent(getApplicationContext(), CreateFamilyActivity.class);
                createFamilyIntent.putExtra(DATA, family.getId().toString());
                startActivity(createFamilyIntent);
                break;
        }
    }

    @Override
    public void onResponseSuccess(String responseBodyString, ApiCallbackParams apiCallbackParams, int apiFlag) {
        hideProgressDialog();
        switch (apiFlag) {
            case FETCH_FAMILY_FOR_LINKING:
                familiesToBeLinked.addAll(FamilyParser.parseLinkedFamilies(responseBodyString));
                if (familiesToBeLinked.size() == 0) {
                    emptyIndicatorContainer.setVisibility(View.VISIBLE);
                    linkedFamilyList.setVisibility(View.INVISIBLE);
                    linkFamilies.setVisibility(View.INVISIBLE);
                } else {
                    emptyIndicatorContainer.setVisibility(View.INVISIBLE);
                    linkedFamilyList.setVisibility(View.VISIBLE);
                    linkFamilies.setVisibility(View.VISIBLE);
                }
                linkFamilyAdapter.notifyDataSetChanged();
                break;
            case REQUEST_LINK_FAMILY:
                finish();
                break;
        }
    }

    @Override
    public void onResponseError(ErrorData errorData, ApiCallbackParams apiCallbackParams, Throwable throwable, int apiFlag) {
        showErrorDialog(errorData.getMessage());
    }

    @Override
    public void showProgressDialog() {
        progressDialog = Utilities.getProgressDialog(this);
        progressDialog.show();
    }

    @Override
    public void hideProgressDialog() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }

    @Override
    public void showErrorDialog(String errorMessage) {
        if (progressDialog == null) {
            progressDialog = Utilities.getErrorDialog(this, errorMessage);
            progressDialog.show();
            return;
        }
        Utilities.getErrorDialog(progressDialog, errorMessage);
    }
}
