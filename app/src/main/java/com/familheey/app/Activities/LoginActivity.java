package com.familheey.app.Activities;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.familheey.app.Dialogs.CountryCodeDialogFragment;
import com.familheey.app.Interfaces.CountrySelectedListener;
import com.familheey.app.Interfaces.RetrofitListener;
import com.familheey.app.Models.ApiCallbackParams;
import com.familheey.app.Models.Country;
import com.familheey.app.Models.ErrorData;
import com.familheey.app.Models.Request.LoginRequest;
import com.familheey.app.Models.Response.UserRegistrationResponse;
import com.familheey.app.Networking.Retrofit.ApiServiceProvider;
import com.familheey.app.R;
import com.familheey.app.Utilities.Constants;
import com.familheey.app.Utilities.CountryCodeUtilities;
import com.familheey.app.Utilities.Utilities;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.CredentialPickerConfig;
import com.google.android.gms.auth.api.credentials.HintRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.material.button.MaterialButton;
import com.google.gson.Gson;
import com.luseen.autolinklibrary.AutoLinkMode;
import com.luseen.autolinklibrary.AutoLinkTextView;

import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import io.michaelrocks.libphonenumber.android.NumberParseException;
import io.michaelrocks.libphonenumber.android.PhoneNumberUtil;
import io.michaelrocks.libphonenumber.android.Phonenumber;

import static com.familheey.app.Utilities.Constants.ApiPaths.PRIVACY_URL;

public class LoginActivity extends AppCompatActivity implements RetrofitListener, CountrySelectedListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    @BindView(R.id.create_event)
    MaterialButton continu;
    @BindView(R.id.countrySelector)
    EditText countrySelector;

    @BindView(R.id.arrowDown)
    ImageView arrowDown;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.country_code)
    TextView countryCode;
    @BindView(R.id.termsAndConditions)
    AutoLinkTextView termsAndConditions;
    String lastChar = " ";
    String phoneString = "";

    private GoogleApiClient mCredentialsApiClient;
    TextWatcher telephoneTextChangeWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            int digits = phone.getText().toString().length();
            if (digits > 1)
                lastChar = phone.getText().toString().substring(digits - 1);
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            int digits = phone.getText().toString().length();
            if (!lastChar.equals("-")) {
                if (digits == 3 || digits == 7) {
                    phone.append("-");
                }
            }

            if (digits > 7) {
                continu.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.buttoncolor)));
                continu.setEnabled(true);
                continu.setBackgroundTintList(LoginActivity.this.getResources().getColorStateList(R.color.buttoncolor));
            } else {
                continu.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.quantum_grey300)));
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };
    private SweetAlertDialog progressDialog;
    private ArrayList<Country> countries = new ArrayList<>();
    // private TelephonyManager telephonyManager = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
//        setStatusBarColor();
        initialize();
        requestHint();
    }

    private void initialize() {
        Locale locale = Locale.getDefault();
        Utilities.attachEmptyListener(countrySelector);
        Utilities.makeEditTextClickOnly(countrySelector);
        countries = CountryCodeUtilities.getCountryCodes(getApplicationContext());
        for (int i = 0; i < countries.size(); i++) {
            Country c = countries.get(i);
            if (locale.getCountry().equalsIgnoreCase(c.getCountryCode())) {
                countryCode.setText(c.getDialCode());
                countrySelector.setText(c.getCountryName());
            }
        }
        phone.addTextChangedListener(telephoneTextChangeWatcher);
        continu.setOnClickListener(view -> {
            phoneString = phone.getText().toString().trim();
            phoneString = phoneString.replaceAll("-", "");
            if (validate())
                registerUser();
        });

        arrowDown.setOnClickListener(v -> countrySelector.callOnClick());

        countrySelector.setOnClickListener(v -> CountryCodeDialogFragment.newInstance(countries).show(getSupportFragmentManager(), "CountryCodeDialogFragment"));
        termsAndConditions.setCustomModeColor(ContextCompat.getColor(getApplicationContext(), R.color.buttoncolor));
        termsAndConditions.addAutoLinkMode(AutoLinkMode.MODE_CUSTOM);
        termsAndConditions.setCustomRegex("\\bterms and conditions\\b | \\bprivacy policy\\b");
        termsAndConditions.setAutoLinkText(getString(R.string.terms));
        termsAndConditions.setAutoLinkOnClickListener((autoLinkMode, matchedText) -> {
            Intent httpIntent = new Intent(Intent.ACTION_VIEW);
            if (matchedText.trim().equalsIgnoreCase("terms and conditions")) {
                httpIntent.setData(Uri.parse(PRIVACY_URL + "termsofservice"));
            } else {
                httpIntent.setData(Uri.parse(PRIVACY_URL + "policy"));

            }
            startActivity(httpIntent);
        });
    }

    private boolean validate() {
        boolean isValid = true;
        if (countryCode.getText().toString().length() == 0) {
            isValid = false;
            Toast.makeText(this, "Please select a country", Toast.LENGTH_SHORT).show();
        }
        if (phoneString.length() < 7) {
            isValid = false;
            phone.setError("Required");
        }
        return isValid;
    }

//    private void setStatusBarColor() {
//        Window window = getWindow();
//        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            window.setStatusBarColor(ContextCompat.getColor(this, R.color.white));
//        }
//    }

    private LoginRequest generateLoginRequest() {
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setData();
        loginRequest.setCountryCode(countryCode.getText().toString());
        loginRequest.setMobileNumber(phoneString.trim());
        return loginRequest;
    }

    //
    private void registerUser() {
        progressDialog = Utilities.getProgressDialog(this);
        progressDialog.show();
        ApiServiceProvider apiServiceProvider = ApiServiceProvider.getInstance(getApplicationContext());
        apiServiceProvider.registerUser(generateLoginRequest(), null, this);
    }

    @Override
    public void onResponseSuccess(String responseBodyString, ApiCallbackParams apiCallbackParams, int apiFlag) {
        progressDialog.dismiss();
        Gson gson = new Gson();
        UserRegistrationResponse userRegistrationResponse = gson.fromJson(responseBodyString, UserRegistrationResponse.class);
        proceedToOTPVerification(userRegistrationResponse);
    }

    @Override
    public void onResponseError(ErrorData errorData, ApiCallbackParams apiCallbackParams, Throwable throwable, int apiFlag) {
        if (errorData.getCode() == Constants.ErrorCodes.INTERNAL_ERROR && errorData.getMessage().equalsIgnoreCase(Constants.ErrorNames.USER_ALREADY_EXISTS)) {
            progressDialog.dismiss();
            proceedToOTPVerification(null);
        } else {
            Utilities.getErrorDialog(progressDialog, Constants.SOMETHING_WRONG);
            //proceedToOTPVerification(null);
        }
    }

    private void proceedToOTPVerification(UserRegistrationResponse userRegistrationResponse) {
        if (userRegistrationResponse == null)
            userRegistrationResponse = new UserRegistrationResponse();
        Intent intent = new Intent(getApplicationContext(), OTPVerificationActivity.class);
        userRegistrationResponse.setCapturedCountryCode(countryCode.getText().toString());
        userRegistrationResponse.setCapturedMobileNumber(phoneString);
        intent.putExtra(Constants.Bundle.DATA, userRegistrationResponse);
        startActivity(intent);
    }

    @Override
    public void OnCountrySelected(Country selectedCountry) {
        countryCode.setText(selectedCountry.getDialCode());
        countrySelector.setText(selectedCountry.getCountryName());
        if(!phone.getText().toString().trim().isEmpty()){
            String wc = phone.getText().toString().replace(countryCode.getText().toString().trim(), "");
            phone.setText(wc);
        }
    }


    private void requestHint() {
        try {
            mCredentialsApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .enableAutoManage(this, this)
                    .addApi(Auth.CREDENTIALS_API)
                    .build();
            HintRequest hintRequest = new HintRequest.Builder()
                    .setHintPickerConfig(new CredentialPickerConfig.Builder().setShowCancelButton(true).build())
                    .setPhoneNumberIdentifierSupported(true)
                    .build();

            PendingIntent intent = Auth.CredentialsApi.getHintPickerIntent(
                    mCredentialsApiClient, hintRequest);
            startIntentSenderForResult(intent.getIntentSender(),
                    599, null, 0, 0, 0);
        } catch (Exception e) {

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 599) {
            if (resultCode == RESULT_OK) {
                Credential credential = data.getParcelableExtra(Credential.EXTRA_KEY);
                assert credential != null;
                PhoneNumberUtil phoneUtil = PhoneNumberUtil.createInstance(this);
                try {
                    Phonenumber.PhoneNumber numberProto = phoneUtil.parse(credential.getId(), "");
                    phone.setText(numberProto.getNationalNumber() + "");

                    for (int i = 0; i < countries.size(); i++) {
                        Country c = countries.get(i);
                        String code = "+" + numberProto.getCountryCode();
                        if (code.equals(c.getDialCode())) {
                            countryCode.setText(c.getDialCode());
                            countrySelector.setText(c.getCountryName());
                        }
                    }


                } catch (NumberParseException e) {

                    String wc = credential.getId().replace(countryCode.getText().toString().trim(), "");
                    phone.setText(wc);
                }

                if (!countryCode.getText().equals(""))
                    continu.performClick();
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        /*
        Need to handle

         */
    }

    @Override
    public void onConnectionSuspended(int cause) {
        /*
        Need to handle

         */
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        /*
        Need to handle

         */
    }

}
