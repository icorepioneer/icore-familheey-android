package com.familheey.app.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.familheey.app.Adapters.FamilyAddMembersPagerAdapter;
import com.familheey.app.Interfaces.ProgressListener;
import com.familheey.app.Models.Response.Family;
import com.familheey.app.R;
import com.familheey.app.Utilities.Utilities;
import com.google.android.material.tabs.TabLayout;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.familheey.app.Utilities.Constants.Bundle.DATA;

public class FamilyAddMemberActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener, ProgressListener {

    public static final int REQUEST_CODE = 1;
    @BindView(R.id.addMembersViewPager)
    androidx.viewpager.widget.ViewPager addMembersViewPager;
    @BindView(R.id.addMembersTab)
    com.google.android.material.tabs.TabLayout addMembersTab;

    private Family family;
    private SweetAlertDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_family_add_member);
        ButterKnife.bind(this);
        family = getIntent().getParcelableExtra(DATA);
        Objects.requireNonNull(getIntent().getExtras()).clear();
        initializeTabs();
        initializeAdapter();
    }

    private void initializeTabs() {
        addMembersTab.addTab(addMembersTab.newTab().setText("PEOPLE"));
        addMembersTab.addTab(addMembersTab.newTab().setText("OTHER CONTACTS"));
    }

    private void initializeAdapter() {
        FamilyAddMembersPagerAdapter familyAddMembersPagerAdapter = new FamilyAddMembersPagerAdapter(family, getSupportFragmentManager(), addMembersTab.getTabCount());
        addMembersViewPager.setAdapter(familyAddMembersPagerAdapter);
        addMembersViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(addMembersTab));
        addMembersTab.addOnTabSelectedListener(this);
        addMembersViewPager.setOffscreenPageLimit(addMembersTab.getTabCount());
    }


    @OnClick({R.id.btn_back})
    public void onClick(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        addMembersViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void showProgressDialog() {
        progressDialog = Utilities.getProgressDialog(this);
        progressDialog.show();
    }

    @Override
    public void hideProgressDialog() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }

    @Override
    public void showErrorDialog(String errorMessage) {
        if (progressDialog == null) {
            progressDialog = Utilities.getErrorDialog(this, errorMessage);
            progressDialog.show();
            return;
        }
        Utilities.getErrorDialog(progressDialog, errorMessage);
    }
}
