package com.familheey.app.Activities;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.familheey.app.Adapters.SubFoldersAdapter;
import com.familheey.app.Dialogs.DocumentNameEditorDialog;
import com.familheey.app.Interfaces.RetrofitListener;
import com.familheey.app.Models.ApiCallbackParams;
import com.familheey.app.Models.ErrorData;
import com.familheey.app.Models.Response.ViewContents;
import com.familheey.app.Networking.Retrofit.ApiServiceProvider;
import com.familheey.app.R;
import com.familheey.app.Utilities.Constants;
import com.familheey.app.Utilities.FileUtils;
import com.familheey.app.Utilities.SharedPref;
import com.google.android.material.button.MaterialButton;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import droidninja.filepicker.FilePickerConst;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static com.familheey.app.Activities.CreateAlbumDetailedActivity.ALBUM_EDIT_REQUEST_CODE;
import static com.familheey.app.Utilities.Constants.Bundle.CAN_UPDATE;
import static com.familheey.app.Utilities.Constants.Bundle.DATA;
import static com.familheey.app.Utilities.Constants.Bundle.DESCRIPTION;
import static com.familheey.app.Utilities.Constants.Bundle.ID;
import static com.familheey.app.Utilities.Constants.Bundle.IS_ADMIN;
import static com.familheey.app.Utilities.Constants.Bundle.IS_ALBUM;
import static com.familheey.app.Utilities.Constants.Bundle.IS_FAMILY_SETTINGS_NEEDED;
import static com.familheey.app.Utilities.Constants.Bundle.PERMISSION;
import static com.familheey.app.Utilities.Constants.Bundle.PERMISSION_GRANTED_USERS;
import static com.familheey.app.Utilities.Constants.Bundle.TITLE;
import static com.familheey.app.Utilities.Constants.Bundle.TYPE;
import static com.familheey.app.Utilities.Constants.FileUpload.TYPE_FAMILY;
import static com.familheey.app.Utilities.Utilities.getMimeType;

public class SubFolderActivity extends AppCompatActivity implements SubFoldersAdapter.OnDocumentSelectedListener, DocumentNameEditorDialog.OnDocumentNameChanged {

    int RESULT_DOC = 12;

    @BindView(R.id.recycler_folder)
    RecyclerView recycler_folder;

    @BindView(R.id.floatingAddFolder)
    CardView floatingAddFolder;

    @BindView(R.id.progressSubFolder)
    ProgressBar progressBar;

    @BindView(R.id.goBack)
    ImageView goBack;
    @BindView(R.id.album_tittle)
    TextView toolBarTitle;
    @BindView(R.id.imageEdit)
    ImageView imageEdit;

    @BindView(R.id.deleteAlbumElements)
    MaterialButton deleteAlbumElements;
    @BindView(R.id.album_description)
    TextView documentDescription;

    @BindView(R.id.txt_less_or_more)
    TextView txt_less_or_more;
    @BindView(R.id.txt_temp)
    TextView textTemp;
    @BindView(R.id.empty_tv)
    TextView emptyDocumentsIndicator;
    @BindView(R.id.addFiles)
    MaterialButton addFiles;

    private SubFoldersAdapter adapter;
    private final List<Long> selectedElementIds = new ArrayList<>();
    private final List<ViewContents.Document> documents = new ArrayList<>();
    private String id;
    private String folderId;
    private int type;
    private boolean isAdmin = false;
    private String title = "";
    private String description = "";
    private ViewContents viewContents;

    private ArrayList<Integer> permissionGrantedUsers = new ArrayList<>();
    private String permission = "";
    private boolean canUpdate = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_folder);
        ButterKnife.bind(this);
        parseIntent();
        initializeToolbar();
        initializeAdapter();
        fetchData();
    }

    private void initializeAdapter() {
        adapter = new SubFoldersAdapter(documents, this, isAdmin || canUpdate);
        recycler_folder.setHasFixedSize(true);
        recycler_folder.setLayoutManager(new GridLayoutManager(SubFolderActivity.this, 3));
        recycler_folder.setAdapter(adapter);
    }

    private void initializeToolbar() {
        toolBarTitle.setText(title);
        goBack.setOnClickListener(v -> finish());
    }

    private void parseIntent() {
        id = getIntent().getStringExtra(Constants.Bundle.ID);
        folderId = getIntent().getStringExtra(Constants.Bundle.FOLDER_ID);
        type = getIntent().getIntExtra(Constants.Bundle.TYPE, 0);
        isAdmin = getIntent().getBooleanExtra(Constants.Bundle.IS_ADMIN, false);
        title = getIntent().getStringExtra(Constants.Bundle.TITLE);
        description = getIntent().getStringExtra(Constants.Bundle.DESCRIPTION);
        isAdmin = getIntent().getBooleanExtra(IS_ADMIN, false);
        canUpdate = getIntent().getBooleanExtra(CAN_UPDATE, false);
        if (description != null)
            documentDescription.setText(description);
        else {
            description = "";
            documentDescription.setText("");
        }
        addViewMoreLogic(description);
        if (isAdmin || canUpdate) {
            imageEdit.setVisibility(View.VISIBLE);
            addFiles.setVisibility(View.VISIBLE);
        } else {
            imageEdit.setVisibility(View.INVISIBLE);
            addFiles.setVisibility(View.INVISIBLE);
        }
    }

    void showProgressBar() {
        if (progressBar != null) {
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    void hideProgressBar() {
        if (progressBar != null) {
            progressBar.setVisibility(View.GONE);
        }
    }

    private boolean isReadStoragePermissionGranted() {
        if (TedPermission.isGranted(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE))
            return true;
        else {
            requestPermission();
            return false;
        }
    }

    private void requestPermission() {
        TedPermission.with(this)
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        FileUtils.pickDocument(SubFolderActivity.this, RESULT_DOC);
                    }

                    @Override
                    public void onPermissionDenied(List<String> deniedPermissions) {

                    }
                })
                .setDeniedMessage("If you reject permission,you can not upload Documents\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
                .check();
    }


    private void fetchData() {
        showProgressBar();
        ApiServiceProvider apiServiceProvider = ApiServiceProvider.getInstance(SubFolderActivity.this);
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("folder_id", folderId);
        switch (type) {
            case Constants.FileUpload.TYPE_EVENTS:
                jsonObject.addProperty("event_id", id);
                break;
            case Constants.FileUpload.TYPE_FAMILY:
                jsonObject.addProperty("group_id", id);
                break;
            case Constants.FileUpload.TYPE_USER:

                break;
        }
        jsonObject.addProperty("user_id", SharedPref.getUserRegistration().getId());
        apiServiceProvider.viewContents(jsonObject, null, new RetrofitListener() {
            @Override
            public void onResponseSuccess(String responseBodyString, ApiCallbackParams apiCallbackParams, int apiFlag) {
                viewContents = new Gson().fromJson(responseBodyString, ViewContents.class);
                title = viewContents.getFolderDetails().getFolderName();
                description = viewContents.getFolderDetails().getDescription();
                toolBarTitle.setText(title);
                documentDescription.setText(description);
                permissionGrantedUsers = (ArrayList<Integer>) viewContents.getFolderDetails().getPermissionUsers();
                permission = viewContents.getFolderDetails().getPermissions();
                if (permission == null)
                    permission = "all";
                if (permissionGrantedUsers == null)
                    permissionGrantedUsers = new ArrayList<>();
                documents.clear();
                documents.addAll(viewContents.getDocuments());
                hideProgressBar();
                if (documents.size() == 0)
                    emptyDocumentsIndicator.setVisibility(View.VISIBLE);
                else emptyDocumentsIndicator.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onResponseError(ErrorData errorData, ApiCallbackParams apiCallbackParams, Throwable throwable, int apiFlag) {
                hideProgressBar();
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_DOC) {
            if (data == null)
                return;
            List<Uri> fileUris = data.getParcelableArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS);

            MultipartBody.Builder builder = getPostMultipartBuilder(fileUris.get(0));
            showProgressBar();
            ApiServiceProvider apiServiceProvider = ApiServiceProvider.getInstance(getApplicationContext());
            apiServiceProvider.uploadFile(builder, null, new RetrofitListener() {
                @Override
                public void onResponseSuccess(String responseBodyString, ApiCallbackParams apiCallbackParams, int apiFlag) {
                    hideProgressBar();
                    fetchData();
                }

                @Override
                public void onResponseError(ErrorData errorData, ApiCallbackParams apiCallbackParams, Throwable throwable, int apiFlag) {
                    hideProgressBar();
                }
            });
        } else if (requestCode == ALBUM_EDIT_REQUEST_CODE && resultCode == RESULT_OK) {
            assert data != null;
            toolBarTitle.setText(data.getStringExtra(TITLE));
            description = data.getStringExtra(DESCRIPTION);
            documentDescription.setText(description);
            addViewMoreLogic(description);
            viewContents.getFolderDetails().setPermissions(data.getStringExtra(PERMISSION));
            viewContents.getFolderDetails().setPermissionUsers(data.getIntegerArrayListExtra(PERMISSION_GRANTED_USERS));
            viewContents.getFolderDetails().setFolderName(data.getStringExtra(TITLE));
            viewContents.getFolderDetails().setDescription(data.getStringExtra(DESCRIPTION));
            permissionGrantedUsers = data.getIntegerArrayListExtra(PERMISSION_GRANTED_USERS);
            permission = data.getStringExtra(PERMISSION);
            title = data.getStringExtra(TITLE);
            description = data.getStringExtra(DESCRIPTION);
            fetchData();
        }
    }

    public MultipartBody.Builder getPostMultipartBuilder(Uri fileUri) {
        MultipartBody.Builder multiPartBodyBuilder = new MultipartBody.Builder().setType(Objects.requireNonNull(MediaType.parse("multipart/form-data")));
        multiPartBodyBuilder.addFormDataPart("permissions", "all");
        multiPartBodyBuilder.addFormDataPart("folder_id", folderId);
        multiPartBodyBuilder.addFormDataPart("user_id", SharedPref.getUserRegistration().getId());
        switch (type) {
            case Constants.FileUpload.TYPE_EVENTS:
                multiPartBodyBuilder.addFormDataPart("event_id", id);
                break;
            case Constants.FileUpload.TYPE_FAMILY:
                multiPartBodyBuilder.addFormDataPart("group_id", id);
                break;
            case Constants.FileUpload.TYPE_USER:

                break;
        }
        multiPartBodyBuilder.addFormDataPart("is_sharable", String.valueOf(true));
        String filePath = FileUtils.getPath(SubFolderActivity.this, fileUri);
        File file = new File(filePath);
        multiPartBodyBuilder.addFormDataPart("file_name", file.getName());
        String MIME_TYPE = getMimeType(getApplicationContext(), fileUri);
        MediaType mediaType = MediaType.parse(MIME_TYPE);
        RequestBody requestBody = RequestBody.create(file, mediaType);
        multiPartBodyBuilder.addFormDataPart("Documents", file.getName(), requestBody);
        return multiPartBodyBuilder;
    }

    @OnClick({R.id.floatingAddFolder, R.id.deleteAlbumElements, R.id.addFiles, R.id.imageEdit})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.floatingAddFolder:
            case R.id.addFiles:
                if (isAdmin || canUpdate) {
                    if (isReadStoragePermissionGranted())

                        FileUtils.pickDocument(this, RESULT_DOC);
                } else {
                    Toast.makeText(getApplicationContext(), "You don't have authorization to create documents", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.deleteAlbumElements:
                deleteAlbumElements();
                break;
            case R.id.imageEdit:
                Intent intent = new Intent(this, CreateAlbumBasicActivity.class);
                intent.putExtra(TYPE, type);
                intent.putExtra(TITLE, title);
                intent.putExtra(DESCRIPTION, description);
                intent.putExtra(ID, folderId);
                intent.putExtra(IS_ALBUM, false);
                if (permission != null)
                    intent.putExtra(PERMISSION, viewContents.getFolderDetails().getPermissions());
                else intent.putExtra(PERMISSION, "all");
                intent.putExtra(DATA, id);
                if (type == TYPE_FAMILY) {
                    if (isAdmin && !canUpdate)
                        intent.putExtra(IS_FAMILY_SETTINGS_NEEDED, false);
                }
                if (permissionGrantedUsers != null)
                    intent.putIntegerArrayListExtra(PERMISSION_GRANTED_USERS, (ArrayList<Integer>) viewContents.getFolderDetails().getPermissionUsers());
                else intent.putExtra(PERMISSION_GRANTED_USERS, new ArrayList<>());
                startActivityForResult(intent, ALBUM_EDIT_REQUEST_CODE);
                break;
        }
    }

    @Override
    public void onDocumentElementSelected(ViewContents.Document document, int position) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(document.getUrl()));
        startActivity(browserIntent);
    }

    @Override
    public void onDocumentSelectedForDeletion(List<Long> selectedElementIds) {
        this.selectedElementIds.addAll(selectedElementIds);
        deleteAlbumElements.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDocumentDeselected() {
        deleteAlbumElements.setVisibility(View.GONE);
        selectedElementIds.clear();
    }

    @Override
    public void onDocumentSelectedForEditing(ViewContents.Document document, int position) {
        DocumentNameEditorDialog.newInstance(document).show(getSupportFragmentManager(), "FileNameChanger");
    }

    private void deleteAlbumElements() {
        progressBar.setVisibility(View.VISIBLE);
        ApiServiceProvider apiServiceProvider = ApiServiceProvider.getInstance(this);
        JsonObject jsonObject = new JsonObject();
        JsonArray jsonArray = new JsonArray();

        List<Long> deDupStringList = new ArrayList<>(new HashSet<>(selectedElementIds));

        for (Long documentId : deDupStringList) {
            jsonArray.add(documentId);
        }
        jsonObject.add("id", jsonArray);
        apiServiceProvider.deleteFile(jsonObject, null, new RetrofitListener() {
            @Override
            public void onResponseSuccess(String responseBodyString, ApiCallbackParams apiCallbackParams, int apiFlag) {
                progressBar.setVisibility(View.INVISIBLE);
                for (Iterator<ViewContents.Document> it = documents.iterator(); it.hasNext(); ) {
                    if (selectedElementIds.contains(it.next().getId())) {
                        it.remove();
                    }
                }
                adapter.notifyDataSetChanged();
                deleteAlbumElements.setVisibility(View.GONE);
                if (documents.size() == 0)
                    emptyDocumentsIndicator.setVisibility(View.VISIBLE);
                else emptyDocumentsIndicator.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onResponseError(ErrorData errorData, ApiCallbackParams apiCallbackParams, Throwable throwable, int apiFlag) {
                progressBar.setVisibility(View.INVISIBLE);
                Toast.makeText(SubFolderActivity.this, Constants.SOMETHING_WRONG, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onDocumentNameChanged(ViewContents.Document document) {
        for (int i = 0; i < documents.size(); i++) {
            if (document.getId().equals(documents.get(i).getId())) {
                documents.get(i).setFileName(document.getFileName());
            }
        }
        adapter.notifyDataSetChanged();
    }

    void addViewMoreLogic(String description) {
        documentDescription.setText(description);
        textTemp.setText(description);
        textTemp.post(() -> {
            if (textTemp.getLineCount() > 2) {
                txt_less_or_more.setVisibility(View.VISIBLE);
                documentDescription.setMaxLines(2);
                documentDescription.setEllipsize(TextUtils.TruncateAt.END);
            } else {
                txt_less_or_more.setVisibility(View.GONE);
            }
            documentDescription.setText(description);
        });
        txt_less_or_more.setOnClickListener(v -> {
            if (txt_less_or_more.getText().equals("Read More")) {
                txt_less_or_more.setText("Read Less");
                documentDescription.setText(description);
                documentDescription.setMaxLines(Integer.MAX_VALUE);
                documentDescription.setEllipsize(null);
            } else {
                txt_less_or_more.setText("Read More");
                documentDescription.setMaxLines(2);
                documentDescription.setEllipsize(TextUtils.TruncateAt.END);
            }
        });

    }
}
