package com.familheey.app.Activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.familheey.app.Fragments.FamilyCreationLevelAdvancedFragment;
import com.familheey.app.Fragments.FamilyDashboardFragment;
import com.familheey.app.Fragments.FamilyEditFragment;
import com.familheey.app.Fragments.FamilySettingsFragment;
import com.familheey.app.Fragments.FamilyViewMembers.FamilySubscriptionFragment;
import com.familheey.app.Fragments.FoldersFragment;
import com.familheey.app.Interfaces.FamilyDashboardListener;
import com.familheey.app.Interfaces.ProgressListener;
import com.familheey.app.Models.Response.Family;
import com.familheey.app.R;
import com.familheey.app.Utilities.Constants;
import com.familheey.app.Utilities.SharedPref;
import com.familheey.app.Utilities.Utilities;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Objects;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.familheey.app.Utilities.Constants.Bundle.DATA;
import static com.familheey.app.Utilities.Constants.Bundle.DESCRIPTION;
import static com.familheey.app.Utilities.Constants.Bundle.IS_CREATED_NOW;
import static com.familheey.app.Utilities.Constants.Bundle.NOTIFICATION_ID;
import static com.familheey.app.Utilities.Constants.Bundle.TITLE;
import static com.familheey.app.Utilities.Constants.Bundle.TYPE;

public class FamilyDashboardActivity extends AppCompatActivity implements FamilyDashboardListener, FamilySubscriptionFragment.OnFamilySubscriptionListener, FamilyCreationLevelAdvancedFragment.OnFamilyCreationListener, ProgressListener, FoldersFragment.OnFolderSelectedListener {

    SweetAlertDialog progressDialog;
    private Family family;
    private String NOTIFICATION = "";
    private String PAY = "";
    private String notificationId = "";
    private DatabaseReference database;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_family_dashboard);
        if (getIntent() != null && getIntent().hasExtra("PAY")) {
            PAY = getIntent().getStringExtra("PAY");
        }
        if (getIntent() != null && getIntent().hasExtra(TYPE)) {
            Integer eventId = Integer.parseInt(Objects.requireNonNull(getIntent().getStringExtra(DATA)));
            family = new Family();
            family.setGroupId(eventId);
            family.setId(eventId);
            addFragment(FamilyDashboardFragment.newInstance(family, getIntent().getStringExtra(TYPE), PAY));
        } else {
            family = Objects.requireNonNull(getIntent()).getParcelableExtra(DATA);
            addFragment(FamilyDashboardFragment.newInstance(family, getIntent().hasExtra(IS_CREATED_NOW)));
        }
        if (getIntent() != null && getIntent().hasExtra(Constants.Bundle.NOTIFICATION)) {
            NOTIFICATION = getIntent().getStringExtra(Constants.Bundle.NOTIFICATION);
        }

        // Modified By: Dinu(22/02/2021) For update visible_status="read" in firebase
        if (getIntent().hasExtra(NOTIFICATION_ID)) {
            notificationId=getIntent().getStringExtra(NOTIFICATION_ID);
            database= FirebaseDatabase.getInstance("https://familheey-255407.firebaseio.com/").getReference(Constants.ApiPaths.FIREBASE_USER_TYPE + SharedPref.getUserRegistration().getId() + "_notification");
            database.child(notificationId).child("visible_status").setValue("read");
        }
    }

    @Override
    public void showProgressDialog() {
        progressDialog = Utilities.getProgressDialog(this);
        progressDialog.show();
    }

    @Override
    public void hideProgressDialog() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }

    @Override
    public void showErrorDialog(String errorMessage) {
        if (progressDialog == null) {
            progressDialog = Utilities.getErrorDialog(this, errorMessage);
            progressDialog.show();
            return;
        }
        Utilities.getErrorDialog(progressDialog, errorMessage);
    }

    public boolean addFragment(Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.familyDashboardContainer, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    public void loadFamilySubscriptions(Family family) {
        getSupportFragmentManager().beginTransaction()
                .add(R.id.familyDashboardContainer, FamilySubscriptionFragment.newInstance(family))
                .addToBackStack("FamilySubscriptionFragment")
                .commit();
    }

    @Override
    public void loadFamilyAdvancedSettings(Family family) {

        getSupportFragmentManager().beginTransaction()
                .add(R.id.familyDashboardContainer, FamilyCreationLevelAdvancedFragment.newInstance(family, true))
                .addToBackStack("FamilyCreationLevelAdvancedFragment")
                .commit();
    }

    @Override
    public void loadFamilyBasicSettings(Family family) {

        getSupportFragmentManager().beginTransaction()
                .add(R.id.familyDashboardContainer, FamilyEditFragment.newInstance(family))
                .addToBackStack("FamilyEditFragment")
                .commit();
    }

    @Override
    public void loadFamilySettings(Family family) {
        getSupportFragmentManager().beginTransaction()
                .add(R.id.familyDashboardContainer, FamilySettingsFragment.newInstance(family))
                .addToBackStack("FamilySettingsFragment")
                .commit();
    }

    @Override
    public void loadFileUploading(Family family) {
        getSupportFragmentManager().beginTransaction()
                .add(R.id.familyDashboardContainer, FoldersFragment.newInstance(family.getId().toString(), Constants.FileUpload.TYPE_FAMILY))
                .addToBackStack("FoldersFragment")
                .commit();
    }

    @Override
    public void onFamilyUpdated(boolean isBackPressedNeeded) {
        if (isBackPressedNeeded)
            onBackPressed();

        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.familyDashboardContainer);
        if (fragment instanceof FamilySettingsFragment) {
            onBackPressed();

            Fragment fragment1 = getSupportFragmentManager().findFragmentById(R.id.familyDashboardContainer);
            if (fragment1 instanceof FamilyDashboardFragment) {
                FamilyDashboardFragment familyDashboardFragment = (FamilyDashboardFragment) fragment1;
                familyDashboardFragment.refreshFamilyDetails(family);
            }
        } else if (fragment instanceof FamilyDashboardFragment) {
            FamilyDashboardFragment familyDashboardFragment = (FamilyDashboardFragment) fragment;
            familyDashboardFragment.refreshFamilyDetails(family);
        }

    }

    @Override
    public void loadMembersToBeAddedToFamily(Family family) {
        Intent intent = new Intent(getApplicationContext(), FamilyAddMemberActivity.class);
        intent.putExtra(Constants.Bundle.DATA, family);
        startActivityForResult(intent, FamilyAddMemberActivity.REQUEST_CODE);
    }

    @Override
    public void onFamilyCreated(Family family) {
        onBackPressed();

        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.familyDashboardContainer);
        if (fragment instanceof FamilySettingsFragment) {
            onBackPressed();

            Fragment fragment1 = getSupportFragmentManager().findFragmentById(R.id.familyDashboardContainer);
            if (fragment1 instanceof FamilyDashboardFragment) {
                FamilyDashboardFragment familyDashboardFragment = (FamilyDashboardFragment) fragment1;
                familyDashboardFragment.refreshFamilyDetails(family);
            }
        } else if (fragment instanceof FamilyDashboardFragment) {
            FamilyDashboardFragment familyDashboardFragment = (FamilyDashboardFragment) fragment;
            familyDashboardFragment.refreshFamilyDetails(family);
        }

    }

    @Override
    public void onBackPressed() {

        if (NOTIFICATION.equals("NOTIFICATION")) {
            finish();

            overridePendingTransition(R.anim.left,
                    R.anim.right);
        } else {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.familyDashboardContainer);
            if (fragment instanceof FamilyDashboardFragment) {
                ((FamilyDashboardFragment) fragment).goBack();
            } else
                super.onBackPressed();
        }
    }

    @Override
    public void loadSubFolder(String id, String folderId, int type, String title, String description, boolean isAdmin, boolean canCreate, boolean canUpdate) {
        Intent intent = new Intent(this, SubFolderActivity.class);
        intent.putExtra(Constants.Bundle.ID, id).putExtra(Constants.Bundle.FOLDER_ID, folderId);
        intent.putExtra(Constants.Bundle.TYPE, type);
        intent.putExtra(Constants.Bundle.IS_ADMIN, isAdmin);
        intent.putExtra(TITLE, title);
        intent.putExtra(DESCRIPTION, description);
        startActivity(intent);
    }
}
