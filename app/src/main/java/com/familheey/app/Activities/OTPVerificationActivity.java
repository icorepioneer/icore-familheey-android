package com.familheey.app.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.familheey.app.FamilheeyApplication;
import com.familheey.app.Interfaces.RetrofitListener;
import com.familheey.app.Models.ApiCallbackParams;
import com.familheey.app.Models.ErrorData;
import com.familheey.app.Models.Request.Device;
import com.familheey.app.Models.Request.OTPRequest;
import com.familheey.app.Models.Response.UserRegistrationResponse;
import com.familheey.app.Networking.Retrofit.ApiServiceProvider;
import com.familheey.app.Networking.Retrofit.ApiServices;
import com.familheey.app.Networking.Retrofit.RetrofitBase;
import com.familheey.app.Networking.utils.GsonUtils;
import com.familheey.app.R;
import com.familheey.app.SplashScreen;
import com.familheey.app.Utilities.Constants;
import com.familheey.app.Utilities.SharedPref;
import com.familheey.app.Utilities.Utilities;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.iid.FirebaseInstanceId;
import com.mukesh.OtpView;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;

public class OTPVerificationActivity extends AppCompatActivity implements RetrofitListener {

    private CountDownTimer timer;
    private UserRegistrationResponse userRegistrationResponse;
    private SweetAlertDialog progressDialog;
    public CompositeDisposable subscriptions;

    @BindView(R.id.otp)
    OtpView otp;
    @BindView(R.id.resendOTP)
    TextView resendOTP;
    @BindView(R.id.verifyOTP)
    MaterialButton verifyOTP;
    @BindView(R.id.countDownTimer)
    TextView countDownTimer;
    @BindView(R.id.labelEnterTheOtpYouReceived)
    TextView labelEnterTheOtpYouReceived;
    @BindView(R.id.back)
    ImageView back;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otpverification);
        ButterKnife.bind(this);
        setStatusBarColor();


        subscriptions = new CompositeDisposable();
        userRegistrationResponse = getIntent().getParcelableExtra(Constants.Bundle.DATA);
        startTimer();
        verifyOTP.setVisibility(View.INVISIBLE);
        otp.setOtpCompletionListener(otpEntered -> {
            try {
                InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                assert imm != null;
                imm.hideSoftInputFromWindow(otp.getWindowToken(), 0);
            } catch (Exception e) {
                e.printStackTrace();
            }
            verifyOTP.performClick();
        });
        labelEnterTheOtpYouReceived.setText("Enter the OTP you received to " + userRegistrationResponse.getMobileNumber());
    }

    private void setStatusBarColor() {
        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }
    }


    private void startTimer() {
        resendOTP.setClickable(false);
        timer = new CountDownTimer(60000, 1000) {

            public void onTick(long millisUntilFinished) {
                countDownTimer.setText("" + (millisUntilFinished / 1000));
            }

            public void onFinish() {
                // Enable Resend OTP From Here
                resendOTP.setClickable(true);
                resendOTP.setTextColor(getResources().getColor(R.color.colorPrimary));
            }

        }.start();
    }

    @OnClick({R.id.resendOTP, R.id.verifyOTP, R.id.back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.resendOTP:
                resendOTPRequest();
                break;
            case R.id.verifyOTP:
                verifyOTP.setEnabled(false);
                if (Objects.requireNonNull(otp.getText()).toString().trim().length() != 6) {
                    Toast.makeText(this, "Please enter 6 digit OTP !!", Toast.LENGTH_SHORT).show();
                    verifyOTP.setEnabled(true);
                    return;
                }
                OTPRequest otpRequest = new OTPRequest();
                otpRequest.setMobileNumber(userRegistrationResponse.getMobileNumber());
                otpRequest.setReceivedOTP(otp.getText().toString());
                validateOTP(otpRequest);
                break;
            case R.id.back:
                onBackPressed();
                break;
        }
    }

    private void resendOTPRequest() {
        progressDialog = Utilities.getProgressDialog(this);
        progressDialog.show();
        ApiServiceProvider apiServiceProvider = ApiServiceProvider.getInstance(this);
        apiServiceProvider.resendOTP(userRegistrationResponse.getMobileNumber(), null, this);
    }

    private void validateOTP(OTPRequest otpRequest) {
        progressDialog = Utilities.getProgressDialog(this);
        progressDialog.show();
        ApiServiceProvider apiServiceProvider = ApiServiceProvider.getInstance(this);
        apiServiceProvider.confirmOTP(otpRequest, null, this);
        verifyOTP.setEnabled(true);
    }

    @Override
    public void onResponseSuccess(String responseBodyString, ApiCallbackParams apiCallbackParams, int apiFlag) {
        progressDialog.dismiss();
        switch (apiFlag) {
            case Constants.ApiFlags.CONFIRM_OTP:
                if (timer != null) timer.cancel();
                UserRegistrationResponse userRegistrationResponse = GsonUtils.getInstance().getGson().fromJson(responseBodyString, UserRegistrationResponse.class);
                SharedPref.write(SharedPref.IS_REGISTERED, true);
                SharedPref.write(SharedPref.USER, GsonUtils.getInstance().getGson().toJson(userRegistrationResponse));
                SharedPref.setUserRegistration(userRegistrationResponse);
                getFCMToken();
                if (userRegistrationResponse.isAnExistingUser()) {
                    Intent intent = new Intent(getApplicationContext(), SplashScreen.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(Constants.Bundle.DATA, userRegistrationResponse);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(getApplicationContext(), UserRegistrationActivity.class);
                    intent.putExtra(Constants.Bundle.DATA, userRegistrationResponse);
                    startActivity(intent);
                }
                finish();
                break;
            case Constants.ApiFlags.RESEND_OTP:
                startTimer();
                otp.setText("");
                resendOTP.setTextColor(getResources().getColor(R.color.default_text_color));
                Toast.makeText(this, "Otp sent !!", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void onResponseError(ErrorData errorData, ApiCallbackParams apiCallbackParams, Throwable throwable, int apiFlag) {
        switch (apiFlag) {
            case Constants.ApiFlags.CONFIRM_OTP:
                Utilities.getErrorDialogWithoutTitle(progressDialog, "\nInvalid OTP, Please try again\n");
                otp.setText("");
                break;
            case Constants.ApiFlags.RESEND_OTP:
                Utilities.getErrorDialog(progressDialog, Constants.SOMETHING_WRONG);
                break;
        }
    }


    private void saveDevice(String fcmToken) {
        String androidId = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Device device = new Device();
        device.setUser_id(SharedPref.getUserRegistration().getId());
        device.setDevice_id(androidId);
        device.setDevice_token(fcmToken);
        device.setDevice_type("ANDROID");
        FamilheeyApplication application = FamilheeyApplication.get(this);
        ApiServices apiServices = RetrofitBase.createRxResourceWithoutAuth(ApiServices.class, this);
        subscriptions.add(apiServices.saveDevice(device)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(application.defaultSubscribeScheduler())
                .subscribe(response -> {
                }, throwable -> {
                }));
    }


    private void saveFcmToken(String idToken) {
        SharedPreferences pref = getSharedPreferences("Token_pref", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("USER_FCM_TOKEN", idToken);
        editor.apply();
    }

    private void getFCMToken() {
        SharedPreferences pref = getSharedPreferences("Token_pref", Context.MODE_PRIVATE);
        String token = pref.getString("USER_FCM_TOKEN", "");
        assert token != null;
        if (token.isEmpty()) {
            generateFcmToken();
        }

    }

    public void generateFcmToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        return;
                    }
                    String token = task.getResult().getToken();
                    saveDevice(token);
                    saveFcmToken(token);
                });
    }
}
