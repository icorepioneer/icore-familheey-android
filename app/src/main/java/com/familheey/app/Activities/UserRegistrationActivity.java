package com.familheey.app.Activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.familheey.app.Interfaces.RetrofitListener;
import com.familheey.app.Models.ApiCallbackParams;
import com.familheey.app.Models.ErrorData;
import com.familheey.app.Models.Response.UserRegistrationResponse;
import com.familheey.app.Networking.Retrofit.ApiServiceProvider;
import com.familheey.app.Networking.utils.GsonUtils;
import com.familheey.app.R;
import com.familheey.app.Utilities.Constants;
import com.familheey.app.Utilities.SharedPref;
import com.familheey.app.Utilities.Utilities;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.model.Place;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;
import static com.familheey.app.Utilities.Constants.ApiPaths.IMAGE_BASE_URL;
import static com.familheey.app.Utilities.Constants.ApiPaths.S3_DEV_IMAGE_URL_SQUARE;
import static com.familheey.app.Utilities.Constants.Bundle.ADDRESS;
import static com.familheey.app.Utilities.Constants.Bundle.IDENTIFIER;
import static com.familheey.app.Utilities.Constants.Bundle.PLACE;
import static com.familheey.app.Utilities.Constants.Paths.PROFILE_PIC;
import static com.familheey.app.Utilities.Utilities.isValidEmail;

public class UserRegistrationActivity extends AppCompatActivity implements RetrofitListener {

    public static final int LIVING_IN = 9856;
    public static final int ORIGIN = 4512;

    @BindView(R.id.headerImage)
    ImageView headerImage;
    @BindView(R.id.profileImage)
    CircleImageView profileImage;
    @BindView(R.id.editProfileImage)
    ImageView editProfileImage;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.livingIn)
    EditText livingIn;
    @BindView(R.id.origin)
    EditText etorigin;
    @BindView(R.id.next)
    Button next;

    private SweetAlertDialog progressDialog;
    private UserRegistrationResponse userRegistration;
    private LatLng latLng = null;
    private FusedLocationProviderClient fusedLocationClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_registration);
        ButterKnife.bind(this);
        userRegistration = getIntent().getParcelableExtra(Constants.Bundle.DATA);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(getApplicationContext());
        SharedPref.init(getApplicationContext());
        initializeRestrictions();
        prefillProfile();
    }

    private void initializeRestrictions() {
        livingIn.setFocusable(false);
        livingIn.setClickable(true);
        livingIn.setLongClickable(false);
        etorigin.setFocusable(false);
        etorigin.setClickable(true);
        etorigin.setLongClickable(false);
    }

    private void prefillProfile() {
        if (userRegistration.getFullName() != null && userRegistration.getFullName().length() > 0)
            name.setText(userRegistration.getFullName());
        if (userRegistration.getOrigin() != null && userRegistration.getOrigin().length() > 0)
            etorigin.setText(userRegistration.getOrigin());
        if (userRegistration.getLocation() != null && userRegistration.getLocation().length() > 0)
            livingIn.setText(userRegistration.getLocation());
        if (userRegistration.getEmail() != null && userRegistration.getEmail().length() > 0)
            email.setText(userRegistration.getEmail());
        if (userRegistration.getPropic() != null && userRegistration.getPropic().length() > 0) {
            Glide.with(getApplicationContext())
                    .load(S3_DEV_IMAGE_URL_SQUARE + IMAGE_BASE_URL + PROFILE_PIC + userRegistration.getPropic())
                    .transition(withCrossFade(Utilities.getDrawableCrossFadeFactory()))
                    .apply(Utilities.getCurvedRequestOptions())
                    .placeholder(R.drawable.avatar_male)
                    .into(profileImage);
        }
    }

    private boolean validate() {
        boolean isValid = true;
        if (name.getText().toString().trim().length() == 0) {
            name.setError("Required Field");
            isValid = false;
        }
        if (email.getText().toString().trim().length() == 0) {
            email.setError("Required Field");
            isValid = false;
        } else if (!isValidEmail(email.getText().toString().trim())) {
            email.setError("Invalid email");
            isValid = false;
        }
        if (livingIn.getText().toString().trim().length() == 0) {
            livingIn.setError("Invalid email");
            isValid = false;
        }
        if (etorigin.getText().toString().trim().length() == 0) {
            etorigin.setError("Invalid email");
            isValid = false;
        }
        return isValid;
    }

    public void registerUser() {
        userRegistration.setEmail(email.getText().toString().trim());
        userRegistration.setOrigin(etorigin.getText().toString().trim());
        userRegistration.setLocation(livingIn.getText().toString().trim());
        userRegistration.setFullName(name.getText().toString().trim());
        userRegistration.setVerified(true);
        if (latLng != null) {
            userRegistration.setLat(latLng.latitude);
            userRegistration.setLng(latLng.longitude);
        } else {
            userRegistration.setLat(null);
            userRegistration.setLng(null);
        }
        progressDialog = Utilities.getProgressDialog(this);
        progressDialog.show();
        userRegistration.setActive(true);
        ApiServiceProvider apiServiceProvider = ApiServiceProvider.getInstance(getApplicationContext());
        apiServiceProvider.completeUserRegistration(userRegistration, null, this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.cancel();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == Activity.RESULT_OK) {
                assert result != null;
                Uri profilePictureUri = result.getUri();
                Glide.with(getApplicationContext())
                        .load(profilePictureUri)
                        .apply(RequestOptions.circleCropTransform())
                        .into(profileImage);
                userRegistration.setCapturedImage(profilePictureUri.toString());
            } /*else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }*/
        } else if (requestCode == AddressFetchingActivity.RequestCode) {
            if (resultCode == Activity.RESULT_OK) {
                assert data != null;
                if (data.getIntExtra(IDENTIFIER, ORIGIN) == ORIGIN)
                    etorigin.setText(data.getStringExtra(ADDRESS));
                else {
                    livingIn.setText(data.getStringExtra(ADDRESS));
                    if (etorigin.getText().toString().trim().length() == 0)
                        etorigin.setText(data.getStringExtra(ADDRESS));
                    Place place = data.getParcelableExtra(PLACE);
                    if (place != null && place.getLatLng() != null)
                        latLng = place.getLatLng();
                    else latLng = null;
                }
            }
        }
    }

    @Override
    public void onResponseSuccess(String responseBodyString, ApiCallbackParams apiCallbackParams, int apiFlag) {

        UserRegistrationResponse userRegistrationResponse = SharedPref.getUserRegistration();
        userRegistration = GsonUtils.getInstance().getGson().fromJson(responseBodyString, UserRegistrationResponse.class);
        userRegistration.setAccessToken(userRegistrationResponse.getAccessToken());
        userRegistration.setRefreshToken(userRegistrationResponse.getRefreshToken());
        SharedPref.write(SharedPref.IS_REGISTERED, true);
        SharedPref.write(SharedPref.USER, GsonUtils.getInstance().getGson().toJson(userRegistration));
        SharedPref.setUserRegistration(userRegistration);
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
        Intent intent = new Intent(getApplicationContext(), UserRegistrationCompletedActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(Constants.Bundle.DATA, userRegistration);
        startActivity(intent);
        finish();
    }

    @Override
    public void onResponseError(ErrorData errorData, ApiCallbackParams apiCallbackParams, Throwable throwable, int apiFlag) {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismissWithAnimation();
        Utilities.getErrorDialog(this, Constants.SOMETHING_WRONG).show();
    }

    @OnClick({R.id.profileImage, R.id.editProfileImage, R.id.livingIn, R.id.origin, R.id.next})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.profileImage:
            case R.id.editProfileImage:
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setAspectRatio(1, 1)
                        .setCropShape(CropImageView.CropShape.OVAL)
                        .start(this);
                break;
            case R.id.livingIn:
                Intent livingInAddressintent = new Intent(getApplicationContext(), AddressFetchingActivity.class);
                livingInAddressintent.putExtra(IDENTIFIER, LIVING_IN);
                startActivityForResult(livingInAddressintent, AddressFetchingActivity.RequestCode);
                break;
            case R.id.origin:
                Intent originAddressIntent = new Intent(getApplicationContext(), AddressFetchingActivity.class);
                originAddressIntent.putExtra(IDENTIFIER, ORIGIN);
                startActivityForResult(originAddressIntent, AddressFetchingActivity.RequestCode);
                break;
            case R.id.next:
                if (validate()) {
                    registerWithLocation();
                }
                break;
        }
    }

    private void registerWithLocation() {
        if (latLng != null)
            registerUser();
        else {
            if (TedPermission.isGranted(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION))
                getLastKnownLocation();
            else requestPermission();
        }
    }

    private void requestPermission() {
        TedPermission.with(getApplicationContext())
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        getLastKnownLocation();
                    }

                    @Override
                    public void onPermissionDenied(List<String> deniedPermissions) {
                        latLng = null;
                        registerUser();
                    }
                })
                .setDeniedMessage("If you reject permission,you do not get perfect suggestions\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setPermissions(Manifest.permission.ACCESS_FINE_LOCATION)
                .check();
    }

    private void getLastKnownLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(location -> {
                    if (location != null) {
                        latLng = new LatLng(location.getLatitude(), location.getLongitude());
                    } else latLng = null;
                    registerUser();
                })
                .addOnFailureListener(e -> {
                    latLng = null;
                    registerUser();
                    e.printStackTrace();
                });
    }
}
