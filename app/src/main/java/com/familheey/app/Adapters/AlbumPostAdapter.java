package com.familheey.app.Adapters;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.familheey.app.Models.Request.Image;
import com.familheey.app.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.familheey.app.Utilities.Constants.ApiPaths.IMAGE_BASE_URL;

public class AlbumPostAdapter extends RecyclerView.Adapter<AlbumPostAdapter.ViewHolder> {
    private List<Image> documents;
    private OnAlbumSelectedListener mListener;

    public AlbumPostAdapter(OnAlbumSelectedListener mListener, List<Image> albumDocuments) {
        documents = albumDocuments;
        this.mListener = mListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.album_post_adatper, parent, false);

        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Image document = documents.get(position);
            holder.progressBar.setVisibility(View.VISIBLE);
            holder.imagePlay.setVisibility(View.INVISIBLE);

            if(document.isIsuploading()){
                holder.progressBar.setVisibility(View.VISIBLE);
                holder.delete.setVisibility(View.INVISIBLE);
                holder.progressBar.setProgress(document.getPrograss());
            }else{
                holder.progressBar.setVisibility(View.GONE);
                holder.delete.setVisibility(View.VISIBLE);
            }

        if(document.isVideo()){
            holder.imagePlay.setVisibility(View.VISIBLE);
        }else{
            holder.imagePlay.setVisibility(View.GONE);
        }


        if(document.isDoc()){
            holder.albumImage.setImageResource(R.drawable.doc);
        }
        else {

            if (document.isUrl()) {
                Glide.with(holder.albumImage.getContext())
                        .load(IMAGE_BASE_URL + "post/" + document.getmUrl())
                        .placeholder(R.drawable.family_dashboard_background)
                        .into(holder.albumImage);
            } else {
                Glide.with(holder.albumImage.getContext())
                        .load(Uri.parse(document.getmUrl()))
                        .placeholder(R.drawable.family_dashboard_background)
                        .into(holder.albumImage);
            }
        }

            holder.delete.setOnClickListener(v -> {

                if(!document.isIsuploading()){
                documents.remove(position);
                notifyDataSetChanged();
                mListener.onAlbumDeleted(position);
                }
            });

    }

    @Override
    public int getItemCount() {
        return documents.size();
    }



    public interface OnAlbumSelectedListener {

        void onAlbumDeleted( int position);

    }

    static
    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imagedelete)
        ImageView delete;
        @BindView(R.id.albumImage)
        ImageView albumImage;
        @BindView(R.id.progressBar)
        ProgressBar progressBar;
        @BindView(R.id.imagePlay)
        ImageView imagePlay;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}